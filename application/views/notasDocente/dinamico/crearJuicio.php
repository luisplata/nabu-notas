<div class="col-lg-6">
    <?= form_open("juicios/guardarJuicio") ?>

    <div class="form-group">
        Descripcion
        <input type="text" name="descripcion" class="form-control" />
    </div>
    <div class="form-group">
        Asignatura
        <select name="asignatura_id" class="form-control" multiple>
            <?php foreach ($asignaturas as $value) { ?>
                <option value="<?= $value->id ?>">
                    <?= $value->nombre ?>
                </option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group">
        <input type="submit" value="Guardar" class="btn btn-success" />
        <?= anchor("juicios/", "Cancelar", "class='btn btn-warning'") ?>
    </div>
</form>
</div>
<div class="col-lg-6 col-md-6 table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr>
                <td>
                    Asignatura
                </td>

                <td>
                    Descripcion
                </td>
                <td>
                    Accion
                </td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($juicios as $value) { ?>
                <tr>
                    <td>
                        <?= $value->asignatura_nombre ?>
                    </td>
                    <td>
                        <?= $value->descripcion ?>
                    </td>
                    <td>
                        <?= anchor("juicios/eliminar/" . $value->id, "Eliminar", "class='btn btn-danger'") ?>
                        <?= anchor("juicios/modificarJuicio/" . $value->id, "Modificar", "class='btn btn-default'") ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>


</div>