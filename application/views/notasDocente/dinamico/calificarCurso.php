<div class="col-lg-12 table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <td>
                    Apellido
                </td>
                <td>
                    Nombre
                </td>
                <td>
                    Codigo
                </td>
                <td>
                    Accion
                </td>
                
            </tr>
        </thead>
        <tbody>
            <?php foreach ($estudiantes as $value) { ?>
                <tr>
                    <td>
                        <?= $value->estudiante_nombre ?>
                    </td>
                    <td>
                        <?= $value->estudiante_apellido ?>
                    </td>
                    <td>
                        <?= $value->estudiante_codigo ?>
                    </td>
                    <td>
                        <iframe height="179" width="600" src="<?= base_url() ?>calificaciones/notasEstudiantes/<?= $value->estudiante_id ?>/<?= $asignatura ?>" ></iframe>
                    </td>
                </tr>
            <?php } ?>

        </tbody>
    </table>

</div>
