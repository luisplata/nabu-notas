<div class="col-lg-12">
    <?= form_open("juicios/crearCargarJuicioPorCurso") ?>
    <div class="form-group col-xs-6">
        Curso:
        <select name="curso" size="10" class="form-control" required >
            <?php foreach ($cursos as $value) { ?>
                <option value="<?= $value->grado_id ?>-<?= $value->grupo_id ?>-<?= $value->jornada_id ?>">
                    <?= $value->grado ?>-<?= $value->grupo ?>-<?= $value->jornada ?>
                </option>
            <?php } ?>
        </select>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            Juicio
            <select class="form-control" size="10" name="juicio_id" required >
                <?php foreach ($juicios as $value) { ?>
                    <option value="<?= $value->id ?>" >
                        <?= $value->asignatura_nombre ?> - <?= $value->descripcion ?>
                    </option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <input type="submit" value="Seleccionar" class="btn btn-success" />    
    </div>
</form>
</div>