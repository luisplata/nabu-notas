<?php
$periodo = $this->configuracion_model->periodo();

?>
<div class="col-md-12">
    <?php
    $periodo['periodo'];

    $datetime1 = date_create($periodo['fecha_inicio']);
    $datetime2 = date_create($periodo['fecha_final']);
    $interval = date_diff($datetime1, $datetime2); //total de dias
    $actual_fecha = date_create(date("Y-m-d"));
    $inicial = date_create($periodo['fecha_inicio']);
    $transcurrido = date_diff($inicial, $actual_fecha); //Transcurrido
    ?>
    <?php if (($interval->format('%R%a días') - $transcurrido->format('%R%a días')) <= 15) { ?>
        <a href="<?= base_url() ?>juicios/crearJuicio" class="btn btn-default">Crear Juicio</a> 
        <a href="<?= base_url() ?>juicios/cargarJuicioPorCurso" class="btn btn-default">Cargar Juicio a todo un grupo</a> 
        <a href="<?= base_url() ?>juicios/cargarJuicio" class="btn btn-default">Cargar Juicio individual</a> 
        <a href="<?= base_url() ?>juicios/calificarUnEstudiante" class="btn btn-default">Calificar el juicio a un estudiante</a> 
    <?php } else { ?>
        <a href="<?= base_url() ?>juicios/crearJuicio" class="btn btn-default">Crear Juicio</a>         
    <?php } ?>

</div>