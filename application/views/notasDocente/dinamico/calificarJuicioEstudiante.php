<div>
    <div class="col-xs-12">
        <div class="form-group">
            <table class="table table-hover table-bordered table-responsive">
                <thead>
                    <tr>
                        <td>
                            Apellido
                        </td>
                        <td>
                            Nombre
                        </td>
                        <td>
                            Codigo
                        </td>
                        <td>
                            Juicios A Cargar
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($estudiantes as $value) { ?>
                        <tr>
                            <td class="text-capitalize text-center">
                                <?= $value->estudiante_apellido ?> 
                            </td>
                            <td class="text-capitalize text-center">
                                <?= $value->estudiante_nombre ?>
                            </td>
                            <td class="text-capitalize text-center">
                                <?= $value->estudiante_codigo ?>
                            </td>
                            <td>
                                <?php $value->estudiante_id ?>
                    <iframe width="100%" src="<?= base_url() ?>juicios/calificarJuicioEstudianteUnico/<?= $value->estudiante_id ?>" ></iframe>

                    </td>
                    </tr>

                <?php } ?>
                <tr>
                    <td>

                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>