<div>

    <div class="col-xs-7">
        <div class="form-group">
            <table class="table table-hover table-bordered table-responsive">
                <thead>
                    <tr>
                        <td>
                            Apellido
                        </td>
                        <td>
                            Nombre
                        </td>
                        <td>
                            Codigo
                        </td>
                        <td>
                            Juicios A Cargar
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($estudiantes as $value) { ?>
                        <tr>
                            <td class="text-capitalize text-center">
                                <?= $value->estudiante_apellido ?> 
                            </td>
                            <td class="text-capitalize text-center">
                                <?= $value->estudiante_nombre ?>
                            </td>
                            <td class="text-capitalize text-center">
                                <?= $value->estudiante_codigo ?>
                            </td>
                            <td>
                                <?php $value->estudiante_id ?>
                                <div id="cart">
                                    <?php form_open("juicios/guardarJuicioIndividual", "class='juicioPersonal'") ?>
                                    <div class="">
                                        <ol class="<?= $value->estudiante_id ?>">
                                            <li class="placeholder">Add your items here</li>
                                        </ol>                                        
                                        <button class="btn-xs btn-success form-control" onclick="juicioPersonal(<?= $value->estudiante_id ?>)" value="Enviar">Enviar</button>
                                    </div>
                                    <!--</form>-->
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>

                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div id="column-left" class="col-xs-4">
        <div id="catalog">
            <?php
            foreach ($asignaturas as $value2) {
                ?>
                <span><a class = "h2" ><?= $value2->nombre ?></a></span>
                <div>
                    <ul class="list-unstyled">
                        <?php
                        foreach ($juicios as $value) {
                            if ($value->asignatura_id == $value2->id) {
                                ?>
                                <li value="<?= $value->id ?>">
                                    <?= $value->asignatura_nombre ?> - <?= $value->descripcion ?>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            <?php }
            ?>

        </div>
    </div>
</div>
