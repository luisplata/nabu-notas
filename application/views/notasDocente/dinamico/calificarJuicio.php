<div class="col-lg-12">
    <?= form_open("juicios/calificarJuicioEstudiante") ?>
    <div class="form-group col-xs-4">
        Curso:
        <select name="curso" size="10" class="form-control">
            <?php foreach ($cursos as $value) { ?>
                <option value="<?= $value->grado_id ?>-<?= $value->grupo_id ?>-<?= $value->jornada_id ?>">
                    <?= $value->grado ?>-<?= $value->grupo ?>-<?= $value->jornada ?>
                </option>
            <?php } ?>
        </select>
        <div class="form-group">
            <input type="submit" value="Seleccionar" class="btn btn-success" />    
        </div>
    </div>
</form>
</div>