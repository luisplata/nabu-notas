<div>
    <?= form_open("juicios/guardarCargarjuicio") ?>
    <div class="col-lg-4">
        <div class="form-group">
            Estudiante
            <select class="form-control" size="10" name="estudiante_id">
                <?php foreach ($estudiantes as $value) { ?>
                    <option value="<?= $value->estudiante_id ?>">
                        <?= $value->estudiante_apellido ?> <?= $value->estudiante_nombre ?>
                    </option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            Juicio
            <select class="form-control" size="10" name="juicio_id">
                <?php foreach ($juicios as $value) { ?>
                    <option value="<?= $value->id ?>" >
                        <?= $value->asignatura_nombre ?> - <?= $value->nombre ?>
                    </option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="col-lg-12">
        <input type="submit" class="btn btn-success" value="Guardar" />
        <a href="<?= base_url() ?>juicios/cargarJuicio" class="btn btn-danger">Cancelar</a>
    </div>
</form>
<div id="products">
    <h1 class="ui-widget-header">Products</h1>
    <div id="catalog">
        <h2><a href="#">T-Shirts</a></h2>
        <div>
            <ul>
                <li value="14">Lolcat Shirt</li>
                <li>Cheezeburger Shirt</li>
                <li>Buckit Shirt</li>
            </ul>
        </div>
        <h2><a href="#">Bags</a></h2>
        <div>
            <ul>
                <li>Zebra Striped</li>
                <li>Black Leather</li>
                <li>Alligator Leather</li>
            </ul>
        </div>
        <h2><a href="#">Gadgets</a></h2>
        <div>
            <ul>
                <li>iPhone</li>
                <li>iPod</li>
                <li>iPad</li>
            </ul>
        </div>
    </div>
</div>

<div id="cart">
    <h1 class="ui-widget-header">Shopping Cart</h1>
    <form method="POST" action="#">
        <div class="ui-widget-content">
            <ol>
                <li class="placeholder">Add your items here</li>
            </ol>
            <input type="submit" value="Enviar"/>
        </div>
    </form>
</div>
</div>