<div class="col-lg-12 table-responsive">

    <div class="col-lg-offset-4 col-md-4">
        <table class="table table-responsive" >
            <thead>
                <tr>
                    <td>
                        Asignatura
                    </td>
                    <td>
                        Accion
                    </td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($asignaturas as $value) { ?>
                    <tr>
                        <td>
                            <?= $value->asignatura ?>
                        </td>
                        <td>
                            <?= anchor("calificaciones/calificarCurso/" . $grado_id . "-" . $grupo_id . "-" . $jornada_id . "/" . $value->asignatura_id, "Escojer", 'class="btn btn-success"') ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

    </div>
</div>
