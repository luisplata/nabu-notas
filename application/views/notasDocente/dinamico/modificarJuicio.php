<div class="col-lg-12">
    <?= form_open("juicios/modificar") ?>
    <?php foreach ($juicio as $value) { ?>

        <div class="form-group">
            <input type="hidden" name="id" value="<?= $value->id ?>"/>
            <input type="hidden" name="asignatura_id" value="<?= $value->asignatura_id ?>"/>
            <label>
                Descripcion:
            </label>
            <textarea name="descripcion" class="form-control"><?= $value->descripcion ?></textarea>
        </div>
        <div>
            <input type="submit" value="Guardar" class="btn btn-success"/>
            <?= anchor("juicios/crearJuicio", "Cancelar", "class='btn btn-default'") ?>
        </div>
    <?php } ?>
</form>
</div>