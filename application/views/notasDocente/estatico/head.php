<?php
if ($this->session->userdata("docente_id") == NULL) {
    redirect("login/cerrarSession");
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?= $this->config->item("NOMBREAPP") ?></title>

        <!-- Bootstrap Core CSS -->
        <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?= base_url() ?>css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?= base_url() ?>css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?= base_url() ?>font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!--Para hacer el arrastrable con jquery UI-->
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

        <style>
            h1 { padding: .2em; margin: 0; }
            #products { float:left; width: 500px; margin-right: 2em; }
            #cart { width: 100%; float: left; margin-top: 1em; }
            /* style the list to maximize the droppable hitarea */
            #cart ol { margin: 0; padding: 1em 0 1em 3em; }

            /*Aqui es para que el div de juicios este siempre en la misma pocision mientras se baja*/
            #column-left {
                position: fixed;
                right: 10px;
            }


        </style>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">





