<?php
$periodo = $this->configuracion_model->periodo();
?>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="<?= base_url() ?>login/modulos" class="navbar-brand"><?= $this->config->item("NOMBREAPP") ?></a> 
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">

        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-tasks fa-fw"></i> Linea de Tiempo <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-tasks">
                <li>
                    <a href="#">
                        <div>
                            <p>
                                <strong>Periodo Actual : <?= $periodo['periodo'] ?>
                                    <?php
                                    $datetime1 = date_create($periodo['fecha_inicio']);
                                    $datetime2 = date_create($periodo['fecha_final']);
                                    $interval = date_diff($datetime1, $datetime2); //total de dias
                                    $actual_fecha = date_create(date("Y-m-d"));
                                    $inicial = date_create($periodo['fecha_inicio']);
                                    $transcurrido = date_diff($inicial, $actual_fecha); //Transcurrido
                                    //porcentaje
                                    $porcentaje = ($transcurrido->format('%R%a días') * 100) / $interval->format('%R%a días');
                                    ?>
                                    <?php $interval->format('%R%a días') ?>
                                    <?php $transcurrido->format('%R%a días') ?>
                                    <?php $porcentaje ?>
                                    <br/>
                                    Faltan: <?= $interval->format('%R%a días') - $transcurrido->format('%R%a días') ?> dias
                                </strong>
                            </p>
                            <div class="progress progress-striped active">

                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: <?= $porcentaje ?>%">
                                    <span class="sr-only">50% Complete (success)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>                
            </ul>
            <!-- /.dropdown-tasks -->
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <?= $this->session->userdata("usuario_nombre") ?> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li class="hide"><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li class="hide"><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider hide"></li>
                <li><a href="<?= base_url() ?>login/cerrarsession"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search hide">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                </li>                
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>
                        Institución 
                        <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li class="hide">
                            <a href="<?= base_url() ?>instituciones">Ver</a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?>usuarios/">Usuarios</a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?>sedes">Sedes</a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?>sedes/admin" class="hide">Sedes Admin</a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?>docentes/">Docentes</a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?>correos/">Enviar Correo a todos los docentes</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>
                        Lo que es
                        <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <?= anchor("estudiantes/matriculas", "Matricula") ?>
                            <!-- Para esto necesito:
                            estudiantes, cursos, 
                            -->
                        </li>
                        <li>
                            <?= anchor("estudiantes/boletin", "Boletin") ?>
                            <!-- para esto necesito:
                            estudiantes, notas, juicios, periodo, 
                            -->
                        </li>

                        <li>
                            <?= anchor("estudiantes/pagos", "Pagos") ?>
                            <!-- Para esto necesito:
                            Estudiantes, deuda, facturas
                            -->
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>
                        Estudiantes
                        <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <?= anchor("estudiantes/", "Lista De estudiantes") ?>
                        </li>
                        <li>
                            <?= anchor("estudiantes/boletin", "Boletines") ?> 
                        </li>
                        <li>
                            <?= anchor("estudiantes/promocion", "Promocion") ?>
                            <!-- para esto necesito:
                            estudiantes, notas, juicios, periodo, 
                            -->
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-bell fa-fw"></i>
                        Docentes
                        <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <?= anchor("juiciosAdmin/juiciosDocentes", "Juicios de cada docente") ?>
                        </li>                        
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-wrench fa-fw"></i>
                        Datos de institucion
                        <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <?= anchor("cursos/", "Cursos") ?>
                        </li>
                        <li>
                            <?= anchor("cursos/grados", "Grados") ?>
                        </li>
                        <li>
                            <?= anchor("cursos/grupos", "Grupos") ?>
                        </li>
                        <li>
                            <?= anchor("cursos/jornadas", "Jornadas") ?>
                        </li>
                        <li>
                            <?= anchor("materias/areas", "Areas") ?>
                        </li>
                        <li>
                            <?= anchor("materias/asignaturas", "Asignaturas") ?>
                        </li>
                        <li>
                            <?= anchor("materias/cargaAcademica", "Carga Academica") ?>
                        </li>
                        <li>
                            <?= anchor("configuracion/", "Configuracion") ?>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>

<!-- jQuery Version 1.11.0 -->
<script src="<?= base_url() ?>js/jquery-1.11.0.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url() ?>js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?= base_url() ?>js/plugins/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?= base_url() ?>js/sb-admin-2.js"></script>
<!-- JavaScript de la aplicacion-->
<script src="<?= base_url() ?>js/nabu-notas.js"></script>
<script src="<?= base_url() ?>js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".table").DataTable({
            "ordering": true,
            language: {
                "emptyTable": "No hay datos para mostrar",
                "info": "del _START_ al _END_ de _TOTAL_ datos",
                "search": "Buscar:",
                "lengthMenu": "Mostar _MENU_ Entradas",
                "infoFiltered":   "(filtrado de _MAX_ datos)",
                paginate: {
                    previous: '‹',
                    next: '›'
                }
            }
        });
    });
</script>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="row">