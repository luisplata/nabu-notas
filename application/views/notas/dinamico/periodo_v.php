<div class="col-xs-4">    
    <table class="table table-hover">
        <thead>
        <td>
            Periodo
        </td>
        <td>
            Fecha Inicio
        </td>
        <td>
            Fecha Final
        </td>
        <td>
            Accion
        </td>
        </thead>
        <tbody>
            <?php
            $fecha_mas_alta = "";
            $aux;
            //print_r($peridos);
            foreach ($peridos as $value) {
                $fecha = new DateTime($value->fecha_inicio);
                //print_r(date_format($fecha, 'Y-m-d H:i:s'));
                $aux = $value->fecha_final;
                if ($aux > $fecha_mas_alta) {
                    $fecha_mas_alta = $aux;
                }
                ?>

                <?php
                if ($this->configuracion_model->periodo()['id'] == $value->id) {
                    ?>  
                    <tr class="bg-success">
                        <?php
                    } else {
                        ?>  
                    <tr>
                        <?php
                    }
                    ?>                
                    <td>
                        <?= $value->periodo ?>
                    </td>
                    <td>
                        <?= $value->fecha_inicio ?>
                    </td>
                    <td>
                        <?= $value->fecha_final ?>
                    </td>
                    <td>
                        <?= anchor("periodos/periodoActual/" . $value->id, "Periodo Actual", "class='btn btn-default'") ?>
                    </td>
                </tr>
                <?php
            }
            //echo $fecha_mas_alta;
            ?>
        </tbody>
    </table>
</div>

<div class="col-xs-6">    
    <?= form_open("periodos/guardar") ?>
    <div class="form-group">
        <span class="h3">Periodo</span>
        <input type="number" required class="form-control" name="periodo" placehover="Periodo" min="0" />
    </div>
    <div class="form-group">
        <span class="h3">
            Fecha Inicial
        </span>
        <?php if ($fecha_mas_alta == "") { ?>
            <input type="date" value="<?= date("Y-m-d") ?>" class="form-control" name="fecha_inicio" required />
        <?php } else { ?>
            <input type="date" value="<?= $fecha_mas_alta ?>" class="form-control" name="fecha_inicio" required />
        <?php }
        ?>

    </div>
    <div class="form-group">
        <span class="h3">
            Fecha Final
        </span>
        <input type="date" class="form-control" value="<?= $fecha_mas_alta ?>" name="fecha_final" required />
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-default" />
    </div>
</div>
