<div class="col-xs-5 table-responsive">
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <td>
                    Descripcion    
                </td>
                <td>
                    Asignatura
                </td>                
            </tr>

        </thead>
        <tbody>            
            <?php foreach ($juicios as $juicio) { ?>
                <tr>                   
                    <td>
                        <?= $juicio->descripcion ?>
                    </td>
                    <td>
                        <?= $juicio->nombre ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>    
</div>
<div class="col-xs-2">
    <?= anchor("juiciosAdmin/juiciosDocentes", "Atras", "class='btn btn-success'") ?>
</div>