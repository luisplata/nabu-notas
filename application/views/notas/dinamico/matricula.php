<div class="">
    <h2>
        Matricular a estudiante
    </h2>
    <?= form_open("estudiantes/guardar") ?>
    <div class="">
        <div class="form-group">
            <label>
                Nombre
            </label>
            <input type="text" class="form-control" name="nombre" />
        </div>
        <div class="form-group">
            <label>
                Apellido
            </label>
            <input type="text" class="form-control" name="apellido" />
        </div>
        <div class="form-group">
            <label>
                Documento
            </label>
            <input type="number" class="form-control" name="documento" />
        </div>
        <div class="form-group">
            <label>
                Fecha De Nacimiento
            </label>
            <input type="date" value="<?= date("Y-m-d") ?>" class="form-control" name="fecha_nacimiento" />
        </div>
        <label>
            Genero
        </label>
        <div class="form-group radio">
            <label><input type="radio" name="genero" value="M" />Masculino</label>
            <label><input type="radio" name="genero" value="F" />Femenno</label>
        </div>
        <div class="form-group">
            <label>
                Curso
            </label>
            <select name="curso" class="form-control">
                <?php foreach ($cursos as $value) { ?>
                    <option value="<?= $value->grado_id ?>-<?= $value->grupo_id ?>-<?= $value->jornada_id ?>">
                        <?= $value->grado ?>-<?= $value->grupo ?>-<?= $value->jornada ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Guardar" />
            <input type="reset" class="btn btn-warning" value="Borrar" />
        </div>
    </div>
</form>
</div>