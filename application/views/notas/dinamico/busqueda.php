<div class="col-lg-4">
    <?= form_open("estudiantes/buscarBoletin") ?>
    <div class="form-group">
        <label>
            Curso
        </label>
        <select class="form-control" name="curso">
            <option></option>
            <?php foreach ($cursos as $value) { ?>
                <option value="<?= $value->grado_id ?>-<?= $value->grupo_id ?>-<?= $value->jornada_id ?>"><?= $value->grado ?>-<?= $value->grupo ?>-<?= $value->jornada ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group">
        <label>
            Codigo
        </label>
        <input type="text" name="codigo" placeholder="Codigo" class="form-control" />
    </div>
    <div class="form-group">
        <input type="submit" value="Buscar" class="btn btn-success" />
        <input type="reset" value="Resetear" class="btn btn-warning" />
    </div>
</form>
</div>
