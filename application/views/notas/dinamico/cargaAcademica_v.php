<div class="">
    <div class="col-md-6 table-responsive">
        <h2 class="header">Cargas Academicas</h2>
        <table class="table table-hover">
            <thead>
                <tr>
                    <td>
                        Docente
                    </td>
                    <td>
                        Asignatura
                    </td>
                    <td>
                        Curso
                    </td>
                    <td>
                        Accion
                    </td>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($cargasAcademicas as $value) { ?>
                    <tr>
                        <td>
                            <?= $value->docente_nombre ?> <?= $value->docente_apellido ?>
                        </td>
                        <td>
                            <?= $value->asignatura_nombre ?> 
                            
                        </td>
                        <td>
                            <?= $value->grado ?>-<?= $value->grupo ?>
                            -<?= $value->jornada ?>
                        </td>
                        <td>

                        </td>
                    </tr>
                <?php } ?>

            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <h2>
            Nueva Carga academica
        </h2>
        <?= form_open("materias/guardarCargaAcademica") ?>
        <div class="">
            <div class="form-group">
                <label>
                    Docente
                </label>
                <select name="docente_id" size="10" class="form-control" >
                    <?php foreach ($docentes as $value) { ?>
                        <option value="<?= $value->id ?>"><?= $value->nombre ?> <?= $value->apellido ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>
                    Asignatura
                </label>
                <select name="asignatura_id" size="10" class="form-control" >
                    <?php foreach ($asignaturas as $value) { ?>
                        <option value="<?= $value->id ?>"><?= $value->nombre ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>
                    Curso
                </label>
                <select name="curso" size="10" class="form-control" >
                    <?php foreach ($cursos as $value) { ?>
                        <!--formato del curso: grado-grupo-jornada-->
                        <option value="<?= $value->grado_id ?>-<?= $value->grupo_id ?>-<?= $value->jornada_id ?>">
                            <?= $value->grado ?>-<?= $value->grupo ?>-<?= $value->jornada ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Guardar" />
                <input type="reset" class="btn btn-warning" value="Borrar" />
            </div>
        </div>
        </form>
    </div>
</div>