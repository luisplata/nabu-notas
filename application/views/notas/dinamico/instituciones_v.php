<div class="">
    <div class="col-md-6">
        <h2 class="header">Instituciones</h2>
        <table class="table table-responsive">
            <thead>
                <tr>
                    <td>
                        Nombre
                    </td>
                    <td>
                        Correo
                    </td>
                    <td>
                        Estado
                    </td>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($instituciones as $value) { ?>
                    <tr>
                        <td>
                            <?= $value->nombre ?>
                        </td>
                        <td>
                            <?= $value->email ?>
                        </td>
                        <td>
                            <?php
                            if ($value->activa) {
                                echo anchor("instituciones/activar", "Activar", "class='btn btn-success'");
                            } else {
                                echo anchor("instituciones/desactivar", "Desactivar", "class='btn btn-danger'");
                            }
                            ?>
                        </td>
                    </tr>
                <?php } ?>

            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <h2>
            Nueva Institucion
        </h2>
        <?= form_open("instituciones/guardar") ?>
        <div class="">
            <div class="form-group">
                <label>
                    Nombre*
                </label>
                <input type="text" class="form-control" name="nombre" required/>
            </div>
            <div class="form-group">
                <label>
                    Email*
                </label>
                <input type="email" class="form-control" name="email" required/>
            </div>
            <div class="form-group">
                <label>
                    Direccion
                </label>
                <input type="text" class="form-control" name="direccion" />
            </div>
            <div class="form-group">
                <label>
                    Telefono
                </label>
                <input type="number" class="form-control" name="telefono" />
            </div>
            <div class="form-group">
                <label>
                    DANE
                </label>
                <input type="text" class="form-control" name="dane" />
            </div>
            <div class="form-group">
                <label>
                    Resolucion
                </label>
                <input type="text" class="form-control" name="resolucion" />
            </div>
            <div class="form-group">
                <label>
                    NIT
                </label>
                <input type="text" class="form-control" name="nit" />
            </div>
            <div class="form-group">
                <label>
                    Codigo Icfes
                </label>
                <input type="text" class="form-control" name="codigo_icfes" />
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Guardar" name="codigo_icfes" />
                <input type="reset" class="btn btn-warning" value="Borrar" name="codigo_icfes" />
            </div>
        </div>
        </form>
    </div>
</div>