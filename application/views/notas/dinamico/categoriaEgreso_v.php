<div>
    <div class="col-md-6">
        <table class="table table-responsive">
            <thead>
                <tr>
                    <td>
                        Nombre
                    </td>
                    <td>
                        Descripcion
                    </td>
                    <td>
                        Accion
                    </td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($categoriaEgresos as $value) { ?>
                    <tr>
                        <td>
                            <?= $value->nombre ?>
                        </td>
                        <td>
                            <?= $value->descripcion ?>
                        </td>
                        <td>
                            <?= anchor("cursos/eliminarCurso/", "Eliminar", "class='ask-plain'") ?>
                            <?= anchor("", "Editar") ?>

                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <h2>
            Nueva Categoria de Egreso
        </h2>
        <?= form_open("categoriaEgresos/guardarCategoriaEgreso") ?>
        <div class="form-group">
            Nombre
            <input type="text" name="nombre" class="form-control" />
        </div>
        <div class="form-group">
            Descripcion
            <input type="text" name="descripcion" class="form-control" />
        </div>
        <div class="form-group">
            <input type="submit" value="Guardar" class="btn btn-success" />
            <input type="reset" value="Borrar" class="btn btn-warning" />
        </div>
        </form>
    </div>
</div>