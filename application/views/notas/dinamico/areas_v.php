<div class="">
    <div class="col-md-6 table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <td>
                        Nombre
                    </td>
                    <td>
                        Codigo
                    </td>
                    <td>
                        Accion
                    </td>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($areas as $value) { ?>
                    <tr>
                        <td>
                            <?= $value->nombre ?>
                        </td>
                        <td>
                            <?= $value->codigo ?>
                        </td>
                        <td>
                            <?= anchor("materias/eliminarArea/".$value->id, "Eliminar","class = 'ask-plain'") ?>
                            <?= anchor("materias/modificarArea/".$value->id, "Modificar") ?>
                            
                        </td>
                    </tr>
                <?php } ?>

            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <h2>
            Nueva Area
        </h2>
        <?= form_open("materias/guardarArea") ?>
        <div class="">
            <div class="form-group">
                <label>
                    Nombre
                </label>
                <input type="text" class="form-control" name="nombre" required/>
            </div>
            <div class="form-group">
                <label>
                    Codigo
                </label>
                <input type="text" class="form-control" name="codigo" required/>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Guardar" />
                <input type="reset" class="btn btn-warning" value="Borrar" />
            </div>
        </div>
        </form>
    </div>
</div>