<style type="text/css">
    .lista-usuario{
        padding-bottom: 25px;
    }
</style>
<div class="col-xs-6">
    <div class="list-group">
        <a href="#" class="list-group-item active">
            Lista de Usuarios
        </a>
        <?php foreach ($administradores as $value) { ?>       
            <div href="#" class="list-group-item lista-usuario">                
                <img class="hide" src="<?= base_url() ?>img/persona.jpg">
                <span><?= $value->nombre ?></span>
                <span><?= $value->apellido ?></span>
                <br/>
                <span><?= $value->email ?></span>
                <div class="pull-right">
                    <button class="btn btn-info editar" data-id="<?= $value->id ?>">Editar</button>                    
                    <?= anchor("usuarios/eliminar/$value->id", "Eliminar", "class='btn btn-warning eliminar'") ?>
                </div>                
            </div>
            <?php
        }
        ?>
    </div>    
</div>
<div class="col-xs-6">

    <?= form_open("usuarios/guardarUsuario", "class='row'") ?>
    <div class="col-xs-6">
        <div class="form-group">
            Nombre
            <input type="text" name="nombre" class="form-control"/>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            Apellido
            <input type="text" name="apellido" class="form-control"/>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            Documento
            <input type="number" name="documento" class="form-control"/>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            Email
            <input type="email" name="email" class="form-control"/>
            <input name="id" class="hide"/>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            Telefono
            <input type="tel" name="telefono" class="form-control"/>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Guardar" />
        </div>
    </div>
</form>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="edicionUsuario">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Editar Usuario</h4>
            </div>
            <?= form_open("usuarios/Modificar", "") ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            Nombre
                            <input type="text" name="nombre" id="nombre" class="form-control"/>
                            <input type="hidden" name="id" id="id" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            Apellido
                            <input type="text" name="apellido" id="apellido" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            Documento
                            <input type="number" name="documento" disabled="disabled" id="documento" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            Email
                            <input type="email" readonly="readonly" name="email" id="email" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            Telefono
                            <input type="tel" name="telefono" id="telefono" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(document).on("click", ".editar", function () {
        $("#edicionUsuario").modal();
        $.ajax({
            method: "POST",
            url: "<?= base_url() ?>usuarios/GetUsuarioJson/" + this.dataset.id,
            data: {id: this.dataset.id}
        }).done(function (json) {
            //colocamos los datos donde van
            var usuario = JSON.parse(json);
            $("#id").val("");
            $("#id").val(usuario.id);
            $("#nombre").val("");
            $("#nombre").val(usuario.nombre);
            $("#apellido").val("");
            $("#apellido").val(usuario.apellido);
            $("#documento").val("");
            $("#documento").val(usuario.documento);
            $("#email").val("");
            $("#email").val(usuario.email);
            $("#telefono").val("");
            $("#telefono").val(usuario.telefono);
        });
    });
</script>