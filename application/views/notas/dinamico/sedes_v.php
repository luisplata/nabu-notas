<div class="col-xs-12">    
    <div class="row">        
        <button class="btn btn-info nuevaSede">Nueva Sede</button>
    </div>
    <hr/>
    <div class="row">
        <table class="table table-responsive table-striped">
            <thead>
                <tr>
                    <td>
                        Nombre
                    </td>
                    <td>
                        Direccion
                    </td>
                    <td>
                        Institucion
                    </td>
                    <td>
                        Email
                    </td>
                    <td>
                        Accion
                    </td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($sedes as $value) { ?>
                    <tr>
                        <td>
                            <?= $value->sede_nombre ?>
                        </td>
                        <td>
                            <?= $value->sede_direccion ?>
                        </td>
                        <td>
                            <?= $value->institucion_nombre ?>
                        </td>
                        <td>
                            <?= $value->sede_email ?>
                        </td>
                        <td>
                            <?php
                            if ($value->sede_activa == 1) {
                                echo anchor("sedes/activar/0/" . $value->sede_id, "Desactivar", "class='btn btn-info'");
                            } else {
                                echo anchor("sedes/activar/1/" . $value->sede_id, "Activar", "class='btn btn-success'");
                            }
                            ?>
                            <?= anchor("sedes/eliminar/" . $value->sede_id, "Eliminar", "class='btn btn-warning pull-right'") ?>
                        </td>
                    </tr>
                <?php } ?>

            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="ModalNuevaSede" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nueva Sede</h4>
            </div>
            <?= form_open("sedes/guardar") ?>
            <div class="modal-body">

                <div class="form-group">
                    <input type="text" name="nombre" class="form-control" placeholder="Nombre" required/>
                </div>
                <div class="form-group">
                    <input type="text" name="direccion" class="form-control" placeholder="Direccion" required/>
                </div>
                <div class="form-group">
                    <input type="text" name="telefono" class="form-control" placeholder="Telefono" />
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Email" required/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(document).on("click", ".nuevaSede", function () {
        $("#ModalNuevaSede").modal();
    });
</script>
