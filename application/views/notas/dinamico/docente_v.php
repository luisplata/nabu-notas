<button class="btn btn-info ModalNuevoDocente">Nuevo Docente</button>
<hr/>
<div class="col-md-12 table-responsive">    
    <table class="table table-hover">
        <thead>
            <tr>
                <th>
                    Nombre
                </th>
                <th>
                    Apellido
                </th>
                <th>
                    Documento
                </th>
                <th>
                    Email
                </th>
                <th>
                    Telefono
                </th>
                <th>
                    Accion
                </th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ($docentes as $value) { ?>
                <tr>
                    <td>
                        <?= $value->nombre ?>
                    </td>
                    <td>
                        <?= $value->apellido ?>
                    </td>
                    <td>
                        <?= $value->documento ?>
                    </td>
                    <td>
                        <?= $value->email ?>
                    </td>
                    <td>
                        <?= $value->telefono ?>
                    </td>
                    <td>                       
                        <button class="btn btn-info EditarDocente" data-id="<?= $value->id ?>">Editar</button>
                        <?= anchor("docentes/eliminar/$value->id", "Eliminar", "class='btn btn-warning'") ?>
                    </td>
                </tr>
            <?php } ?>

        </tbody>
    </table>
</div>


<div class="modal fade" id="ModalNuevoDocente" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo Docente</h4>
            </div>
            <?= form_open("docentes/guardar") ?>
            <div class="modal-body">
                <div class="form-group">
                    <label>
                        Nombre
                    </label>
                    <input type="text" name="nombre" class="form-control" required/>
                </div>
                <div class="form-group">
                    <label>
                        Apellido
                    </label>
                    <input type="text" name="apellido" class="form-control" required />
                </div>
                <div class="form-group">
                    <label>
                        Documento
                    </label>
                    <input type="number" name="documento" class="form-control" required/>
                </div>
                <div class="form-group">
                    <label>
                        Sexo
                    </label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="sexo" value="Femenino" checked />
                            Femenino
                        </label>
                        <label>
                            <input type="radio" name="sexo" value="Masculino" />
                            Masculino
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label>
                        Email
                    </label>
                    <input type="email" name="email" class="form-control" required/>
                </div>
                <div class="form-group">
                    <label>
                        Telefono
                    </label>
                    <input type="number" name="telefono" class="form-control" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(document).on("click", ".ModalNuevoDocente", function () {
        $("#ModalNuevoDocente").modal();
    });
</script>