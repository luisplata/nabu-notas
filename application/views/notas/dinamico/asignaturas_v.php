<div class="">
    <div class="col-md-6 table-responsive">
        <h2 class="header">Asignaturas de la sede</h2>
        <table class="table">
            <thead>
                <tr>
                    <td>
                        Nombre
                    </td>
                    <td>
                        Codigo
                    </td>
                    <td>
                        Area
                    </td>
                    <td>
                        Accion
                    </td>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($asignaturas as $value) { ?>
                    <tr>
                        <td>
                            <?= $value->nombre ?>
                        </td>
                        <td>
                            <?= $value->codigo ?>
                        </td>
                        <td>
                            <?= $value->area_nombre ?>
                        </td>
                        <td>
                            <?= anchor("materias/eliminarAsignatura".$value->id,"Eliminar") ?>
                            <?= anchor("".$value->id,"Actualizar") ?>
                        </td>
                    </tr>
                <?php } ?>

            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <h2>
            Nueva Asignatura
        </h2>
        <?= form_open("materias/guardarAsignatura") ?>
        <div class="">
            <div class="form-group">
                <label>
                    Nombre
                </label>
                <input type="text" class="form-control" name="nombre" />
            </div>
            <div class="form-group">
                <label>
                    Codigo
                </label>
                <input type="text" class="form-control" name="codigo" />
            </div>
            <div class="form-group">
                <label>
                    Area
                </label>
                <select name="area_id" class="form-control" >
                    <?php foreach ($areas as $value) { ?>
                        <option value="<?= $value->id ?>"><?= $value->nombre ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Guardar" />
                <input type="reset" class="btn btn-warning" value="Borrar" />
            </div>
        </div>
        </form>
    </div>
</div>