<div>
    <div class="col-md-6">
        <table class="table table-responsive">
            <thead>
                <tr>
                    <td>
                        Nombre
                    </td>
                    <td>
                        Codigo
                    </td>
                    <td>
                        Accion
                    </td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($grupos as $value) { ?>
                    <tr>
                        <td>
                            <?= $value->nombre ?>
                        </td>
                        <td>
                            <?= $value->codigo ?>
                        </td>
                        <td>
                            <?= anchor("cursos/eliminarGrupo/".$value->id,  "Eliminar","class = 'ask-plain'") ?>
                            <?= anchor("cursos/modificaGrupo/".$value->id, "Modificar") ?>
                        </td>
                    </tr>
                <?php } ?>

            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <h2>
            Nuevo Grupo
        </h2>
        <?= form_open("cursos/guardarGrupo") ?>
        <div class="form-group">
            <input type="text" name="nombre" class="form-control" placeholder="Nombre" required/>
        </div>
        <div class="form-group">
            <input type="text" name="codigo" class="form-control" placeholder="Codigo" required/>
        </div>
        <div class="form-group">
            <input type="submit" value="Guardar" class="btn btn-success" />
            <input type="reset" value="Borrar" class="btn btn-warning" />
        </div>
        </form>
    </div>
</div>