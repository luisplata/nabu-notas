<div>
    <?php foreach ($curso as $actual) { ?>
        <?= form_open("cursos/actualizarCurso") ?>
        <div class="form-group">
            grado
            <input type="text" class="" disabled value="<?= $actual->grado ?>" />
            <input type="hidden" class="" readonly name="grado_id" value="<?= $actual->grado_id ?>" />
        </div>
        <div class="form-group">
            grupo
            <input type="text" class="disabled" disabled value="<?= $actual->grupo ?>" />
            <input type="hidden" class="disabled" readonly name="grupo_id" value="<?= $actual->grupo_id ?>" />
        </div>
        <div class="form-group">
            jornada
            <input type="text" class="disabled" disabled value="<?= $actual->jornada ?>" /> 
            <input type="hidden" class="disabled" readonly name="jornada_id" value="<?= $actual->jornada_id ?>" /> 
        </div>
        <div class="form-group">
            Curso para promocion
            <select name="promocion" >
                <?php foreach ($cursos as $value) { ?>
                    <?php foreach ($promociones as $value1) { ?>
                        <?php if ($value->grado_codigo == $value1->codigo) { ?>
                            <option value="<?= $value->grado_id ?>-<?= $value->grupo_id ?>-<?= $value->jornada_id ?>">
                                <?= $value->grado ?>-<?= $value->grupo ?>-<?= $value->jornada ?>
                            </option>
                        <?php } else { ?>

                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </select>
        </div>

        <div class="form-group">
            <input type="submit" value="Guardar" class="btn btn-success" />
            <input type="reset" value="Borrar" class="btn btn-warning" />
        </div>
    </form>
<?php } ?>
</div>