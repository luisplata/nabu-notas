<div>
    <div class="col-md-6">
        <table class="table table-responsive">
            <thead>
                <tr>
                    <td>
                        Nombre
                    </td>
                    <td>
                        Sede
                    </td>
                    <td>
                        Accion
                    </td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($cursos as $value) { ?>
                    <tr>
                        <td>
                            <?= $value->grado ?>-<?= $value->grupo ?>-<?= $value->jornada ?>
                            <?= $arreglo[]=$value->grado?>
                        </td>
                        <td>
                            <?= $value->sede ?>
                        </td>
                        <td>
                            <?= anchor("cursos/eliminarCurso/","Eliminar","class='ask-plain'")?>
                        <?= anchor("", "Editar")?>
                  
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <h2>
            Nuevo Curso
        </h2>
        <?= form_open("cursos/guardarCurso") ?>
        <div class="form-group">
            grado
            <select name="grado_id">
                <?php foreach ($grados as $value) { ?>
                    <option value="<?= $value->id ?>">
                        <?= $value->nombre ?> - <?= $value->codigo ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            grupo
            <select name="grupo_id">
                <?php foreach ($grupos as $value) { ?>
                    <option value="<?= $value->id ?>">
                        <?= $value->nombre ?> - <?= $value->codigo ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            jornada
            <select name="jornada_id">
                <?php foreach ($jornadas as $value) { ?>
                    <option value="<?= $value->id ?>">
                        <?= $value->nombre ?> - <?= $value->horario ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <input type="submit" value="Guardar" class="btn btn-success" />
            <input type="reset" value="Borrar" class="btn btn-warning" />
        </div>
        </form>
    </div>
</div>