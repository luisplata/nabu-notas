<div>
    <h2 class="text-uppercase">
        Datos de Configuracion
    </h2>
    <?= form_open("configuracion/guardar") ?>
    <?php foreach ($configuracion as $value) { ?>
        <div class="col-lg-3">
            <div class="form-group">
                Escudo
                <img src="<?= base_url() . $value->escudo ?>"  class="img-thumbnail" width="100" height="100">
            </div>
            <?php if ($value->eliminar) { ?>
                <div class="radio">
                    Eliminar datos<br/>
                    <label>
                        <input checked type="radio" name="eliminar" class="" value="1" />
                        Si
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="eliminar" class="" value="0" />
                        No
                    </label>
                </div>
            <?php } else { ?>
                <div class="radio">
                    Eliminar datos<br/>
                    <label>
                        <input type="radio" name="eliminar" class="" value="1" />
                        Si
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input checked type="radio" name="eliminar" class="" value="0" />
                        No
                    </label>
                </div>
            <?php } ?>

            <?php if ($value->imprimir) { ?>
                <div class="radio">
                    Imprimir Factura<br/>
                    <label>
                        <input checked type="radio" name="imprimir" class="" value="1" />
                        Si
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="imprimir" class="" value="0" />
                        No
                    </label>
                </div>
            <?php } else { ?>
                <div class="radio">
                    Imprimir Factura<br/>
                    <label>
                        <input type="radio" name="imprimir" class="" value="1" />
                        Si
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input checked type="radio" name="imprimir" class="" value="0" />
                        No
                    </label>
                </div>
            <?php } ?>
            <?php if ($value->calificacion_decimal) { ?>
                <div class="radio">
                    Calificacion Decimal<br/>
                    <label>
                        <input checked type="radio" name="calificacion_decimal" class="" value="1" />
                        Si
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="calificacion_decimal" class="" value="0" />
                        No
                    </label>
                </div>
            <?php } else { ?>
                <div class="radio">
                    Califcalificacion Decimal<br/>
                    <label>
                        <input type="radio" name="calificacion_decimal" class="" value="1" />
                        Si
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input checked type="radio" name="calificacion_decimal" class="" value="0" />
                        No
                    </label>
                </div>
            <?php } ?>
            <div class="form-group">
                Intereces
                <div class="input-group">
                    <input type="number" step="any" value="<?= $value->interes * 100 ?>" class="form-control" min="0" max="100" name="interes">
                    <span class="input-group-addon">%</span>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                Calificacion Baja
                <input type="text" value="<?= $value->calificacion_baja ?>" class="form-control" maxlength="2" name="calificacion_baja"/>
            </div>
            <div class="form-group">
                Calificacion Basico
                <input type="text" value="<?= $value->calificacion_basico ?>" class="form-control" maxlength="2" name="calificacion_basico"/>
            </div>
            <div class="form-group">
                Calificacion Alta
                <input type="text" value="<?= $value->calificacion_alta ?>" class="form-control" maxlength="2" name="calificacion_alta"/>
            </div>
            <div class="form-group">
                Calificacion Superior
                <input type="text" value="<?= $value->calificacion_superior ?>" class="form-control" maxlength="2" name="calificacion_superior"/>
            </div>
            <div class="form-group">
                Calificacion Maxima
                <input name="calificacion_maxima" value="<?= $value->calificacion_maxima ?>" class="form-control" min="0" type="number" />
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                Porcentaje Bajo
                <div class="input-group">
                    <input type="number" step="any" value="<?= $value->porcentaje_bajo * 100 ?>" class="form-control" max="100" min="0" name="porcentaje_bajo" />
                    <span class="input-group-addon">%</span>
                </div>
            </div>
            <div class="form-group">
                Porcentaje Medio
                <div class="input-group">
                    <input type="number" step="any" value="<?= $value->porcentaje_medio * 100 ?>" class="form-control" max="100" min="0" name="porcentaje_medio" />
                    <span class="input-group-addon">%</span>
                </div>
            </div>
            <div class="form-group">
                Porcentaje Alto
                <div class="input-group">
                    <input type="number" step="any" value="<?= $value->porcentaje_alto * 100 ?>" class="form-control" max="100" min="0" name="porcentaje_alto" />
                    <span class="input-group-addon">%</span>
                </div>

            </div>
            <div class="form-group">
                Porcentaje Superior
                <div class="input-group">
                    <input type="number" step="any" value="<?= $value->porcentaje_superior * 100 ?>" class="form-control" max="100" min="0" name="porcentaje_superior" />
                    <span class="input-group-addon">%</span>
                </div>
            </div>
            <div class="form-group">
                Periodo Actual
                <input type="number" value="<?= $value->periodo_actual ?>" class="form-control" disabled name="periodo_actual"/>
            </div>
            <div class="form-group">
                Fecha Limite mensualidad
                <input type="number" value="<?= $value->fecha_mensualidad ?>" class="form-control" min="0" max="30" name="fecha_mensualidad" />
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                Sueldo Base
                <input type="number" value="<?= $value->sueldo_base ?>" class="form-control" name="sueldo_base" />
            </div>
            <div class="form-group">
                Factura Numero
                <input type="number" value="<?= $value->factura_numero ?>" class="form-control" disabled name="factura_numero" />
            </div>
            <div class="form-group">
                Meses Por Año
                <input type="number" value="<?= $value->meses_por_anio ?>" class="form-control" name="meses_por_anio" min="1" max="12" />
            </div>
            <div class="form-group">
                Numero de Notas
                <input type="number" value="<?= $value->numero_notas ?>" class="form-control" name="numero_notas" min="1">
            </div>
            <div class="form-group">
                Producto a cobrar en la matricula
                <select name="producto_id" class="form-control">
                    <?php foreach ($productos as $value2) { ?>
                        <?PHP if ($value2->id == $value->producto_matricula) { ?>
                            <option selected value="<?= $value2->id ?>">
                                <?= $value2->nombre ?>
                            </option>
                        <?php } else { ?>
                            <option value="<?= $value2->id ?>">
                                <?= $value2->nombre ?>
                            </option>
                        <?php }
                        ?>                        
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                Consecutivo
                <input type="number" value="<?= $value->consecutivo ?>" class="form-control" name="consecutivo" disabled />
            </div>
            <div class="form-group">
                Categoria de egreso de nomina
                <select name="categoria_egreso" class="form-control">
                    <?php foreach ($categoriaEgresos as $value4) { ?>
                        <?PHP if ($value4->id == $value->categoria_egreso) { ?>
                            <option selected value="<?= $value4->id ?>">
                                <?= $value4->nombre ?>
                            </option>
                        <?php } else { ?>
                            <option value="<?= $value4->id ?>">
                                <?= $value4->nombre ?>
                            </option>
                        <?php }
                        ?>                        
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <?= anchor("configuracion/periodos", "Periodos", "class='btn btn-default'") ?>
            </div>
            <div class="">
                <input class="btn btn-success" type="submit" value="Guardar" />
                <input class="btn btn-warning" type="reset" value="Resetear" />
            </div>
        </div>
    <?php } ?>
</form>
<p>

</p>
</div>