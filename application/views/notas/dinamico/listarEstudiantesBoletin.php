<div class="col-lg-12">
    <h2>
        Estudiantes
    </h2>
    <table>
        <thead>
            <tr>
                <td>
                    Apellido    
                </td>
                <td>
                    Nombre
                </td>
                <td>
                    Codigo
                </td>
                <td>
                    Accion
                </td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($estudiantes as $value) { ?>

                <tr>
                    <td>
                        <?= $value->apellido ?>
                    </td>
                    <td>
                        <?= $value->nombre ?>
                    </td>
                    <td>
                        <?= $value->codigo ?>
                    </td>
                    <td>
                        <a target="_blank" class="btn btn-default" href="<?= base_url() ?>estudiantes/generarBoletin/<?= $value->id ?>">Boletin</a>
                    </td>

                </tr>

            <?php } ?>
        </tbody>
    </table>
</div>