<div>
    <div class="col-md-6">
        <table class="table table-responsive">
            <thead>
                <tr>
                    <td>
                        Nombre
                    </td>
                    <td>
                        Horario
                    </td>
                    <td>
                        Accion
                    </td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($jornadas as $value) { ?>
                    <tr>
                        <td>
                            <?= $value->nombre ?>
                        </td>
                        <td>
                            <?= $value->horario ?>
                        </td>
                        <td>
                            <?= anchor("cursos/eliminarJornada/".$value->id, "Eliminar","class = 'ask-plain'") ?>
                            <?= anchor("cursos/modificaJornada/".$value->id, "Modificar") ?>
                        </td>
                    </tr>
                <?php } ?>

            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <h2>
            Nueva Jornada
        </h2>
        <?= form_open("cursos/guardarJornada") ?>
        <div class="form-group">
            <input type="text" name="nombre" class="form-control" placeholder="Nombre" required/>
        </div>
        <div class="form-group">
            <input type="text" name="horario" class="form-control" placeholder="8:00 am - 12:30 PM" required/>
        </div>
        <div class="form-group">
            <input type="submit" value="Guardar" class="btn btn-success" />
            <input type="reset" value="Borrar" class="btn btn-warning" />
        </div>
        </form>
    </div>
</div>