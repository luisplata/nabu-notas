</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery Version 1.11.0 -->
<!--<script src="<?= base_url() ?>js/jquery-2.1.1.min.js"></script>-->


<!-- jQuery Version 1.11.0 -->
<script src="<?= base_url() ?>js/jquery-1.11.0.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url() ?>js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<!--<script src="<?= base_url() ?>js/plugins/metisMenu/metisMenu.min.js"></script>-->

<!-- Morris Charts JavaScript -->
<!--<script src="<?= base_url() ?>js/plugins/morris/raphael.min.js"></script>
<script src="<?= base_url() ?>js/plugins/morris/morris.min.js"></script>
<script src="<?= base_url() ?>js/plugins/morris/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<!--<script src="<?= base_url() ?>js/sb-admin-2.js"></script>-->

<!-- DataTables JavaScript -->
<script src="<?= base_url() ?>js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>js/plugins/dataTables/dataTables.bootstrap.js"></script>


<!-- Script de la facturadora -->

<script>
    $(document).ready(function () {

        //creando la tabla bacana

        $("#tablaFactura").dataTable();


        //metodo para hacer la cuenta var pathname = window.location.pathname;
        //alert(window.location.pathname);
        // servidor nuevo controlador
        if (window.location.pathname === "/facturas"
                || window.location.pathname === "/facturas/") {
            $("#totalDeTodo").val(0);
            //haciendo el ajax desde aqui        
            $("#formulario_facturadora").submit(function () {
                $.ajax({
                    data: {id: $("#codigo_de_barra").val()},
                    success: function (data, textStatus, jqXHR) {
                        $("#productos").append(data);
                        $("#total").load();
                        sacarTotal();
                        $("#codigo_de_barra").val(" ");
                    },
                    url: "<?= base_url() ?>ajax/",
                    beforeSend: function (xhr) {
                    },
                    type: "post"
                            //fin del ajax
                });
                //guardamos todos los resultados en pantalla
                return false;
                //cierre del Submit
            });
            //fin de ajax
            //
            //
            //Para sacar el total de los productos
            function sacarTotal() {
                var totalDeTodo = 0;
                var productos = document.getElementsByName("precio[]");
                for (i = 0; i < productos.length; i++)
                {
                    totalDeTodo += parseInt(productos[i].value);
                    //$("#total").text(totalDeTodo);
                    $("#totalDeTodo").val(totalDeTodo);
                }
            }
        }
        //fin del ready
    });
</script>

</body>

</html>