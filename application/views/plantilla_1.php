<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?=$this->config->item("NOMBREAPP")?></title>
        <!-- CSS de Bootstrap -->
        <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet" media="screen">

        <!-- librerías opcionales que activan el soporte de HTML5 para IE8 -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div>
            <div class="col-lg-3">

                <img class="img-thumbnail" width="100" height="100" src="<?= base_url() ?><?= $this->session->userdata("escudo") ?>">
                <h3>
                    Factura No <?= $this->session->userdata("factura_numero") + 1 ?> <br/> 
                    <small><?= date("Y-m-d H:i:s") ?></small>
                </h3>
                <h5>
                    <small>
                        Colegio: <?= $this->session->userdata("institucion_nombre") ?><br/>
                        Direccion: <?= $this->session->userdata("institucion_direccion") ?><br/>
                        Telefono: <?= $this->session->userdata("institucion_telefono") ?><br/>
                        Acudiente: 
                    </small>
                </h5>

                <div>
                    <table class="table table-condensed">
                        <tr><?php $totalDeTodo = 0; //el que tiene el total de todo                                  ?>
                            <th>
                                ID
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Precio
                            </th>
                            <th>
                                Total
                            </th>
                        </tr>
                        <?php
                        foreach ($productos as $value) {
                            $totalDeTodo += $value['precio'];
                            ?> 
                            <tr>
                                <td>
                                    <?= $value["id"] ?>
                                </td>
                                <td>
                                    <?= $value["nombre"] ?>
                                </td>
                                <td>
                                    <?= $value["precio"] ?>
                                </td>
                                <td>
                                    <?= $value["precio"] ?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>

                        <tr>

                            <td colspan="3">
                                Recivido:
                            </td>
                            <td colspan="2" id="recivido">

                            </td>

                        </tr>

                        <tr>
                            <td colspan="3">
                                Total: 
                            </td>
                            <td id="total" colspan="2">
                                <?php echo $totalDeTodo ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                Vuelto:
                            </td>
                            <td colspan="2" id="vuelto">

                            </td>
                        </tr>
                    </table>
                    <div class="text-left">
                        <h5>
                            Datos del cliente<br/>
                            <small>
                                Nombre: <?= $cliente['nombre'] ?><br/>
                                Documento: <?= $cliente['documento'] ?>
                            </small>
                        </h5>
                    </div>
                    <p class="">
                    <h6 class="text-right text-uppercase">
                        <small>
                            upgradec.com
                        </small>
                    </h6>
                    </p>
                </div>

            </div>
        </div>
        <!-- Librería jQuery requerida por los plugins de JavaScript -->
        <script src="http://code.jquery.com/jquery.js"></script>
        <!-- Todos los plugins JavaScript de Bootstrap (también puedes
                     incluir archivos JavaScript individuales de los únicos
                     plugins que utilices) -->
        <script src="<?= base_url() ?>js/bootstrap.min.js"></script>


        <script>
            //para imprimir
            $(document).ready(function () {
                //alert(window.location.hostname);
                var recivido = prompt("Recivio: ");
                $("#recivido").text(recivido);
                var total = $("#total").text();

                var vuelto = recivido - total;
                $("#vuelto").text(vuelto);
                alert("Vuelto: " + vuelto);
                if (vuelto < 0) {
                    alert("Estas reciviendo menos de lo que es");
                }
                window.print();
                //servidor
                $(location).attr('href', "http://" + window.location.hostname + "/facturas/");
                //local
                //                $(location).attr('href', "http://" + window.location.hostname + ":8080/Nabu-Facturadora"
                //                        + "/facturacion/nuevaFactura");
            });
        </script>

    </body>
</html>


