<?php
if ($this->session->userdata("usuario_id") != NULL) {
    redirect("login/modulos");
}
$calificado = FALSE;
$total = 0;
$cantidad = 0;
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Sistema de notas para colegios">
        <meta name="author" content="luis plata">

        <title><?= $this->config->item("NOMBREAPP") ?></title>

        <!-- Bootstrap Core CSS -->
        <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?= base_url() ?>css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?= base_url() ?>css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?= base_url() ?>font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <table class="table-responsive table-hover table">
            <thead>
                <tr>
                    <td>
                        Asignatura
                    </td>
                    <td>
                        Juicio
                    </td>
                    <td>
                        Nota
                    </td>
                </tr>
            </thead>
            <tbody>
                <?php
                $poderCalificar = FALSE;
                echo form_open("juicios/calificarJuicio");
//                print_r($juicios_cargados);
                foreach ($juicios_cargados as $value) {
                    ?>
                    <tr>
                        <td>
                            <?= $value->nombre ?>
                        </td>
                        <td>
                            <?= $value->descripcion ?>
                        </td>
                        <td>
                            <?php
                            /* Cuando no s etenga la nota se condicionara con el formato con la cual se mostrara
                             * si en en decimal se colocara lo mismo que en las notas
                             * si es no decimal se colocara igual que en las notas
                             */
                            if ($value->nota == NULL) {
                                $poderCalificar = TRUE;
                                //se podra editar
                                ?>
                                <input type="hidden" value="<?= $value->juicio_id ?>" name="juicio_id[]" class=""  />
                                <input type="hidden" value="<?= $value->periodo ?>" name="periodo[]" class=""  />
                                <input type="hidden" value="<?= $value->estudiante_id ?>" name="estudiante_id[]" class=""  />
                                <input type="number" max="<?= $calificacion_maxima ?>" min="0" name="nota[]" step="0.01" class="form-control"  />                                
                                <?php
                            } else {
                                //no se podra editar
                                ?>
                                <input type="number" value="<?= $value->nota ?>" class="form-control" disabled="" />
                                <?php
                            }
                            ?>

                        </td>
                    </tr>
                <?php } ?>
                <tr>
                    <td colspan="3">
                        <?php
                        if ($poderCalificar) {
                            //se podra editar
                            ?>
                            <input type="submit" class="btn btn-default" />
                            <?php
                        } else {
                            //no se podra editar
                            ?>
                            <input type="submit" class="btn btn-default disabled" />
                            <?php
                        }
                        ?>

                    </td>
                </tr>
            </tbody>
        </table>

        <!-- jQuery Version 1.11.0 -->
        <script src="<?= base_url() ?>js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?= base_url() ?>js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?= base_url() ?>js/plugins/metisMenu/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<?= base_url() ?>js/sb-admin-2.js"></script>

    </body>

</html>
