<div class="col-lg-12 table-responsive">

    <div class="col-md-6">
        <table class="table table-responsive" >
            <thead>
                <tr>
                    <td>
                        Numero
                    </td>
                    <td>
                        Fecha
                    </td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($facturas as $value) { ?>
                    <tr>
                        <td>
                            <a href="<?= base_url() ?>facturas/detalle/<?= $value->numero ?>" class="btn btn-default">
                                <?= $value->numero ?>    
                            </a>
                        </td>
                        <td>
                            <?= $value->fecha ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

    </div>
    <br/>
    <div class="col-md-6">

        <div class="col-md-6">
            <form id="formulario_facturadora"rol="form">
                <input id="codigo_de_barra" name="id" autofocus="autofocus" type="number" class="form-control"/>
                <input type="submit" class="btn btn-default" value="Buscar"  id="buscar"/>
                <input type="submit" form="facturadoraPrincipal" class="btn btn-success" value="Facturar"/>
                <?= anchor("facturas/", "Cancelar", "class='btn btn-warning'") ?>
            </form>
        </div>
        <div class="col-md-6">
            <label>
                Estudiante
            </label>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="credito">
                    <div id="datos">
                        <div class="form-group">
                            <input form="facturadoraPrincipal" 
                                   required="" type="number" name="codigo" 
                                   class="form-control" placeholder="Codigo"/>    
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="banco" value="1"/>Banco
                            </label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div id="total">
                <label>
                    Total
                </label>
                <input type="number" name="totalDeTodo" class="form-control" disabled 
                       form="facturadoraPrincipal" value="0" id="totalDeTodo"/>
            </div>
        </div>
        <div id="resultado" class="col-lg-12">
            <?= form_open("facturas/generarFactura", array("id" => "facturadoraPrincipal")) ?>
            <table id="productos" class="table">
                <tr>
                    <th>
                        Codigo
                    </th>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Precio
                    </th>
                </tr>
            </table>
            </form>
        </div>

    </div>
</div>