<div class="col-lg-12 table-responsive">

    <div class="col-lg-offset-4 col-md-4">
        <table class="table table-responsive" >
            <thead>
                <tr>
                    <td>
                        Curso
                    </td>
                    <td>
                        Accion
                    </td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($facturas as $value) { ?>
                    <tr>
                        <td>
                            <?= $value->grado ?>-<?= $value->grupo ?>-<?= $value->jornada ?>
                        </td>
                        <td>
                            <?= anchor("calificaciones/seleccionarAsignatura/" . $value->grado_id . "-" . $value->grupo_id . "-" . $value->jornada_id, "Escojer", 'class="btn btn-success"') ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

    </div>
</div>
