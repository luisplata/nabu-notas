<div class="col-lg-12 table-responsive">
    <div class="col-md-6">
        <table class="table table-responsive" >
            <thead>
                <tr>
                    <td>
                        Id
                    </td>
                    <td>
                        Nombre
                    </td>
                    <td>
                        Precio
                    </td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($productos as $value) { ?>
                    <tr>
                        <td>
                            <?= $value->id ?>
                        </td>
                        <td>
                            <?= $value->nombre ?>
                        </td>
                        <td>
                            <?= $value->precio ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

    </div>
    <div class="col-md-6">
        <?= form_open("productos/guardar") ?>
        <div class="form-group">
            <label>
                Nombre
            </label>
            <input type="text" name="nombre" class="form-control" />    
        </div>
        <div class="form-group">
            <label>
                Descripcion
            </label>
            <input type="text" name="descripcion" class="form-control" />    
        </div>
        <div class="form-group">
            <label>
                Precio
            </label>
            <input type="number" name="precio" class="form-control" />    
        </div>
        <div class="form-group">
            <input type="submit" value="Facturar" class="btn btn-success" />    
            <input type="reset" value="Resetear" class="btn btn-warning" />    
        </div>
        </form>
    </div>
</div>
