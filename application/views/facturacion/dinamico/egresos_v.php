<div>
    <div class="tab-content col-lg-6">
        <table class="table table-hover table-responsive">
            <thead>
                <tr>
                    <td>
                        Descripcion    
                    </td>
                    <td>
                        Cantindad
                    </td>
                    <td>
                        Fecha
                    </td>
                    <td>
                        Usuario
                    </td>
                    <td>
                        Accion
                    </td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($egresos as $value) { ?>
                    <tr>
                        <td>
                            <?= $value->descripcion ?>
                        </td>
                        <td>
                            <?= $value->cantidad ?>
                        </td>
                        <td>
                            <?= $value->fecha ?>
                        </td>
                        <td>
                            <?= $value->administrador_nombre ?> <?= $value->administrador_apellido ?>
                        </td>
                        <td>

                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="col-lg-6">
        <!--Formulario para nuevo egreso-->
        <?= form_open("facturas/nuevoEgreso") ?>
        <div class="form-group">
            Descripcion:
            <input type="text" maxlength="200" class="form-control" name="descripcion" />
        </div>
        <div class="form-group">
            Cantidad:
            <input type="number" class="form-control" name="cantidad" />
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Guardar" />
            <input type="reset" class="btn btn-warning" value="Borrar" />
        </div>
    </div>
</div>