<?php
if ($this->session->userdata("usuario_id") == NULL) {
    redirect("login/modulos");
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Sistema de notas para colegios">
        <meta name="author" content="luis plata">

        <title><?= $this->config->item("NOMBREAPP") ?></title>

        <!-- Bootstrap Core CSS -->
        <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?= base_url() ?>css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?= base_url() ?>css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?= base_url() ?>font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body style="background-color: white;">
        <div class="container">
            <div class="col-lg-12 text-center">
                <h2>
                    <?= $this->session->userdata("institucion_nombre") ?>
                </h2>
            </div>

            <div class="col-md-4 col-xs-4">
                <table class="table table-bordered table-striped">
                    <?php
                    foreach ($instituciones as $value) {
                        $direccion = $value->direccion;
                        $telefono = $value->telefono;
                        ?>
                        <tr>
                            <td>
                                Nit
                            </td>
                            <td>
                                <?= $value->nit ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Resolucion
                            </td>
                            <td>
                                <?= $value->resolucion ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                DANE
                            </td>
                            <td>
                                <?= $value->DANE ?>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <div class="col-md-4 col-xs-4 text-center">
                <center>
                    <img src="<?= base_url() ?><?= $this->session->userdata("escudo") ?>"  width="300" />            
                </center>
            </div>
            <div class="col-md-4 col-xs-4">
                <table class="table table-bordered table-striped">
                    <?php foreach ($estudiantes as $value) { ?>
                        <tr>
                            <td>
                                Grado
                            </td>
                            <td>
                                <?= $value->grado_nombre ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Grupo
                            </td>
                            <td>
                                <?= $value->grupo_nombre ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Jornada
                            </td>
                            <td>
                                <?= $value->jornada_nombre ?>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>    

            <div class="row">
                <div class="col-lg-12 text-center clearfix" >
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>
                                    Nombre
                                </td>
                                <td>
                                    Apellido
                                </td>
                                <td>
                                    Codigo
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($estudiantes as $value) { ?>
                                <tr>
                                    <td>
                                        <?= $value->estudiante_nombre ?>
                                    </td>
                                    <td>
                                        <?= ($value->estudiante_apellido == "X") ? "" : $value->estudiante_apellido ?>
                                        <?php //$value->estudiante_apellido    ?>
                                    </td>
                                    <td>
                                        <?= $value->estudiante_codigo ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>
                                    Asignatura
                                </td>
                                <td>
                                    Notas
                                </td>
                                <td>
                                    Juicios
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            /* Prueba de quitar notas sin juicios */

                            //print_r($juicios);
                            if ($juicios == NULL) {
                                
                            } else {
                                
                            }

                            /* Prueba de quitar notas sin juicios */
                            ?>
                            <?php foreach ($asignaturas as $value1) { ?>

                                <?php
                                foreach ($juicios AS $juicio) {
                                    if ($juicio->asignatura_id == NULL) {
                                        
                                    }
                                }
                                ?>
                                <tr>
                                    <td>
                                        <?= $value1->asignatura_nombre ?>
                                    </td>

                                    <td>
                                        <?php
                                        $total = 0;
                                        $cantidad = 0;


                                        foreach ($notas as $value2) {
                                            if ($value1->asignatura_id == $value2->asignatura_id) {
                                                $cantidad++;
                                                $total += $value2->nota;
                                                ?>
                                                <span class=""><?php echo $value2->nota ?></span>
                                                <span class="">
                                                    <?php
//                                                    switch ($value2->nota) {
//                                                        case "1":
//                                                            echo 'Bj';
//
//                                                            break;
//                                                        case "3":
//                                                            echo 'Bs';
//                                                            break;
//                                                        case "4":
//                                                            echo 'Al';
//                                                            break;
//                                                        case "5":
//                                                            echo 'Su';
//                                                            break;
//                                                        default:
//                                                            echo 'NN';
//                                                            break;
//                                                    }
                                                    ?></span>
                                            <?php } ?>
                                        <?php } ?>

                                        <span class="pull-right">Total: <?php
                                            foreach ($this->configuracion_model->listarTodo() as $value) {
                                                $calificaionMaxima = $value->calificacion_maxima;
                                                $porcentajeBajo = $value->porcentaje_bajo;
                                                $porcentajeMedio = $value->porcentaje_medio;
                                                $porcentajeAlto = $value->porcentaje_alto;
                                                $porcentajeSuperior = $value->porcentaje_superior;
                                                $calificacionBaja = $value->calificacion_baja;
                                                $calificacionBasico = $value->calificacion_basico;
                                                $calificacionAlta = $value->calificacion_alta;
                                                $calificacionSuperior = $value->calificacion_superior;
                                            }
                                            if (($total / $cantidad) >= 0 && ($total / $cantidad) < ($porcentajeBajo * $calificaionMaxima)) {
                                                //Bajo
                                                echo 'Bj ' . ($total / $cantidad);
                                            } elseif (($total / $cantidad) >= ($porcentajeBajo * $calificaionMaxima) && ($total / $cantidad) < ($porcentajeMedio * $calificaionMaxima)) {
                                                //Medio
                                                echo 'Bs ' . ($total / $cantidad);
                                            } elseif (($total / $cantidad) >= ($porcentajeMedio * $calificaionMaxima) && ($total / $cantidad) < ($porcentajeAlto * $calificaionMaxima)) {
                                                //Alto
                                                echo 'Al ' . ($total / $cantidad);
                                            } elseif (($total / $cantidad) >= ($porcentajeMedio * $calificaionMaxima) && ($total / $cantidad) <= ($porcentajeSuperior * $calificaionMaxima)) {
                                                //superiior
                                                echo 'Su ' . ($total / $cantidad);
                                            }
                                            ?></span>
                                    </td>
                                    <td>
                                        <?php
                                        /* Sacamos el promedio de las notas de los juicios
                                         */
                                        $notaActual = 0;
                                        $cantidadDeJuicios = 0;
                                        $totalDeNota;
                                        foreach ($juicios as $value3) {
                                            if ($value1->asignatura_id == $value3->asignatura_id) {
                                                $notaActual+=$value3->nota;
                                                $cantidadDeJuicios++;
                                                ?>
                                                <span class=""><?= $value3->descripcion ?> <?= $value3->nota ?></span>
                                                <hr />
                                                <?php
                                            }
                                        }
                                        $totalDeNota = $notaActual / $cantidadDeJuicios;
                                        ?>
                                        <span>Promedio: <?= $totalDeNota ?></span>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>    
            </div>

            <div class= "col-lg-12 text-uppercase">
                <div class="col-xs-6 col-md-6 text-center">
                    <br/>
                    __________________________________
                    <br/>
                    Directora
                    <br/>

                    <br/>
                </div>
                <div class="col-xs-6 cool-md-6 text-center">
                    <br/>
                    __________________________________
                    <br/>
                    Directora de grupo
                    <br/>
                    <br/>
                </div>

            </div>
            <div class="col-lg-12 text-center">
                <?= $direccion ?> <br/>
                <?= $telefono ?><br/>
                <h4 class="pull-right">
                    <small class="text-uppercase">
                        Upgradec.com
                    </small>
                </h4>
            </div>
        </div>

        <!-- jQuery Version 1.11.0 -->
        <script src="<?= base_url() ?>js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?= base_url() ?>js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?= base_url() ?>js/plugins/metisMenu/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<?= base_url() ?>js/sb-admin-2.js"></script>

    </body>

</html>
