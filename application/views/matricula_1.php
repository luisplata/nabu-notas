<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= $this->config->item("NOMBREAPP") ?></title>
        <!-- CSS de Bootstrap -->
        <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet" media="screen">

        <style>
            
            *{
                font-family: 'Arial Black', Gadget, sans-serif;
            }
        </style>
        <!-- librerías opcionales que activan el soporte de HTML5 para IE8 -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div>
            <div class="col-xs-3">

                <img class="img-responsive" width="100%" height="100%" src="<?= base_url() ?><?= $this->session->userdata("escudo") ?>">
                <h3>
                    Factura No <?= $factura_numero + 1 ?> <br/> 
                    <small><?= date("Y-m-d H:i:s") ?></small>
                </h3>
                <h5>
                    <small>
                        Colegio: <?= $this->session->userdata("institucion_nombre") ?><br/>
                        Direccion: <?= $this->session->userdata("institucion_direccion") ?><br/>
                        Telefono: <?= $this->session->userdata("institucion_telefono") ?><br/>
                        Acudiente: 
                    </small>
                </h5>

                <div>
                    <table class="table">
                        <tr><?php $totalDeTodo = 0; //el que tiene el total de todo                                                   ?>
                            <th>
                                ID
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Precio
                            </th>
                            <th>
                                Total
                            </th>
                        </tr>
                        <?php
                        $fecha_matricula = explode("-", $fechaMatricula);
                        if (($fecha_matricula[2] - date("d")) < 0) {
                            //significa que esta fuera del rango de las matriculas
                            /* falta obtener el valor exacto dinamico para este precio
                             * ya que con datos estaticos no aguanta
                             */
                            $producto['precio'] = 520000;
                        }

                        $totalDeTodo += $producto['precio'];
                        ?> 
                        <tr>
                            <td>
                                <?= $producto["id"] ?>
                            </td>
                            <td>
                                <?= $producto["nombre"] ?>
                            </td>
                            <td>
                                <?= $producto["precio"] ?>

                            </td>
                            <td>
                                <?= $producto["precio"] ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                Recivido:
                            </td>
                            <td colspan="2" id="recivido">

                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                Total: 
                            </td>
                            <td id="total" colspan="2">
                                <?php echo $totalDeTodo ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                Vuelto:
                            </td>
                            <td colspan="2" id="vuelto">

                            </td>
                        </tr>
                    </table>
                    <div class="text-left">
                        <h5>
                            Datos del cliente<br/>
                            <small>
                                Nombre: <?= $estudiante['nombre'] ?> <?= $estudiante['apellido'] ?><br/>
                                Documento: <?= $estudiante['documento'] ?>
                            </small>
                        </h5>
                    </div>
                    <p class="">
                    <h5 class="text-right text-uppercase">
                        <small>
                            upgradec.com
                        </small>
                    </h5>
                    </p>
                </div>

            </div>
        </div>
        <!-- Librería jQuery requerida por los plugins de JavaScript -->
        <script src="http://code.jquery.com/jquery.js"></script>
        <!-- Todos los plugins JavaScript de Bootstrap (también puedes
                     incluir archivos JavaScript individuales de los únicos
                     plugins que utilices) -->
        <script src="<?= base_url() ?>js/bootstrap.min.js"></script>


        <script>
            //para imprimir
            $(document).ready(function () {
                //alert(window.location.hostname);
                var recivido = prompt("Recivio: ");
                $("#recivido").text(recivido);
                var total = $("#total").text();

                var vuelto = recivido - total;
                $("#vuelto").text(vuelto);
                alert("Vuelto: " + vuelto);
                if (vuelto < 0) {
                    alert("Estas reciviendo menos de lo que es");
                }
                window.print();
                //servidor
                $(location).attr('href', "http://" + window.location.hostname + "/estudiantes/");
                //local
                //                $(location).attr('href', "http://" + window.location.hostname + ":8080/Nabu-Facturadora"
                //                        + "/facturacion/nuevaFactura");
            });
        </script>

    </body>
</html>


