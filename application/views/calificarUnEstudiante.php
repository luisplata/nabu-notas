<?php
if ($this->session->userdata("usuario_id") != NULL) {
    redirect("login/modulos");
}
$calificado = FALSE;
$total = 0;
$cantidad = 0;
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Sistema de notas para colegios">
        <meta name="author" content="luis plata">

        <title><?= $this->config->item("NOMBREAPP") ?></title>

        <!-- Bootstrap Core CSS -->
        <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?= base_url() ?>css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?= base_url() ?>css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?= base_url() ?>font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <div class="container table-responsive">
            <?= form_open("calificaciones/calificarEstudiante") ?>
            <!-- 
            Colocamos los datos del estudiante y los del ingreso a la nota para
            que pueda ser calificado
            -->
            <input name="estudiante_id" type="hidden" value="<?= $estudiante_id ?>" readonly="" />
            <input name="asignatura_id" type="hidden" value="<?= $asignatura_id ?>" readonly="" />
            <table class="table">
                <thead>
                    <tr>
                        <?php
                        for ($i = 1; $i <= $this->session->userdata("numero_notas"); $i++) {
                            $cantidad++;
                            ?>
                            <td> Nota #<?= $i ?></td>
                        <?php } ?>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php
                        //aqui vamos a validar si se mandaron los datos de las notas
                        if (isset($notas)) {
                            //cuando la nota hay
                            //aqui vamos a colocar las notas de estudiante y las que
                            //sean calificadas se colocaran como desabilitadas
                            for ($o = 0; $o < count($notas); $o++) {
                                if ($notas[$o]['nota'] == 0) {
                                    ?>
                                    <!-- Se podra editar -->
                                    <?php if ($this->session->userdata("calificacion_decimal") == 0) { ?>
                                        <td>
                                            <select class="form-control" name="nota<?= $o ?>">
                                                <option value="0:<?= $notas[$o]["id"] ?>">
                                                </option>
                                                <option value="1:<?= $notas[$o]["id"] ?>">
                                                    <?= $this->session->userdata("calificacion_baja") ?>
                                                </option>
                                                <option value="3:<?= $notas[$o]["id"] ?>">
                                                    <?= $this->session->userdata("calificacion_basico") ?>
                                                </option>
                                                <option value="4:<?= $notas[$o]["id"] ?>">
                                                    <?= $this->session->userdata("calificacion_alta") ?>
                                                </option>
                                                <option value="5:<?= $notas[$o]["id"] ?>">
                                                    <?= $this->session->userdata("calificacion_superior") ?>
                                                </option>
                                            </select>
                                        </td>
                                    <?php } else {
                                        ?>
                                        <td>
                                            <input type="number" name="nota<?= $o ?>" value="0" disabled class="form-control" step="0.01" min="0" max="5"  />
                                        </td>
                                        <?php
                                    }
                                } else {
                                    $calificado = TRUE;
                                    $total += $notas[$o]["nota"];
                                    ?>
                                    <!-- No se podra Editar -->
                                    <?php if ($this->session->userdata("calificacion_decimal") == 0) { ?>
                                        <td>
                                            <select class="form-control"  disabled="">
                                                <option>
                                                    <?php
                                                    //hacemos preguntas de rangos para esta parte
                                                    /* Necesitamos estos datos:
                                                     * Nota maxima
                                                     * porcentaje: bj, bs, alt, sup
                                                     */
                                                    foreach ($this->configuracion_model->listarTodo() as $value) {
                                                        $calificaionMaxima = $value->calificacion_maxima;
                                                        $porcentajeBajo = $value->porcentaje_bajo;
                                                        $porcentajeMedio = $value->porcentaje_medio;
                                                        $porcentajeAlto = $value->porcentaje_alto;
                                                        $porcentajeSuperior = $value->porcentaje_superior;
                                                        $calificacionBaja = $value->calificacion_baja;
                                                        $calificacionBasico = $value->calificacion_basico;
                                                        $calificacionAlta = $value->calificacion_alta;
                                                        $calificacionSuperior = $value->calificacion_superior;
                                                    }
                                                    //marcando limites

                                                    if ($notas[$o]["nota"] >= 0 && $notas[$o]["nota"] < ($porcentajeBajo * $calificaionMaxima)) {
                                                        //Bajo
                                                        echo 'Bj ' . $notas[$o]["nota"];
                                                    } elseif ($notas[$o]["nota"] >= ($porcentajeBajo * $calificaionMaxima) && $notas[$o]["nota"] < ($porcentajeMedio * $calificaionMaxima)) {
                                                        //Medio
                                                        echo 'Bs ' . $notas[$o]["nota"];
                                                    } elseif ($notas[$o]["nota"] >= ($porcentajeMedio * $calificaionMaxima) && $notas[$o]["nota"] < ($porcentajeAlto * $calificaionMaxima)) {
                                                        //Alto
                                                        echo 'Al ' . $notas[$o]["nota"];
                                                    } elseif ($notas[$o]["nota"] >= ($porcentajeMedio * $calificaionMaxima) && $notas[$o]["nota"] <= ($porcentajeSuperior * $calificaionMaxima)) {
                                                        //superiior
                                                        echo 'Su ' . $notas[$o]["nota"];
                                                    }
                                                    ?>
                                                </option>
                                            </select>
                                        </td>
                                    <?php } else {
                                        ?>
                                        <!--Aqui se colocaran las notas decimales no editables-->
                                        <td>
                                            <input type="text" disabled value='<?= $notas[$o]["nota"]; ?>' class="form-control" />
                                        </td>
                                        <?php
                                    }
                                }
                            }
                        } else {
                            // cuando no hay nota, y hay que generar nuevos datos
                            ?>
                            <?php for ($i = 1; $i <= $this->session->userdata("numero_notas"); $i++) { ?>
                                <td> 
                                    <?php if ($this->session->userdata("calificacion_decimal") == 0) { ?>
                                        <!-- Cuando es Por letras y no se ah calificado al estudiante -->
                                        <select class="form-control" name="nota<?= $i ?>">
                                            <option value="0">
                                            </option>
                                            <option value="1">
                                                <?= $this->session->userdata("calificacion_baja") ?>
                                            </option>
                                            <option value="3">
                                                <?= $this->session->userdata("calificacion_basico") ?>
                                            </option>
                                            <option value="4">
                                                <?= $this->session->userdata("calificacion_alta") ?>
                                            </option>
                                            <option value="5">
                                                <?= $this->session->userdata("calificacion_superior") ?>
                                            </option>
                                        </select>
                                    <?php } else { ?>
                                        <!-- Cuando es por nota Nuemros -->
                                        <!--Colocando los campos numericos-->
                                        <input type="number" name="nota<?= $i ?>" class="form-control" step="0.01" min="0" max="5"  />
                                    <?php } ?>

                                </td>
                            <?php } ?>

                        <?php } ?>


                    </tr>
                    <tr>
                        <td colspan="<?= $this->session->userdata("numero_notas") ?>">
                            <span class="pull-left">Nota FInal: <?= $total / $cantidad ?></span>
                            <input type="submit" value="Calificar" <?= ($calificado) ? "disabled=''" : "" ?> class="btn btn-success pull-right" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>

    <!-- jQuery Version 1.11.0 -->
    <script src="<?= base_url() ?>js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url() ?>js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?= base_url() ?>js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?= base_url() ?>js/sb-admin-2.js"></script>

</body>

</html>
