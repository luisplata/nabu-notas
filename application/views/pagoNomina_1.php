<?php
if ($this->session->userdata("usuario_id") == NULL) {
    redirect("login/modulos");
}
setlocale(LC_MONETARY, 'en_US');
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Sistema de notas para colegios">
        <meta name="author" content="luis plata">

        <title><?= $this->config->item("NOMBREAPP") ?></title>

        <!-- Bootstrap Core CSS -->
        <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?= base_url() ?>css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?= base_url() ?>css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?= base_url() ?>font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body style="background-color: white;">
        <div class="container">
            <div class="table-responsive col-xs-12">
                <?= form_open("contabilidad/confirmarNomina") ?>
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <td colspan="4" class=" text-center">
                                <img src="<?= base_url() ?>/img/default.png" width="200" height="200" class=""> 
                                <br/>
                                <h2>Institucion Nabu.com</h2>
                            </td>

                        </tr>
                        <tr class="">
                            <td class="text-center">
                                Fecha: 2014-11-25
                            </td>
                            <td colspan="2" class="text-center">
                                Administrador: Luis Plata
                            </td>
                            <td class="text-center">
                                Nombre De Formato: Pago De Nomina
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        //print_r($docentes);
                        $total = 0;
                        foreach ($docentes as $value2) {
                            foreach ($datos as $key => $value) {
                                if ($value["docente_id"] == $value2->id) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?= $value2->nombre ?> <?= $value2->apellido ?>
                                            <input type="hidden" name="nombre[]" value="<?= $value2->nombre ?>" />
                                            <input type="hidden" name="apellido[]" value="<?= $value2->apellido ?>" />
                                            <input type="hidden" name="sueldo[]" value="<?= $value['totalPagado'] ?>" />
                                            <?php foreach ($configuracion as $value5) { ?>
                                                <input type="hidden" name="categoria_egreso_id" value="<?= $value5->categoria_egreso ?>" />
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?= $value2->documento ?>
                                        </td>
                                        <td>
                                            $<?= number_format($value['sueldoBase']) ?> - $<?= number_format($value['salud']) ?> - $<?= number_format($value['pension']) ?>
                                        </td>
                                        <td class="text-right">
                                            <?= number_format($value['totalPagado']) ?>
                                            <?php $total+=$value['totalPagado'] ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                        ?>                        
                        <tr>
                            <td colspan="4" class="text-right">
                                Total: $<?= number_format($total); ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <input  type="submit" value="Confirmar" class="pull-right btn btn-success hidden-print" /> 
                <a href="<?= base_url() ?>contabilidad/pagoNomina" class="pull-right btn btn-warning hidden-print">Cancelar</a>
                </form>
            </div>
        </div>
        <!-- jQuery Version 1.11.0 -->
        <script src="<?= base_url() ?>js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?= base_url() ?>js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?= base_url() ?>js/plugins/metisMenu/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<?= base_url() ?>js/sb-admin-2.js"></script>

    </body>

</html>
