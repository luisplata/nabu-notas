<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Docentes extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("docente_model", "docente");
    }

    public function Index() {
        $datos = array(
            "pagina" => "docente_v",
            "docentes" => $this->docente->ListarTodo(),
        );
        $this->load->view("notas", $datos);
    }

    public function Guardar() {
        //creando el array del docente
        $docente = array(
            "nombre" => $this->input->post("nombre"),
            "apellido" => $this->input->post("apellido"),
            "documento" => $this->input->post("documento"),
            "sexo" => $this->input->post("sexo"),
            "email" => $this->input->post("email"),
            "telefono" => $this->input->post("telefono"),
            "sede_id" => $this->session->userdata("sede_id")
        );
        //lo insertamos
        if ($this->docente->Guardar($docente)) {
            redirect("docentes/?mensaje=Se guardo el docente de forma exitosa&tipo=success");
        } else {
            redirect("docentes/?mensaje=No se guardo el docente de forma exitosa&tipo=warning");
        }
    }

    public function Eliminar($id) {
        if ($this->docente->Eliminar($id)) {
            redirect("docentes/?mensaje=Se eliminó con exito&tipo=success");
        } else {
            redirect("docentes/?mensaje=No se eliminó con exito&tipo=warning");
        }
    }

}
