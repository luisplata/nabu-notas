<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Cursos extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("curso_model", "curso");
//        $this->output->enable_profiler(TRUE);
    }

    public function index() {
        //listamos los cursos disponibles
        $datos = array(
            "pagina" => "cursos_v",
            "cursos" => $this->curso->listarTodo(),
            "grados" => $this->curso->listarGrados(),
            "grupos" => $this->curso->listarGrupos(),
            "jornadas" => $this->curso->listarJornadas()
        );
        $this->load->view("notas", $datos);
    }

    public function modificarCurso($pegado) {
        $curso = explode("-", $pegado);

        $datos = array(
            "pagina" => "modificarCurso",
            "curso" => $this->curso->listarCurso($curso),
            "cursos" => $this->curso->listarTodo(),
            "promociones" => $this->curso->cursosPromocion($curso[0])
        );
        $this->load->view("notas", $datos);
    }

    public function actualizarCurso() {
        $set = array(
            "promocion" => $this->input->post("promocion")
        );
        $where = array(
            "grado_id" => $this->input->post("grado_id"),
            "grupo_id" => $this->input->post("grupo_id"),
            "jornada_id" => $this->input->post("jornada_id")
        );
        if ($this->curso->actualizarCurso($where, $set)) {
            
        } else {
            
        }
        redirect("cursos/");
    }

    public function guardarCurso() {
        //guardar el curso nuevo
        $curso = array(
            "grado_id" => $this->input->post("grado_id"),
            "grupo_id" => $this->input->post("grupo_id"),
            "jornada_id" => $this->input->post("jornada_id"),
            "sede_id" => $this->session->userdata("sede_id"),
        );
        if ($this->curso->guardarCurso($curso)) {
            
        } else {
            
        }
        redirect("cursos/");
    }

    public function grados() {
        //listar los grados   
        $datos = array(
            "pagina" => "grados_v",
            "grados" => $this->curso->listarGrados()
        );
        $this->load->view("notas", $datos);
    }

//
//    public function guardarGrado() {
//        //guardar nuevo grado
//        //armamos el array del grado
//        $grado = array(
//            "nombre" => $this->input->post("nombre"),
//            "codigo" => $this->input->post("codigo"),
//            "promocion" => $this->input->post("promocion"),
//            "sede_id" => $this->session->userdata("sede_id")
//        );
//        if ($this->curso->guardarGrado($grado)) {
//            
//        } else {
//            
//        }
//        redirect("cursos/grados");
//    }
//    public function modificarGrado($id) {
//        //listar los grados   
//        $datos = array(
//            "pagina" => "modificarGrado",
//            "grado" => $this->curso->listarGrado($id),
//            "grados" => $this->curso->listarGrados()
//        );
//        $this->load->view("notas", $datos);
//    }

    public function grupos() {
        //listar grupos
        $datos = array(
            "pagina" => "grupos_v",
            "grupos" => $this->curso->listarGrupos()
        );
        $this->load->view("notas", $datos);
    }

//    public function guardarGrupo() {
//        //guardar nuevo grupo
//        $grupo = array(
//            "nombre" => $this->input->post("nombre"),
//            "codigo" => $this->input->post("codigo"),
//            "sede_id" => $this->session->userdata("sede_id")
//        );
//        if ($this->curso->guardarGrupo($grupo)) {
//            
//        } else {
//            
//        }
//        redirect("cursos/grupos");
//    }
//    public function jornadas() {
//        //listar grupos
//        $datos = array(
//            "pagina" => "jornadas_v",
//            "jornadas" => $this->curso->listarJornadas()
//        );
//        $this->load->view("notas", $datos);
//    }
//    public function guardarJornada() {
//        //guardar nuevo grupo
//        $jornada = array(
//            "nombre" => $this->input->post("nombre"),
//            "horario" => $this->input->post("horario"),
//            "sede_id" => $this->session->userdata("sede_id")
//        );
//        if ($this->curso->guardarJornada($jornada)) {
//            
//        } else {
//            
//        }
//        redirect("cursos/jornadas");
//    }
// codigo de ospino


    public function guardarGrado() {
        //guardar nuevo grado
        //armamos el array del grado
        $grado = array(
            "nombre" => $this->input->post("nombre"),
            "codigo" => $this->input->post("codigo"),
            "sede_id" => $this->session->userdata("sede_id")
        );
        if ($this->curso->guardarGrado($grado)) {
            
        } else {
            
        }
        redirect("cursos/grados");
    }

    public function eliminarGrupo($id) {

        $datos = array(
            "id" => $id,
        );
        $set = array(
            "eliminado" => 1
        );
        $this->curso->eliminarGrupo($datos, $set);
    }

    public function eliminarGrado($id) {

        $datos = array(
            "id" => $id,
        );
        $set = array(
            "eliminado" => 1
        );
        $this->curso->eliminarGrado($datos, $set);
    }

    /*
     * este metodo modifica los datos de un grado
     * Vista -> modificarGrados
     */

    public function actualizarGrado() {
        $grado_modificado = array(
            "nombre" => $this->input->post("nombre"),
            "codigo" => $this->input->post("codigo"),
            "sede_id" => $this->session->userdata("sede_id")
        );
        $where = array(
            "id" => $this->input->post("id")
        );
        $this->curso->actualizarGrado($where, $grado_modificado);
    }

    public function actualizarJornada() {
        $jornada_modificada = array(
            "nombre" => $this->input->post("nombre"),
            "horario" => $this->input->post("horario"),
            "sede_id" => $this->session->userdata("sede_id")
        );
        $where = array(
            "id" => $this->input->post("id")
        );
        $this->curso->actualizarJornada($where, $jornada_modificada);
    }

    public function actualizarGrupo() {
        $grupo_modificado = array(
            "nombre" => $this->input->post("nombre"),
            "codigo" => $this->input->post("codigo"),
            "sede_id" => $this->session->userdata("sede_id")
        );
        $where = array(
            "id" => $this->input->post("id")
        );
        $this->curso->actualizarGrupo($where, $grupo_modificado);
    }

    public function modificaGrupo($id) {

        $datos = array(
            "pagina" => "modificarGrupo",
            "grado" => $this->curso->listarGrupos($id),
        );
        $this->load->view("notas", $datos);
    }

    public function modificarGrado($id) {
        //modificar grado
        $datos = array(
            "pagina" => "modificarGrado",
            "grado" => $this->curso->listarGrado($id),
            "grados" => $this->curso->listarGrados()
        );
        $this->load->view("notas", $datos);
    }

    public function guardarGrupo() {
        //guardar nuevo grupo
        $grupo = array(
            "nombre" => $this->input->post("nombre"),
            "codigo" => $this->input->post("codigo"),
            "sede_id" => $this->session->userdata("sede_id")
        );
        if ($this->curso->guardarGrupo($grupo)) {
            
        } else {
            
        }
        redirect("cursos/grupos");
    }

    public function jornadas() {
        //listar grupos
        $datos = array(
            "pagina" => "jornadas_v",
            "jornadas" => $this->curso->listarJornadas()
        );
        $this->load->view("notas", $datos);
    }

    public function guardarJornada() {
        //guardar nuevo grupo
        $jornada = array(
            "nombre" => $this->input->post("nombre"),
            "horario" => $this->input->post("horario"),
            "sede_id" => $this->session->userdata("sede_id")
        );
        if ($this->curso->guardarJornada($jornada)) {
            
        } else {
            
        }
        redirect("cursos/jornadas");
    }

    public function eliminarJornada($id) {
        $datos = array(
            "id" => $id,
        );
        $set = array(
            "eliminado" => 1
        );
        $this->curso->eliminarJornada($datos, $set);
    }

    public function modificaJornada($id) {
        $datos = array(
            "pagina" => "modificarJornada",
            "jornada" => $this->curso->editarJornada($id),
        );
        $this->load->view("notas", $datos);
    }

}
