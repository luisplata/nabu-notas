<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class JuiciosAdmin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("docente_model", "docente");
        $this->load->model("juicio_model", "juicio");
    }

    public function juiciosDocentes() {
        $datos = array(
            "pagina" => "juiciosDocentes",
            "docentes" => $this->docente->listarTodo()
        );
        $this->load->view("notas", $datos);
    }

    public function juicios($docente_id) {
        $datos = array(
            "pagina" => "juiciosDocentesDetalle",
            "juicios" => $this->juicio->verJuiciosInstitucion($docente_id)
        );
        $this->load->view("notas", $datos);
    }

}
