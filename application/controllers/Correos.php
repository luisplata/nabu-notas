<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Correos extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("correo_model", "correo");
        $this->output->enable_profiler(TRUE);
    }

    public function index() {
        $datos = array(
            "pagina" => "correo",
            "contenido" => "Aqui esta toda la parte de contabilidad de la institucion"
        );
        $this->load->view("notas", $datos);
    }

    public function mandarCorreoDocente() {
        //print_r($this->input->post("mensaje"));
        if ($this->correo->mandarCorreoDocente($this->input->post("titulo"), $this->input->post("mensaje"), $this->input->post("respondera"))) {
            //echo 'mando los correos';
            redirect("notas");
        } else {
            //echo 'no mando los correos';
            redirect("correos/index/no_se_mando_el_correo");
        }
    }

}
