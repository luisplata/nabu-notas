<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Sedes extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("sede_model", "sede");
    }

    public function Index() {
        $datos = array(
            "pagina" => "sedes_v",
            "sedes" => $this->sede->ListarTodo()
        );
        $this->load->view("notas", $datos);
    }

    public function Guardar() {
        $sede = array(
            "nombre" => $this->input->post("nombre"),
            "direccion" => $this->input->post("direccion"),
            "email" => $this->input->post("email"),
            "telefono" => $this->input->post("telefono"),
            "institucion_id" => $this->session->userdata("institucion_id")
        );
        if ($this->sede->Guardar($sede)) {
            redirect("sedes/?mensaje=Se Guardo la sede con exito&tipo=success");
        } else {
            redirect("sedes/?mensaje=No se Guardo la sede con exito&tipo=warning");
        }
    }

    public function Activar($estado, $id) {
        //el estado tiene 1 = activar; 0 = desactivar
        if ($this->sede->Activar($estado, $id)) {
            redirect("sedes/?mensaje=se cambio de estado con exito");
        } else {
            redirect("sedes/?mensaje=no se cambio de estado con exito");
        }
    }

    public function Eliminar($id) {
        if ($this->sede->Eliminar($id)) {
            redirect("sedes/?mensaje=se elimino con exito");
        } else {
            redirect("sedes/?mensaje=no se elimino con exito");
        }
    }

}
