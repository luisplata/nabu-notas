<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Instituciones extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("institucion_model", "institucion");
    }

    public function index() {
        $this->configuracion_model->acceso(array(4));
        $datos = array(
            "pagina" => "instituciones_v",
            "instituciones" => $this->institucion->listarTodo()
        );
        $this->load->view("notas", $datos);
    }

    public function guardar() {
        $institucion = array(
            "nombre" => $this->input->post("nombre"),
            "email" => $this->input->post("email"),
            "dane" => $this->input->post("dane"),
            "resolucion" => $this->input->post("resolucion"),
            "direccion" => $this->input->post("direccion"),
            "telefono" => $this->input->post("telefono"),
            "nit" => $this->input->post("nit"),
            "codigo_icfes" => $this->input->post("codigo_icfes")
        );
        if ($this->institucion->guardar($institucion)) {
            redirect("instituciones");
        } else {
            redirect("instituciones");
            $this->session->set_userdata("mensaje", "No se Guardo la Institucion");
            $this->session->set_userdata("tipo", "warning");
        }
    }

}
