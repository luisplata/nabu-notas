<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Productos extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("producto_model", "producto");
    }

    public function index() {
        //aqui se listaran y se creara los productos
        $datos = array(
            "pagina" => "productos",
            "productos" => $this->producto->listarTodo()
        );
        $this->load->view("facturacion", $datos);
    }

    public function guardar() {
        $producto = array(
            "nombre" => $this->input->post("nombre"),
            "descripcion" => $this->input->post("descripcion"),
            "precio" => $this->input->post("precio"),
            "institucion_id" => $this->session->userdata("institucion_id")
        );
        if ($this->producto->guardar($producto)) {
            
        } else {
            
        }
        redirect("productos");
    }

}
