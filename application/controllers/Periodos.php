<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Periodos extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("configuracion_model");
    }

    public function index() {
        $this->load->model("configuracion_model", "configuracion");
        $datos = array(
            "pagina" => "periodo_v",
            "peridos" => $this->configuracion->listarPeriodos()
        );
        $this->load->view("notas", $datos);
    }

    public function guardar() {
        $this->load->model("periodo_model", "periodo");
        $periodos = array(
            "periodo" => $this->input->post("periodo"),
            "fecha_inicio" => $this->input->post("fecha_inicio"),
            "fecha_final" => $this->input->post("fecha_final"),
            "institucion_id" => $this->session->userdata("institucion_id")
        );
        if ($this->periodo->guardar($periodos)) {
            
        } else {
            
        }
        redirect("periodos");
    }

    public function periodoActual($id) {
        if ($this->configuracion_model->periodoActual($id)) {
            
        } else {
            
        }
        redirect("periodos");
    }

}
