<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Inicio extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
    }

    public function index() {
        $this->load->view("login");
    }

    public function modulos() {
        $this->load->view("modulos");
    }

    public function docente() {

        $this->load->view("loginDocente");
    }

    public function moduloDocente() {
        $this->load->view("modulosDocente");
    }

}
