<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Estudiantes extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("estudiante_model", "estudiante");
        $this->load->model("curso_model", "curso");
        //$this->output->enable_profiler(TRUE);
    }

    public function index() {
        $datos = array(
            "pagina" => "estudiantes_v",
            "estudiantes" => $this->estudiante->listarTodo(),
            "cursos" => $this->curso->listarTodo()
        );
        $this->load->view("notas", $datos);
    }

    public function guardar() {
//armamos el array del estudiante
        /* Sacamos el grado-grupo y jornada del input del formulario 
         * Donde el orden es el siguiente:
         * grado-grupo-jornada-grado_codigo
         */
        /* cargando el producto a cobrar en la matricula         */
        $this->load->model("configuracion_model", "configuracion");
        $this->load->model("producto_model", "producto");
        foreach ($this->configuracion->listarTodo() as $datos) {
            $consecutivo = $datos->consecutivo;
            $dias_de_matricula = $datos->dias_de_matricula;
            $fecha_de_matricula = $datos->fecha_de_matricula;
            $factura_numero = $datos->factura_numero;
            foreach ($this->producto->listarTodo() as $datos2) {
                if ($datos->producto_matricula == $datos2->id) {
                    $producto = array(
                        "id" => $datos2->id,
                        "nombre" => $datos2->nombre,
                        "precio" => $datos2->precio
                    );
                }
            }
        }
        $cursos = explode("-", $this->input->post("curso"));
//y el de la matricula
        $estudiante = array(
            "nombre" => $this->input->post("nombre"),
            "apellido" => $this->input->post("apellido"),
            "codigo" => date("Y") . $cursos[3] .
            $consecutivo,
            "fecha_nacimiento" => $this->input->post("fecha_nacimiento"),
            "documento" => $this->input->post("documento"),
            "genero" => $this->input->post("genero"),
            "sede_id" => $this->session->userdata("sede_id")
        );

        if ($this->estudiante->guardarEstudiante($estudiante)) {
            $estudiante_id = $this->db->insert_id();

            $this->configuracion->guardar(array("consecutivo" => $consecutivo + 1), array("institucion_id" => $this->session->userdata("institucion_id")));
//$this->session->set_userdata("consecutivo", $this->session->userdata("consecutivo"));
            $matricula = array(
                "estudiante_id" => $estudiante_id,
                "sede_id" => $this->session->userdata("sede_id"),
                "curso_grado_id" => $cursos[0],
                "curso_grupo_id" => $cursos[1],
                "curso_jornada_id" => $cursos[2],
                "fecha" => date("Y-m-d")
            );
            if ($this->estudiante->guardarMatricula($matricula)) {
                $this->load->model("factura_model", "factura");
                $this->factura->guardar(array($producto), $estudiante_id, 0);
                $contenido = array(
                    "estudiante" => $estudiante,
                    "producto" => $producto,
                    "diasMatricula" => $dias_de_matricula,
                    "fechaMatricula" => $fecha_de_matricula,
                    "factura_numero" => $factura_numero
                );
                if ($this->configuracion->consultarDato("imprimir") == 1) {
                    $this->load->view("matricula_1", $contenido);
                }
            } else {
                echo "no salio";
            }
        } else {
            echo 'no salio';
        }
    }

    public function boletin() {
        /* El generador de boletines este es el que los lista 
         * Lo que se va a hacer es crear un buscador, sin ajax.
         * lo cual va a buscar por los siguientes requisitos.
         * curso, codigo.
         */
        $datos = array(
            "pagina" => "busqueda",
            "cursos" => $this->curso->listarTodo()
        );
        $this->load->view("notas", $datos);
    }

    public function promocion() {
        /* El generador de boletines este es el que los lista 
         * Lo que se va a hacer es crear un buscador, sin ajax.
         * lo cual va a buscar por los siguientes requisitos.
         * curso, codigo.
         */
        $datos = array(
            "pagina" => "promocion",
            "cursos" => $this->curso->listarTodo()
        );
        $this->load->view("notas", $datos);
    }

    public function buscarBoletin() {
        $datos = array(
            "pagina" => "listarEstudiantesBoletin",
            "estudiantes" =>
            $this->estudiante->listarTodoBoletin($this->input->post("curso"), $this->input->post("codigo"))
        );
        $this->load->view("notas", $datos);
    }

    public function buscarPromocion() {
        $datos = array(
            "pagina" => "listarEstudiantesPromocion",
            "estudiantes" =>
            $this->estudiante->listarTodoBoletin($this->input->post("curso"), $this->input->post("codigo"))
        );
        $this->load->view("notas", $datos);
    }

    public function generarBoletin($estudiante_id) {
        /* El generador de boletines este es el que los lista 
         * Se necesitan los siguientes datos:
         * Institucion:
         *  Nombre 
         *  resolucion
         *  nit
         *  dane
         *  jornada
         * Estudiante
         *  curso
         *  año electivo
         *  nombre y apellido
         *  codigo
         * Notas
         *  asignaturas
         *  nota N
         *  total
         *  desempeño
         *  Juicios
         */
        $this->load->model("institucion_model", "institucion");
        $this->load->model("nota_model", "nota");
        $this->load->model("juicio_model", "juicio");
        $instituciones = $this->institucion->listarUnaInstitucion($this->session->userdata("institucion_id"));
        $estudiante = $this->estudiante->listarUnEstudiante($estudiante_id);
        $juicios = $this->juicio->listarPorEstudiante($estudiante_id);
        
        /* vamos a sacar de la base de datos las notas del estudiante la cual van a ser las notas
         * con la id del estudiante y el periodo actual
         */

        $notas = $this->nota->notasEstudiante($estudiante_id);

        $datos = array(
            "estudiantes" => $estudiante,
            "instituciones" => $instituciones,
            "notas" => $notas,
            "juicios" => $juicios,
            "asignaturas" => $this->nota->asignaturasDeEstudiante($estudiante_id)
        );
        $this->load->view("boletin_1", $datos);
    }
    
    public function buscarBoletinPasado($estudiante_id){
        
    }

    public function matriculas() {
        /* listado de matriculacion de estudiante */

        $datos = array(
            "pagina" => "estudiantes_v",
            "estudiantes" => $this->estudiante->listarTodo(),
            "cursos" => $this->curso->listarTodo()
        );
        $this->load->view("notas", $datos);
    }

    public function pagos() {
        /* listado de pagos */
        /* listado de matriculacion de estudiante */

        $datos = array(
            "pagina" => "estudiantes_v",
            "estudiantes" => $this->estudiante->listarTodo(),
            "cursos" => $this->curso->listarTodo()
        );
        $this->load->view("notas", $datos);
    }

    public function eliminarEstudiante($id) {
        if ($this->estudiante->eliminarDeVerdad($id)) {
            redirect("estudiantes/index/elimino");
        } else {
            redirect("estudiantes/index/noElimino");
        }
        redirect("estudiantes/index/ni Idea");
    }

    /* Promocion del estudiante desde aqui son las funciones de la promocion */
    /* codigo de ospino */

    /* codigo de ospino */


    /* codigo de plata */

    public function buscarNotas($estudiante_id) {
//        $notasPromediadas = array();
        $this->load->model("nota_model", "notas");
//periodos por año! -> modificar la consulta
        $periodos = $this->notas->numeroDePeriodos($estudiante_id);
//        print_r($periodos);
//        echo '<br/>';
//cantidad de periodos
        for ($i = 1; $i <= $periodos; $i++) {
            $asignaturasCalificadas = $this->notas->asignaturasCalificadas($estudiante_id, $i);
//            print_r($asignaturasCalificadas);
//            echo '<br/>';
            foreach ($asignaturasCalificadas as $value) {
                $sumatoria = 0;
                $cantidadnotas = 0;
                $notasCalificadas = $this->notas->buscarNotaEstudianteAsignatura($estudiante_id, $value->asignatura_id, $i);
//                print_r($notasCalificadas);
//                echo '<br/>';
                foreach ($notasCalificadas as $value1) {
                    $sumatoria+=$value1->nota;
                    $cantidadnotas++;
                }
                $promedio = $sumatoria / $cantidadnotas;
                $notasPromediadas[] = array(
                    "asignatura_id" => $value->asignatura_id,
                    "estudiante_id" => $estudiante_id,
                    "periodo" => $i,
                    "nota" => $promedio
                );
            }
        }
//        print_r($notasPromediadas);
//        echo '<br/>';
        if ($this->comprobasAprobavion($notasPromediadas, $periodos, $asignaturasCalificadas)) {
            $datos = array(
                "pagina" => "confirmacionPromocion",
                "mensaje" => "Gano el Año Felicitaciones!",
                "tipo" => "success",
                "estado" => 1,
                "estudiante_id" => $estudiante_id
            );
        } else {
            $datos = array(
                "pagina" => "confirmacionPromocion",
                "mensaje" => "Perdio el Año lo sentimos mucho",
                "tipo" => "warning",
                "estado" => 0,
                "estudiante_id" => $estudiante_id
            );
        }
        $this->load->view("notas", $datos);
    }

    public function comprobasAprobavion($notasEstudiante, $periodos, $asignaturas) {
        $desaprobadas = 0;
        $this->load->model("configuracion_model", "configuracion");
        foreach ($this->configuracion->listarTodo() as $value) {
            $datosAprovacion["minimaAprobacion"] = $value->minima_aprobacion;
            $datosAprovacion['calificacionMaxima'] = $value->calificacion_maxima;
            $datosAprovacion['maximaAsignaturasPerdidas'] = $value->max_asign_perdida;
        }
//echo 'Minimo para pasar debe tener una nota minima de: ' . $datosAprovacion['minimaAprobacion'] * $datosAprovacion['calificacionMaxima'];
//print_r($asignaturas);
        $calificado = array();
        for ($index = 1; $index <= $periodos; $index++) {
            foreach ($notasEstudiante as $key => $value) {
                if ($value["periodo"] == $index) {
                    foreach ($asignaturas as $value1) {
                        if ($value1->asignatura_id == $value['asignatura_id']) {
                            $calificado[$value['asignatura_id']]+=$value['nota'];
                        }
                    }
                }
            }
        }
//        print_r($calificado);
//        echo '<Br/>';
//Ahora falta verificar que se haya aprobado
        foreach ($calificado as $key => $value) {
//echo $value;
            if (($value / $periodos) >= ($datosAprovacion['minimaAprobacion'] * $datosAprovacion['calificacionMaxima'])) {
//                echo 'Aprobo con: ' . $value / $periodos;
//                echo '<Br/>';
            } else {
//                echo 'Desaprobo con: ' . $value / $periodos;
//                echo '<Br/>';
//Aqui invrementamos el contador de desaprobadas
                $desaprobadas++;
            }
        }

        if ($desaprobadas > $datosAprovacion['maximaAsignaturasPerdidas']) {
//            echo 'Perdio el AÑO';
//            echo '<BR/>';
            return FALSE;
        } else {
//            echo 'Gano el AÑO';
//            echo '<BR/>';
            return TRUE;
        }
    }

    public function promover($estudiante_id, $estado) {
        //Se Busca la matricula del estudiante, para sacar el curso al cual pertenece
        $matriculaDelEstudiante = $this->estudiante->listarUnEstudiante($estudiante_id);
//        print_r($matriculaDelEstudiante);
//        echo '<br/>';
//        echo '<br/>';
        //ahora hay que buscar el curso de su promocion
        foreach ($matriculaDelEstudiante as $value) {

            //se busca ahora el curso al cual este promueve
            $curso = $this->curso->listarUnCurso($value->grado_id, $value->grupo_id, $value->jornada_id);
//            print_r($curso);
//            echo '<br/>';
//            echo '<br/>';

            foreach ($curso as $value1) {
                //curso a promover
                $promocion = explode("-", $value1->promocion);
//                print_r($promocion);
//                echo '<br/>';
//                echo '<br/>';
                //cuando este gano el año, se promueve sino, no
                if ($estado) {
                    //se crea una nueva matricula con los datos nuevos que son el curso al cual pertenece
                    $matricula = array(
                        "estudiante_id" => $estudiante_id,
                        "sede_id" => $this->session->userdata("sede_id"),
                        "fecha" => date("Y-m-d"),
                        "curso_grado_id" => $promocion[0],
                        "curso_grupo_id" => $promocion[1],
                        "curso_jornada_id" => $promocion[2]
                    );
                    //Aqui tenemos que desabilitar la matricula que se cargo para crearla nuevamente
                    $this->estudiante->eliminarMatricula($estudiante_id);
                    $this->estudiante->guardarMatricula($matricula);
//                    print_r($matricula);
//                    echo '<br/>';
//                    echo '<br/>';
                } else {
                    //se crea una nueva matricula en el mismo curso, como no s epuede repetiir, se actualiza la matricula igual
                    $where = array(
                        "estudiante_id" => $estudiante_id
                    );
                    $set = array(
                        "fecha" => date("Y-m-d")
                    );
                    $this->estudiante->modificarMatricula($where, $set);
//                    print_r($matricula);
//                    echo '<br/>';
//                    echo '<br/>';
                }

                //se crea la matricula de igual forma gane o pierda
                /* creacion de la factura de la matricula */
                $this->load->model("configuracion_model", "configuracion");
                $this->load->model("producto_model", "producto");
                foreach ($this->configuracion->listarTodo() as $datos) {
                    $dias_de_matricula = $datos->dias_de_matricula;
                    $fecha_de_matricula = $datos->fecha_de_matricula;
                    $facturaNumero = $datos->factura_numero;
                    foreach ($this->producto->listarTodo() as $datos2) {
                        if ($datos->producto_matricula == $datos2->id) {
                            $producto = array(
                                "id" => $datos2->id,
                                "nombre" => $datos2->nombre,
                                "precio" => $datos2->precio
                            );
                        }
                    }
                }
                /* creacion de la factura de la matricula */
                $this->load->model("factura_model", "factura");
                $this->factura->guardar(array($producto), $estudiante_id, 0);
                $contenido = array(
                    "estudiante" => array(
                        "nombre" => $value->estudiante_nombre,
                        "apellido" => $value->estudiante_apellido,
                        "documento" => $value->estudiante_documento
                    ),
                    "producto" => $producto,
                    "diasMatricula" => $dias_de_matricula,
                    "fechaMatricula" => $fecha_de_matricula,
                    "factura_numero" => $facturaNumero
                );
                if ($this->configuracion->consultarDato("imprimir") == 1) {
                    $this->load->view("matricula_1", $contenido);
                }
            }
        }
        //si no sale nada, significa que el estudiante, no notas para ser promovido
    }

    /* codigo de plata */

    public function cosa($dato) {
        $this->load->model("configuracion_model", "configuracion");
        $datos = $this->configuracion->consultarDato($dato);
        echo $datos;
    }

}
