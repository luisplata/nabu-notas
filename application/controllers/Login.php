<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
//cargando al modelo
        $this->load->model("Usuario_model", "usuario");
        //$this->output->enable_profiler(TRUE);
    }

    public function index() {
        redirect("inicio");        
    }

    public function modulos() {
        $this->load->view("modulos");
    }

    public function cerrarSession() {
        $this->session->sess_destroy();
        redirect("inicio");
    }

    public function esta() {
        $usuario = array(
            "user" => $this->input->post("user"),
            "pass" => sha1($this->input->post("pass"))
        );
        if ($this->usuario->esta($usuario)) {
            //esta en la base de datos
            //se cargan los datos de session
            //y los datos de configuracion
            redirect("inicio/modulos");
            //echo "Esta";
        } else {
            //No esta en la base de datos se manda al login enseguida con un mensaje por GET
            //echo 'No esta';
            redirect("inicio/index/?mensaje=El usuario y contaseña no son validos");
        }
    }

    public function estaDocente() {
        //aramos  el array de busqueda
        $docente = array(
            "docente.documento" => $this->input->post("documento"),
            "docente.email" => $this->input->post("email")
        );
        if ($this->usuario->estaDocente($docente)) {
            redirect("inicio/moduloDocente");
        } else {
            redirect("inicio/docente/?mensaje=Usuario y contraseña invalidos");
        }
    }

}
