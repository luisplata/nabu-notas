<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Ajax extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $id = $this->input->post("id");
        $where = array(
            "eliminado" => "0",
            "id" => $id,
            "institucion_id" => $this->session->userdata("institucion_id")
        );
        $this->db->where($where);
        $producto = $this->db->get("producto")->result();

        foreach ($producto as $value) {
            echo "<tr>";
            echo "<td>" .
            $value->id
            . "</td>";
            echo "<td>" .
            $value->nombre
            . "</td>";
            echo "<td id='" . $value->id . "'>" .
            $value->precio .
            "<input type='hidden' name='nombre[]' value='" . $value->nombre . "' />" .
            "<input type='hidden' name='precio[]' id='precioProducto[]' value='" . $value->precio . "' />" .
            "<input type='hidden' name='id[]'  value='" . $value->id . "' />" .
            "</td>";
            echo "</tr>";
        }
    }

    public function producto() {
        $datos = array("mensajes" => "Hola soy json", "error" => false, "dato" => $_REQUEST['dato']);
        echo json_encode($datos);
    }

    public function mandarCorreo() {
        $para = "www.luisplata@gmail.com";
        $asunto = $this->input->post("asunto");
        $mensaje = $this->input->post("mensaje");
        //if (mail($para, $asunto, $mensaje)) {
        if (mail("www.luisplata@gmail.com", "Aqui probando", "Hola Mundo!")) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function identificarCliente() {
        //active record
        $this->db->like("documento", $this->input->post("documento"));
        $consulta = $this->db->get("cliente")->result();

        foreach ($consulta as $value) {
            echo '<input form="facturadoraPrincipal" type="text" readonly '
            . 'value="' . $value->nombre . '" name="nombre_credito" class="form-control"/>';
            echo '<input form="facturadoraPrincipal" type="hidden" readonly '
            . 'value="' . $value->documento . '" name="documento_credito" class="form-control"/>';
        }
    }

    public function cargarJuicioIndividual() {
//        echo $this->input->post("estudiante")."/".$this->input->post("juicio");
        $this->input->post("estudiante");
        $this->input->post("juicio");
        //active record
        //creamos el arreglo con los datos
        $juicio_cargado = array(
            "estudiante_id" => $this->input->post("estudiante"),
            "juicio_id" => $this->input->post("juicio"),
            "periodo" => $this->session->userdata("periodo_actual"),
            "fecha" => date("Y-m-d")
        );
        $this->db->insert("juicio_cargado", $juicio_cargado);
    }

}
