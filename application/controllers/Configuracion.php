<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Configuracion extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        $this->load->model("configuracion_model", "configuracion");
    }

    public function index() {

        $this->load->model("producto_model", "producto");
        $this->load->model("categoria_egreso_model", "categoriaEgreso");
        $datos = array(
            "pagina" => "configuracion_v",
            "configuracion" => $this->configuracion->listarTodo(),
            "productos" => $this->producto->listarTodo(),
            "categoriaEgresos" => $this->categoriaEgreso->listarTodo()
        );
        $this->load->view("notas", $datos);
    }

    public function guardar() {
        $configuracion = array(
            //"escudo" => $this->input->post("escudo"),
            "eliminar" => $this->input->post("eliminar"),
            "imprimir" => $this->input->post("imprimir"),
            "interes" => $this->input->post("interes") / 100,
            "calificacion_decimal" => $this->input->post("calificacion_decimal"),
            "calificacion_baja" => $this->input->post("calificacion_baja"),
            "calificacion_basico" => $this->input->post("calificacion_basico"),
            "calificacion_alta" => $this->input->post("calificacion_alta"),
            "calificacion_superior" => $this->input->post("calificacion_superior"),
            "porcentaje_bajo" => $this->input->post("porcentaje_bajo") / 100,
            "porcentaje_medio" => $this->input->post("porcentaje_medio") / 100,
            "porcentaje_alto" => $this->input->post("porcentaje_alto") / 100,
            "porcentaje_superior" => $this->input->post("porcentaje_superior") / 100,
            "calificacion_maxima" => $this->input->post("calificacion_maxima"),
            "fecha_mensualidad" => $this->input->post("fecha_mensualidad"),
            "meses_por_anio" => $this->input->post("meses_por_anio"),
            "producto_matricula" => $this->input->post("producto_id"),
            "categoria_egreso" => $this->input->post("categoria_egreso"),
            "sueldo_base" => $this->input->post("sueldo_base"),
            "numero_notas" => $this->input->post("numero_notas")
        );
        $where = array(
            "institucion_id" => $this->session->userdata("institucion_id")
        );
        if ($this->configuracion->guardar($configuracion, $where)) {
            redirect("configuracion");
        } else {
            redirect("configuracion");
        }
    }

    public function periodos() {
        $datos = array(
            "pagina" => "periodo_v",
            "peridos" => $this->configuracion->listarPeriodos()
        );
        $this->load->view("notas", $datos);
    }

}
