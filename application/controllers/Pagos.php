<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Pagos extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("pago_model", "pago");
    }

    public function index() {
        $datos = array(
            "pagina" => "pagos",
            "pagos" => $this->pago->listarTodo()
        );
        $this->load->view("facturacion", $datos);
    }

}
