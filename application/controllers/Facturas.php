<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Facturas extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("factura_model", "factura");
        //Cuando se active el modulo de facturación quitar esta linea de código
        redirect("login/modulos");
    }

    public function index() {
        $this->load->model("estudiante_model", "estudiante");
        $this->load->model("producto_model", "producto");
        $datos = array(
            "pagina" => "facturas",
            "facturas" => $this->factura->listarTodoNumero(),
            "estudiantes" => $this->estudiante->listarTodo(),
            "productos" => $this->producto->listarTodo()
        );
        $this->load->view("facturacion", $datos);
    }

    public function facturar() {
        $datos = array(
            "pagina" => "facturas",
            "facturas" => $this->factura->listarTodo()
        );
        $this->load->view("facturacion", $datos);
    }

    public function guardar() {

        $factura = array(
            "fecha" => date("Y-m-d"),
            "producto_id" => $this->input->post("producto_id"),
            "estudiante_id" => $this->input->post("estudiante_id"),
            "usuario_id" => $this->session->userdata("usuario_id"),
            "institucion_id" => $this->session->userdata("institucion_id"),
            "sede_id" => $this->session->userdata("sede_id"),
            "numero" => $this->session->userdata("factura_numero")
        );
        if (date("d") > $this->session->userdata("fecha_mensualidad")) {
            $factura['interes'] = $this->session->userdata("interes");
        }
        if ($this->factura->guardar($factura)) {
            
        } else {
            
        }
        redirect("facturas");
    }

    public function generarFactura() {
        //$this->output->enable_profiler(FALSE);
        $productos["nombre"] = $this->input->post("nombre");
        $productos["precio"] = $this->input->post("precio");
        $productos["id"] = $this->input->post("id");
        //agarramos el cliente que es por credito
        $estudiante = array(
            "codigo" => $this->input->post("codigo")
        );
        /* Sacar la id de este estudiante para la factura */
        $this->db->where($estudiante);
        $estudiantes = $this->db->get("estudiante")->result();
        //obtenemos al estudiante para pasarlo como parametro
        foreach ($estudiantes as $value) {
            $estudiante['id'] = $value->id;
            $estudiante['nombre'] = $value->nombre;
            $estudiante['apellido'] = $value->apellido;
            $estudiante['documento'] = $value->documento;
        }

        //validar si hay datos
        if (empty($productos['id'])) {
            redirect("facturas/");
        }
        //armando el arreglo de los productos
        $productosTotal = array();
        $producto = array();
        foreach ($productos['nombre'] as $value) {
            $producto['nombre'][] = $value;
        }
        foreach ($productos['precio'] as $value) {
            $producto['precio'][] = $value;
        }
        foreach ($productos['id'] as $value) {
            $producto['id'][] = $value;
        }
        //ordenando datos
        for ($i = 0; $i < count($producto['id']); $i++) {
            $productosTotal[] = array(
                "nombre" => $producto['nombre'][$i],
                "precio" => $producto['precio'][$i],
                "id" => $producto['id'][$i]
            );
        }
        $banco = $this->input->post("banco");
        //guardamos el soporte
        if ($this->factura->guardar($productosTotal, $estudiante['id'], $banco)) {
            //arreglamos los datos para su imprecion
            $datos = array(
                "productos" => $productosTotal,
                "totalDeTodo" => $this->input->post("totalDeTodo"),
                "cliente" => $estudiante
            );
            //preguntamos si va a imprimir
            if ($this->session->userdata("imprimir") == 1) {
                $this->load->view("plantilla_1", $datos);
            } else {
                redirect("facturas");
            }
        } else {
            //redirect("facturas");
        }
    }

    /* Egresos de la facturadora */

    public function egresos() {
        //aqui mostrara el listado de egresos y para un nuevo egreso
        $datos = array(
            "pagina" => "egresos_v",
            "egresos" => $this->factura->listarTodoEgresos()
        );
        $this->load->view("facturacion", $datos);
    }

    public function nuevoEgreso() {
        $egreso = array(
        "descripcion" => $this->input->post("descripcion"),
        "cantidad" => $this->input->post("cantidad"),
        "fecha" => date("Y-m-d"),
        "usuario_id" => $this->session->userdata("usuario_id"),
        "institucion_id" => $this->session->userdata("institucion_id")
        );
        if ($this->factura->nuevoEgreso($egreso)) {
            
        } else {
            
        }
        redirect("facturas/egresos");
    }

    /* Egresos de la facturadora */
}
