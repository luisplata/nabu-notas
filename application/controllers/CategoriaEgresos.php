<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CategoriaEgresos extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("categoria_egreso_model", "categoriaEgreso");
    }

    public function index() {
        $datos = array(
            "pagina" => "categoriaEgreso_v",
            "categoriaEgresos" => $this->categoriaEgreso->listarTodo(),
        );
        $this->load->view("notas", $datos);
    }

    public function guardarCategoriaEgreso() {
        $categoriaEgreso = array(
            "nombre" => $this->input->post("nombre"),
            "descripcion" => $this->input->post("descripcion"),
            "institucion_id" => $this->session->userdata("institucion_id")
        );
        if ($this->categoriaEgreso->guardar($categoriaEgreso)) {
            
        } else {
            
        }
        redirect("categoriaEgresos");
    }

}
