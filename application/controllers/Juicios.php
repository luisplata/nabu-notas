<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Juicios extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("juicio_model", "juicio");
        $this->load->model("materia_model", "materia");
        $this->load->model("curso_model", "curso");
        $this->load->model("docente_model", "docente");
        $this->load->model("estudiante_model", "estudiante");
//        $this->output->enable_profiler(TRUE);
    }

    public function index() {
        $datos = array(
            "pagina" => "juicio_v",
            "juicios" => $this->juicio->listarTodoDocente()
        );
        $this->load->view("notasDocente", $datos);
    }

    public function crearJuicio() {
        $datos = array(
            "pagina" => "crearJuicio",
            "asignaturas" => $this->materia->listarTodoAsignaturas(),
            "juicios" => $this->juicio->listarTodoDocente(),
        );
        $this->load->view("notasDocente", $datos);
    }

    public function cargarJuicioPorCurso() {

        $datos = array(
            "pagina" => "cargarJuicioPorCurso",
            "cursos" => $this->curso->cursoDocente(),
            "juicios" => $this->juicio->listarTodoDocente()
        );
        $this->load->view("notasDocente", $datos);
    }

    public function cargarJuicio() {
        $this->load->model("curso_model", "curso");
        $datos = array(
            "pagina" => "cargarJuicio",
            "cursos" => $this->curso->cursoDocente()
        );
        $this->load->view("notasDocente", $datos);
    }

    public function crearCargarJuicio() {

        /* grado-grupo-jornada */
        $curso = explode("-", $this->input->post("curso"));

//        $this->load->view("arrastrable");
        $datos = array(
            "pagina" => "crearCargarJuicio",
            "estudiantes" => $this->estudiante->listarCurso($curso[0], $curso[1], $curso[2]),
            "juicios" => $this->juicio->listarTodoDocente(),
            "asignaturas" => $this->materia->listarTodoAsignaturas()
        );
        $this->load->view("notasDocente", $datos);
    }

    public function crearCargarJuicioPorCurso() {
        $this->load->model("estudiante_model", "estudiante");
        $this->load->model("materia_model", "materia");
        /* grado-grupo-jornada */
        $curso = explode("-", $this->input->post("curso"));
        print_r($curso);
        $juicio_id = $this->input->post("juicio_id");
        //como se va a cargar el juicio a todo un curso se hara lo sieguiente
        //Se van a buscar a todos los alumnos de ese curso
        //se creara el array donde este ese alumno_id y el juicio_id
        //y se insertara en la db
        /* Buscando los estudiante con curso this 
         * Ahora solo falta es colocarle la escala valorativa
         */
        $listaDeEstudiantes = $this->estudiante->listarCurso($curso[0], $curso[1], $curso[2]);
        foreach ($listaDeEstudiantes as $value) {
            $juicio_cargado = array(
                "estudiante_id" => $value->estudiante_id,
                "juicio_id" => $juicio_id,
                "periodo" => $this->session->userdata("periodo_actual"),
                "fecha" => date("Y-m-d")
            );
            $this->db->insert("juicio_cargado", $juicio_cargado);
        }
        redirect("juicios/cargarJuicioPorCurso/CargoExitosamente");
    }

    public function guardarCargarjuicio() {
        $juicio_cargado = array(
            "estudiante_id" => $this->input->post("estudiante_id"),
            "juicio_id" => $this->input->post("juicio_id"),
            "periodo" => $this->session->userdata("periodo_actual"),
        );
        if ($this->juicio->guardarCargarJuicio($juicio_cargado)) {
            redirect("juicios/cargarJuicio");
        } else {
            redirect("juicios/crearCargarJuicio");
        }
    }

    public function guardarJuicio() {
        //se arma el array del juicio
        $juicio = array(
            "docente_id" => $this->session->userdata("docente_id"),
            "descripcion" => $this->input->post("descripcion"),
            "asignatura_id" => $this->input->post("asignatura_id"),
        );
        if ($this->juicio->guardar($juicio)) {
            redirect("juicios/crearJuicio");
        } else {
            redirect("juicios/");
        }
    }

    public function eliminar($id) {
        if ($this->juicio->eliminar($id)) {
            redirect("juicios/crearJuicio");
        } else {
            redirect("juicios/");
        }
    }

    public function modificarJuicio($id) {
        $datos = array(
            "juicio" => $this->juicio->listarUnJuicio($id),
            "pagina" => "modificarJuicio"
        );
        $this->load->view("notasDocente", $datos);
    }

    public function modificar() {
        $juicio = array(
            "id" => $this->input->post("id"),
            "asignatura_id" => $this->input->post("asignatura_id"),
            "descripcion" => $this->input->post("descripcion"),
        );
        if ($this->juicio->modificar($juicio)) {
            
        } else {
            
        }
        redirect("juicios/crearJuicio");
    }

    public function arratable() {
        $this->load->view("arrastrable");
    }

    public function arratableConDiv() {
        $this->load->view("cajaDeCosas");
    }

    public function calificarUnEstudiante() {
        $datos = array(
            "pagina" => "calificarJuicio",
            "cursos" => $this->curso->cursoDocente()
        );
        $this->load->view("notasDocente", $datos);
    }

    public function calificarJuicioEstudiante() {

        /* grado-grupo-jornada */
        $curso = explode("-", $this->input->post("curso"));

//        $this->load->view("arrastrable");
        $datos = array(
            "pagina" => "calificarJuicioEstudiante",
            "estudiantes" => $this->estudiante->listarCurso($curso[0], $curso[1], $curso[2]),
            "juicios" => $this->juicio->listarTodoDocente(),
            "asignaturas" => $this->materia->listarTodoAsignaturas()
        );
        /* falta cargar los juicio y calificarlos
         * la vista esta en view/calificarJuicioEstudiante.php
         * se va a buscar los juicios cargados del estudiante y colocar para calificar
         */
        $this->load->view("notasDocente", $datos);
    }

    public function calificarJuicioEstudianteUnico($idEstudiante) {
        /* Buscar los juicios y calificar por estudiante
         * Se buscaran los juicios del estudiante
         * que tengan su id
         * en el periodo actual
         * 
         */
        $where = array(
            "estudiante_id" => $idEstudiante,
            "periodo" => $this->session->userdata("periodo_actual"),
            "juicio_cargado.eliminado" => "0"
        );
        $this->db->join("juicio", "juicio.id = juicio_cargado.juicio_id");
        $this->db->join("asignatura", "juicio.asignatura_id = asignatura.id");
        $this->db->where($where);
        //print_r($this->db->get("juicio_cargado")->result());
        $datos = array(
            "juicios_cargados" => $this->db->get("juicio_cargado")->result()
        );

        /* cargamos datos de configuracion
         * nota maxima
         * calificaciones en letras
         * si es calficacion decimal o no
         */
        foreach ($this->configuracion_model->listartodo() as $value) {
            $datos['calificacion_maxima'] = $value->calificacion_maxima;
            $datos['calificacion_decimal'] = $value->calificacion_decimal;
            $datos['calificacion_alta'] = $value->calificacion_alta;
            $datos['calificacion_superior'] = $value->calificacion_superior;
            $datos['calificacion_basico'] = $value->calificacion_basico;
            $datos['calificacion_baja'] = $value->calificacion_baja;
        }
        $this->load->view("calificarJuicioEstudianteUnico", $datos);
    }

    public function calificarJuicio() {
//        print_r($this->input->post("estudiante_id"));
//        print_r($this->input->post("juicio_id"));
//        print_r($this->input->post("periodo"));

        $juicios["estudiante_id"] = $this->input->post("estudiante_id");
        $juicios["periodo"] = $this->input->post("periodo");
        $juicios["juicio_id"] = $this->input->post("juicio_id");
        $juicios["nota"] = $this->input->post("nota");


        //armando el arreglo de los productos
        $productosTotal = array();
        $juicioCalificado = array();
        foreach ($juicios['estudiante_id'] as $value) {
            $juicioCalificado['estudiante_id'][] = $value;
            $estudiante_id = $value;
        }
        foreach ($juicios['nota'] as $value) {
            $juicioCalificado['nota'][] = $value;
        }
        foreach ($juicios['periodo'] as $value) {
            $juicioCalificado['periodo'][] = $value;
        }
        foreach ($juicios['juicio_id'] as $value) {
            $juicioCalificado['juicio_id'][] = $value;
        }
        //ordenando datos
        for ($i = 0; $i < count($juicioCalificado['juicio_id']); $i++) {
            $productosTotal = array(
                "estudiante_id" => $juicioCalificado['estudiante_id'][$i],
                "periodo" => $juicioCalificado['periodo'][$i],
                "juicio_id" => $juicioCalificado['juicio_id'][$i],
                "nota" => $juicioCalificado['nota'][$i]
            );
            if ($productosTotal['nota'] == "") {
                
            } else {
                if ($this->juicio->calificarJuicio($productosTotal)) {
//                $productosTotalCalificado[] = $productosTotal;
//                echo 'actualizo';
                } else {
//                echo 'no actualzo';
                }
            }
        }
//        echo '<br/>';
//        print_r($this->input->post("nota"));
//        echo '<br/>';
//        print_r($productosTotalCalificado);

        redirect("juicios/calificarJuicioEstudianteUnico/" . $estudiante_id);
    }


}
