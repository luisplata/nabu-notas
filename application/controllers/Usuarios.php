<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Usuarios extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("administrador_model", "admin");
        $this->load->model("Usuario_model", "user");
    }

    public function index() {
        //lista de usuarios    
        //listamos los cursos disponibles
        $datos = array(
            "pagina" => "administradores_v",
            "administradores" => $this->admin->listarTodo()
        );
        $this->load->view("notas", $datos);
    }

    //Este proceso debe hacerlo solo un metodo
    public function guardarUsuario() {
        //guardar el nuevo usuario
        $administrador = array(
            "nombre" => $this->input->post("nombre"),
            "apellido" => $this->input->post("apellido"),
            "documento" => $this->input->post("documento"),
            "email" => $this->input->post("email"),
            "telefono" => $this->input->post("telefono"),
            "sede_id" => $this->session->userdata("sede_id"),
            "institucion_id" => $this->session->userdata("institucion_id")
        );
        if ($this->admin->guardar($administrador)) {
            //redirecciona a la pagina de la lista de usuarios
            //creamos ahora el usuario de esta persona
            $administrador_ID = $this->db->insert_id();
            $usuario = array(
                "user" => $administrador["email"],
                "pass" => sha1($administrador["documento"]),
                "grado" => 2,
                "valor" => 0,
                "administrador_id" => $administrador_ID,
            );
            if ($this->user->guardar($usuario)) {
                redirect("usuarios/?mensaje=Se creo el usuario con exito&tipo=success");
            } else {
                redirect("usuarios/?mensaje=seguardo el administrador pero no el usuario&tipo=info");
            }
        } else {
            //redirecciona a la pagina de lista de usuarios con mensaje de error
            redirect("usuarios/?mensaje=No se pudo guardar el usuario&tipo=alert");
        }
    }

    public function nuevoUsuario() {
        //crear un nuevo usuario
        $datos = array(
            "pagina" => "administradores_nuevoUsuario"
        );
        $this->load->view("notas", $datos);
    }

    public function Eliminar($id) {
        //eliminar un usuario
        if ($this->admin->eliminar($id)) {
            //elimino bien
            redirect("usuarios/?mensaje=se elimino el usuario con exito&tipo=success");
        } else {
            //no elimino bien
            redirect("usuarios/?mensaje=no elimino el usuario&tipo=alert");
        }
    }

    public function desabilitar($id) {
        //desabilitar al usuario
    }

    public function GetUsuarioJson($id) {
        echo json_encode($this->admin->getUser($id)[0]);
    }

    public function modificar() {
        //modificar campos basicos del usuario
        $usuario = array(
            "nombre" => $this->input->post("nombre"),
            "apellido" => $this->input->post("apellido"),
            "telefono" => $this->input->post("telefono")
        );
        if ($this->admin->modificar($usuario, $this->input->post("id"))) {
            redirect("usuarios/?mensaje=se editó el usuario con exito&tipo=success");
        } else {
            redirect("usuarios/?mensaje=no se pudo editar el usuario");
        }
    }

    public function cambiarPass($usuario) {
        //funcion exclusiba para cambiar la contraseña
    }

    public function recuperarPass($usuario) {
        //lo que se va a hacer aqui es una funcion donde:
        /* Lo que va a hacer el usuario es indicar su correo
         * despues se comprobara de que ese correo exista
         * si existe se cambiara la contraseña
         * y se mandara al correo la nueva contraseña para que la cambie posteriormente
         */
    }

}
