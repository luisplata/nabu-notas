<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Calificaciones extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->output->enable_profiler(TRUE);
        $this->load->model("nota_model", "nota");
        $this->load->model("curso_model", "curso");
        $this->load->model("estudiante_model", "estudiante");
    }

    public function seleccionarCurso() {

        $datos = array(
            "pagina" => "cursosDocente",
            "cursos" => $this->curso->cursoDocente()
        );

        $this->load->view("notasDocente", $datos);
    }

    public function seleccionarAsignatura($curso) {
        //sacamos los datos de array de get
        $curso = explode("-", $curso);

        $datos = array(
            "pagina" => "asignaturaDocente",
            "grado_id" => $curso[0],
            "grupo_id" => $curso[1],
            "jornada_id" => $curso[2],
            "asignaturas" => $this->curso->asignaturaDocente($curso)
        );

        $this->load->view("notasDocente", $datos);
    }

    public function calificarCurso($curso, $asignatura) {
        //sacamos los datos de array de get
        $curso = explode("-", $curso);
        $datos = array(
            "pagina" => "calificarCurso",
            "asignatura" => $asignatura,
            "curso" => $curso,
            "estudiantes" => $this->estudiante->listarCurso($curso[0], $curso[1], $curso[2])
        );

        $this->load->view("notasDocente", $datos);
    }

    public function notasEstudiantes($idEstudiante, $asignatura) {
        /* Vamos a cargar las notas si existen
         * buscamos las notas del estudiante si existen
         * si existen se cargan y se muestran
         * si no se cargan y listo
         * logica:
         * se buscan las notas con los datos suministrados
         * si existen se obtienen y se cargan en la vista
         * si tienen 0 como nota se deja editar
         * si tiene una nota distinta no se deja editar
         * si no existen se ponen como nuevos
         */
        $where = array(
            "estudiante_id" => $idEstudiante,
            "periodo" => $this->session->userdata("periodo_actual"),
            "docente_id" => $this->session->userdata("docente_id"),
            "asignatura_id" => $asignatura,
            "eliminado" => "0"
        );
        $nota = $this->nota->buscarNotaEstudiante($where);
        if ($nota->num_rows > 0) {
            //significa que si hay notas de este estudiante
            foreach ($nota->result() as $value) {
                //el numero de notas debe ser la cantidad que en configuracion
                //se coloca
                $notas[] = array(
                    "id" => $value->id,
                    "nota" => $value->nota,
                    "periodo" => $value->periodo,
                    "estudiante_id" => $value->estudiante_id,
                    "docente_id" => $value->docente_id,
                    "asignatura_id" => $value->asignatura_id,
                    "eliminado" => $value->eliminado,
                );
            }
            //mandamos los datos de las notas
            $datos["notas"] = $notas;
        } else {
            //no hay notas de este estudiante
        }
        $datos["estudiante_id"] = $idEstudiante;
        $datos["asignatura_id"] = $asignatura;


        //sacamos los datos de array de get

        $this->load->view("calificarUnEstudiante", $datos);
    }

    public function calificarEstudiante() {
        $calificacionExitosa = FALSE;
        //hacemos el array del registro de la nota
        //creamos un for que recorra las veces de la cantidad de notas
        for ($i = 1; $i <= $this->session->userdata("numero_notas"); $i++) {
            $nota_id = explode(":", $this->input->post("nota" . $i));
            //buscamos si hay dato de id en la nota se actualizara la nota
            //el formato es (nota:id)

            /*
             * Se hara lo siguiente
             * se buscara su hay dato de id
             * si lo hay se arma el array para actualizar
             * si no se crea una nueva nota y se guarda
             * 
             */
            if ($nota_id[1] != NULL) {
                //significa que no hay dato - Se Actualiza -
                $where = array(
                    "id" => $nota_id[1]
                );
                $set = array(
                    "nota" => $nota_id[0]
                );
                //actualizando
                if ($this->nota->actualizar($where, $set)) {
                    //se actualizo   
                }
            } else {
                //significa que hay dato - Se Guarda -
                //Aqui vamos a ingresar a la base de datos el registro de la nota
                //cuando no hay nada en la base de datos
                $nota = array(
                    "nota" => $this->input->post("nota" . $i),
                    "fecha" => date("Y-m-d"),
                    "periodo" => $this->session->userdata("periodo_actual"),
                    "estudiante_id" => $this->input->post("estudiante_id"),
                    "docente_id" => $this->session->userdata("docente_id"),
                    "asignatura_id" => $this->input->post("asignatura_id")
                );
                if ($this->nota->guardar($nota)) {
                    $calificacionExitosa = TRUE;
                }
            }
        }
        if ($calificacionExitosa) {
            
        } else {
            //hay error
        }
        redirect("calificaciones/notasEstudiantes/" . $nota['estudiante_id'] . "/" . $nota['asignatura_id'] . "/" . $nota);
    }

}
