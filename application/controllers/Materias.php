<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Materias extends CI_Controller {

    function __construct() {
        parent::__construct();
//        $this->output->enable_profiler(TRUE);
        $this->load->model("materia_model", "materia");
    }

    public function index() {
        
    }

    public function areas() {
        $datos = array(
            "pagina" => "areas_v",
            "areas" => $this->materia->listarTodoAreas()
        );
        $this->load->view("notas", $datos);
    }

    public function guardarArea() {
        //armando el array que se guardara
        $area = array(
            "nombre" => $this->input->post("nombre"),
            "codigo" => $this->input->post("codigo"),
            "sede_id" => $this->session->userdata("sede_id")
        );
        if ($this->materia->guardarArea($area)) {
            
        } else {
            
        }
        redirect("materias/areas");
    }
    

    public function asignaturas() {
        $datos = array(
            "pagina" => "asignaturas_v",
            "asignaturas" => $this->materia->listarTodoAsignaturas(),
            "areas" => $this->materia->listarTodoAreas()
        );
        $this->load->view("notas", $datos);
    }

    public function guardarAsignatura() {
        //creando el array
        $asignatura = array(
            "nombre" => $this->input->post("nombre"),
            "codigo" => $this->input->post("codigo"),
            "area_id" => $this->input->post("area_id"),
            "sede_id" => $this->session->userdata("sede_id")
        );
        if ($this->materia->guardarAsignatura($asignatura)) {
            
        } else {
            
        }
        redirect("materias/asignaturas");
    }

    public function cargaAcademica() {
        $this->load->model("curso_model", "curso");
        $this->load->model("docente_model", "docente");
        $datos = array(
            "pagina" => "cargaAcademica_v",
            "cargasAcademicas" => $this->materia->listarTodoCargaAcademica(),
            "areas" => $this->materia->listarTodoAreas(),
            "docentes" => $this->docente->listarTodo(),
            "asignaturas" => $this->materia->listarTodoAsignaturas(),
            "cursos" => $this->curso->listarTodo(),
        );
        $this->load->view("notas", $datos);
    }

    public function guardarCargaAcademica() {
        //haciendo el array de la carga academica
        //hacemos un explode del curso para sacar las llaves
        $curso = explode("-", $this->input->post("curso"));
        $cargaAcademica = array(
            "asignatura_id" => $this->input->post("asignatura_id"),
            "docente_id" => $this->input->post("docente_id"),
            "curso_grado_id" => $curso[0],
            "curso_grupo_id" => $curso[1],
            "curso_jornada_id" => $curso[2],
            "sede_id" => $this->session->userdata("sede_id")
        );
        if ($this->materia->guardarCargaAcademica($cargaAcademica)) {
            
        } else {
            
        }
        redirect("materias/cargaAcademica");
    }
	
	//codigo de ospino
    public function actualizarArea() {
     
        $area_modificado = array(
            "nombre" => $this->input->post("nombre"),
            "codigo" => $this->input->post("codigo"),
            "sede_id" => $this->session->userdata("sede_id")
        );
        $where = array(
            "id" => $this->input->post("id")
        );

        $this->materia->actualizarArea($where,$area_modificado);
    }

    public function eliminarAsignatura($id){
        $where = array(
            "id"=>$id
        );
        $set = array(
            "eliminado"=>"1"
        );
       
    }
}
