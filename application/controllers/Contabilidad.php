<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Contabilidad extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        $this->load->model("docente_model", "docente");
        $this->load->model("configuracion_model", "configuracion");
        //Cuando se active el modulo de contabilidad quitar esta linea de código
        redirect("login/modulos");
    }

    public function index() {
        $datos = array(
            "pagina" => "index",
            "contenido" => "Aqui esta toda la parte de contabilidad de la institucion"
        );
        $this->load->view("contabilidad", $datos);
    }

    public function pagoNomina() {
        $datos = array(
            "pagina" => "nomina",
            "docentes" => $this->docente->listarTodo()
        );
        $this->load->view("contabilidad", $datos);
    }

    public function generarNomina() {
//        print_r($this->input->post("pago"));
        //echo $this->input->post("pago");
        //array con id's de docentes
        //calculo el pago

        foreach ($this->configuracion->listarTodo() as $value) {
            $sueldoBase = $value->sueldo_base;
        }
        //adicionales de suma
        /* variables estaticas
         * - salud
         * - pension
         * 
         * total a pagar = sueldoBase-salud-pension
         */
        $salud = $sueldoBase * 0.085;
        $pension = $sueldoBase * 0.085;
        //adicionales de resta
        //Esto se le paga a todos los docentes
        $sueldoPagar = $sueldoBase - $salud - $pension;

        $aPagar = array();
        foreach ($this->input->post("pago") as $key => $value) {
//            echo $key . " - " . $value;
//            echo "<br/>";
            $aPagar[] = array(
                "docente_id" => $value,
                "totalPagado" => $sueldoPagar,
                "sueldoBase" => $sueldoBase,
                "salud" => $salud,
                "pension" => $pension
            );
        }
//        print_r($aPagar);
        //echo $sueldoPagar;
        $datos = array(
            "datos" => $aPagar,
            "docentes" => $this->docente->listarTodo(),
            "configuracion" => $this->configuracion->listarTodo()
        );
        $this->load->view("pagoNomina_1", $datos);
    }

    public function confirmarNomina() {
//        print_r($this->input->post());
        /* Se ingresan los datos a la base de datos
         * se manejara la uçid de la categoria mientras se hace la vista de las 
         * categorias de egreso y se agrega el egreso de nomina en configuracion
         */

        foreach ($this->input->post("nombre") as $value0) {
            $mensaje = "Se le pago ";
            foreach ($this->input->post("sueldo") as $value1) {
                $mensaje.=$value1 . " a ";
                $sueldo = $value1;
            }
            foreach ($this->input->post("nombre") as $value2) {
                $mensaje.=$value2 . " ";
            }
            foreach ($this->input->post("apellido") as $value3) {
                $mensaje.=$value3 . " por nomina";
            }
//            echo $mensaje;
            $egreso = array(
                "descripcion" => $mensaje,
                "cantidad" => $sueldo,
                "fecha" => date("Y-m-d"),
                "usuario_id" => $this->session->userdata("usuario_id"),
                "institucion_id" => $this->session->userdata("institucion_id"),
                "categoria_egreso_id" => $this->input->post("categoria_egreso_id")
            );
//            print_r($egreso);
            $this->load->model("factura_model", "factura");
            if ($this->factura->nuevoEgreso($egreso)) {
                
            } else {
                
            }
            redirect("facturas/egresos");
        }
    }

}
