<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Institucion_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function listarTodo() {
        //Creamos el where
        $where = array(
            "institucion.eliminado" => "0"
        );
        $this->db->where($where);
        //devolvemos lo consultado
        return $this->db->get("institucion")->result();
    }

    public function listarUnaInstitucion($id) {
        //Creamos el where
        $where = array(
            "institucion.eliminado" => "0",
            "institucion.id" => $id
        );
        $this->db->where($where);
        //devolvemos lo consultado
        return $this->db->get("institucion")->result();
    }

    public function guardar($institucion) {
        if ($this->db->insert("institucion", $institucion)) {
            $institucion_id = $this->db->insert_id();
            /* hay que crear los datos necesarios para esta institucion
             * Los cuales son:
             * la sede principal
             * los datos de configuracion
             * el usuario de la sede y la des colegio
             *  se crea un administrativo a la sede y a la institucion
             *  la sede tendra grado 2.0 y la institucion 2.10
             */
            $sede = array(
                "nombre" => "Principal",
                "direccion" => $institucion['direccion'],
                "telefono" => $institucion['telefono'],
                "email" => $institucion['email'],
                "institucion_id" => $institucion_id
            );
            $this->db->insert("sede", $sede);
            $sede_id = $this->db->insert_id();
            //Crear los datos de configuracion
            $configuracion = array(
                "institucion_id" => $institucion_id
            );
            $this->db->insert("configuracion", $configuracion);
            //configuracion
            $administrativo = array(
                "nombre" => $institucion['nombre'],
                "documento" => $institucion['nit'],
                "email" => $institucion['email'],
                "telefono" => $institucion['telefono'],
                "institucion_id" => $institucion_id,
                "sede_id" => $sede_id,
                "apellido" => "Principal",
            );
            $this->db->insert("administrador", $administrativo);
            $administrativo_id = $this->db->insert_id();
            /* Despue de insertar lo datos creamos el usuario */
            $usuario = array(
                "user" => $administrativo['email'],
                "pass" => sha1($administrativo['documento']),
                "grado" => "2",
                "valor" => "0",
                "administrador_id" => $administrativo_id
            );
            $this->db->insert("usuario", $usuario);
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
