<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Docente_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function ListarTodo() {
        //sentencia WHERE
        $where = array(
            "docente.eliminado" => "0",
            "docente.sede_id" => $this->session->userdata("sede_id")
        );
        $this->db->where($where);
        //los datos a seleccionar
        $this->db->select("docente.nombre AS nombre,"
                . "docente.apellido AS apellido,"
                . "docente.id AS id,"
                . "docente.documento AS documento,"
                . "docente.telefono AS telefono,"
                . "docente.email AS email,"
                . "sede.nombre AS sede_nombre,"
                . "sede.id AS sede_id");
        //los join
        $this->db->join("sede", "sede.id = docente.sede_id");
        //retornamos
        return $this->db->get("docente")->result();
    }

    public function Guardar($docente) {
        if ($this->db->insert("docente", $docente)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function Eliminar($id) {
        $this->db->set(array(
            "eliminado" => "1"
        ));
        $this->db->where("id", $id);
        return $this->db->update("docente");
    }

}
