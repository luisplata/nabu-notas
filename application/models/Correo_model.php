<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Correo_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->library('email');
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $this->email->initialize($config);
    }

    public function mandarCorreoDocente($titulo, $mensaje, $responder) {
        $this->load->model("docente_model", "docente");
        $docentes = $this->docente->listarTodo();
        $this->email->from($responder);
        foreach ($docentes as $value) {
            $para = $value->email;
            $titulo = $titulo;
            $head = "<html>
<head>  
</head>
<body>";
            $foot = "</body>
</html>";
            $mensajes = $head . $mensaje . $foot;

            $cabeceras = 'From: ' . $responder . ' <' . $responder . '>' . "\r\n" .
                    //'Content-type: text/html; charset=iso-8859-1' . "\r\n" .
                    //'MIME-Version: 1.0' . "\r\n" .

                    'X-Mailer: PHP/' . phpversion();

//            print_r($mensajes);
            if (!mail($para, $titulo, $mensaje, $cabeceras)) {
                return FALSE;
            }
            //echo 'mando correo a ' . $value->email;
//            //haremos el ciclo para el correo
//            $this->email->to($value->email);
//            $this->email->subject($titulo);
//            $this->email->message($mensaje);
//            if (!$this->email->send()) {
//                print_r($this->email->print_debugger());
//                return FALSE;
//            }
        }
        return TRUE;
    }

}
