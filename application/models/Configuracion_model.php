<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class configuracion_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
    }

    public function listarTodo() {
        //where
        $where = array(
            "configuracion.institucion_id" => $this->session->userdata("institucion_id")
        );
        $this->db->where($where);
        //select
        /**/
        //join
        $this->db->join("periodo", "periodo.id = configuracion.periodo_actual");
        //retorno
        return $this->db->get("configuracion")->result();
    }

    public function guardar($configuracion, $where) {
        $this->db->where($where);
        if ($this->db->update("configuracion", $configuracion)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function listarPeriodos() {
        $where = array(
            "institucion_id" => $this->session->userdata("institucion_id")
        );
        $this->db->where($where);
        return $this->db->get("periodo")->result();
    }

    public function log($accion = null, $tipo = "success") {
        $log = array(
            "accion" => $accion,
            "fecha" => date("Y-m-d H:m:s"),
            "empresa_id" => $this->session->userdata("empresa_id"),
            "sede_id" => $this->session->userdata("sede_id"),
            "usuario_id" => $this->session->userdata("usuario_id"),
            "ip" => $this->input->ip_address(),
            "navegador" => $this->input->user_agent()
        );
        if ($this->db->insert("log", $log)) {
            $this->session->set_userdata("mensaje", $accion);
            $this->session->set_userdata("tipo", $tipo);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function logInterno($accion) {
        $log = array(
            "accion" => $accion,
            "fecha" => date("Y-m-d H:m:s"),
            "empresa_id" => $this->session->userdata("empresa_id"),
            "sede_id" => $this->session->userdata("sede_id"),
            "usuario_id" => $this->session->userdata("usuario_id"),
            "ip" => $this->input->ip_address(),
            "navegador" => $this->input->user_agent()
        );
        if ($this->db->insert("log", $log)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function periodoActual($id) {
        $where = array(
            "institucion_id" => $this->session->userdata("institucion_id")
        );
        $this->db->where($where);
        $set = array(
            "periodo_actual" => $id
        );
        if ($this->db->update("configuracion", $set)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function periodo() {
        $where = array(
            "configuracion.institucion_id" => $this->session->userdata("institucion_id")
        );
        $this->db->where($where);
        $this->db->join("periodo", "periodo.id=configuracion.periodo_actual");
        $configuracion = $this->db->get("configuracion")->result();
        if ($configuracion) {
            foreach ($configuracion as $value) {
                $periodo = array(
                    "id" => $value->periodo_actual,
                    "periodo" => $value->periodo,
                    "fecha_inicio" => $value->fecha_inicio,
                    "fecha_final" => $value->fecha_final
                );
                return $periodo;
            }
        } else {
            return 0;
        }
    }

    public function acceso($grados) {
        /* Lo que vamos a hacer aqui es que se mande en un array los grados que
         * pueden estar aqui, los que no se mandara al inicio
         * el formato del parametro es un array
         * grados [0,2,1]
         */
        $puede = FALSE;
        foreach ($grados as $value) {
            if ($this->session->userdata("grado") == $value) {
                $puede = TRUE;
            }
        }
        if ($puede) {
            //aqui no vamos a hacer nada, ya que puede estar aqui            
        } else {
            //aqui redireccionamos al inicio ya que no tiene los permisos
            redirect();
        }
    }

    public function acceso_docente() {
        /*
         * Restriccion de acceso a los docentes
         * por fechas
         */


        if ($this->session->userdata("docente_id") != NULL) {
            
        }
    }

    public function consultarDato($dato) {
        $where = array(
            "eliminado" => 0,
            "institucion_id" => $this->session->userdata("institucion_id")
        );
        $this->db->where($where);
        $this->db->select($dato);
        foreach ($this->db->get("configuracion")->result() as $value) {
            return $value->$dato;
        }
    }

}
