<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Estudiante_model extends CI_Model {

    function __construct() {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
    }

    public function listarTodo() {
        //where
        $where = array(
            "institucion.activa" => "1",
            "estudiante.eliminado" => "0",
            "estudiante.sede_id" => $this->session->userdata("sede_id"),
            "matricula.eliminado" => "0"
        );
        $this->db->where($where);
        //los datos que voy a seleccionar
        /**/
        $this->db->select("estudiante.nombre AS nombre,"
                . "estudiante.apellido AS apellido,"
                . "estudiante.codigo AS codigo,"
                . "estudiante.documento AS documento,"
                . "estudiante.id AS id");
        //los join
        $this->db->join("sede", "sede.id = estudiante.sede_id");
        $this->db->join("institucion", "institucion.id = sede.institucion_id");
        $this->db->join("matricula", "matricula.estudiante_id = estudiante.id");
        $this->db->join("grupo", "matricula.curso_grupo_id = grupo.id");
        $this->db->join("grado", "matricula.curso_grado_id = grado.id");

        //retornamos
        return $this->db->get("estudiante")->result();
    }

    public function guardarEstudiante($estudiante) {
        if ($this->db->insert("estudiante", $estudiante)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function actualizaConsecutivo() {
        //ACTUALIZAMOS EL CONSECUTIVO DE LA BASE DE DATOS
        //where
        $where = array(
            "institucion_id" => $this->session->userdata("institucion_id")
        );
        $set = array(
            "consecutivo" => $this->session->userdata("consecutivo")
        );
        $this->db->where($where);
        $this->db->update("configuracion", $set);
        return TRUE;
    }

    public function guardarMatricula($matricula) {
        if ($this->db->insert("matricula", $matricula)) {
            //$this->actualizaConsecutivo();
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function listarCurso($grado, $grupo, $jornada) {
        //where
        $where = array(
            "matricula.curso_grado_id" => $grado,
            "matricula.curso_grupo_id" => $grupo,
            "matricula.curso_jornada_id" => $jornada,
            "matricula.eliminado" => "0",
            "sede.eliminado" => "0",
            "sede.activa" => "1",
            "estudiante.eliminado" => "0",
            "matricula.sede_id" => $this->session->userdata("sede_id"),
        );
        $this->db->where($where);
        //seleccionamos los datos
        /**/
        $this->db->select("estudiante.nombre AS estudiante_nombre,"
                . "estudiante.apellido AS estudiante_apellido,"
                . "estudiante.codigo AS estudiante_codigo,"
                . "estudiante.id AS estudiante_id,"
                . "grado.nombre AS grado_nombre,"
                . "grupo.nombre AS grupo_nombre,"
                . "jornada.nombre AS jornada_nombre");
        //joins
        $this->db->join("estudiante", "estudiante.id = matricula.estudiante_id");
        $this->db->join("grado", "grado.id = matricula.curso_grado_id");
        $this->db->join("grupo", "grupo.id = matricula.curso_grupo_id");
        $this->db->join("jornada", "jornada.id = matricula.curso_jornada_id");
        $this->db->join("sede", "sede.id = matricula.sede_id");
        //ordemanos alfaveticamente
        $this->db->order_by("estudiante.apellido");
        //retornamos el resultado
        return $this->db->get("matricula")->result();
    }

    public function listarTodoBoletin($curso, $codigo) {
        $cursos = explode("-", $curso);
        //where
        $where = array(
            "matricula.curso_grado_id" => $cursos[0],
            "matricula.curso_grupo_id" => $cursos[1],
            "matricula.curso_jornada_id" => $cursos[2],
            "matricula.eliminado" => "0",
            "matricula.sede_id" => $this->session->userdata("sede_id")
        );
        $where_or = array(
            "estudiante.codigo" => $codigo
        );
        $this->db->where($where);
        $this->db->or_where($where_or);
        $this->db->select("estudiante.nombre,"
                . "estudiante.apellido,"
                . "estudiante.codigo,"
                . "estudiante.id");
        $this->db->order_by("estudiante.apellido");
        $this->db->join("estudiante", "estudiante.id = matricula.estudiante_id");
        return $this->db->get("matricula")->result();
    }

    public function listarUnEstudiante($id) {
        $where = array(
            "estudiante.eliminado" => "0",
            "estudiante.id" => $id
        );
        $this->db->where($where);
        /* select */
        $this->db->select(
                "estudiante.nombre AS estudiante_nombre,"
                . "estudiante.id AS estudiante_id,"
                . "estudiante.apellido AS estudiante_apellido,"
                . "estudiante.codigo AS estudiante_codigo,"
                . "estudiante.documento AS estudiante_documento,"
                . "grado.nombre AS grado_nombre,"
                . "grupo.nombre AS grupo_nombre,"
                . "jornada.nombre AS jornada_nombre,"
                . "grado.id AS grado_id,"
                . "grupo.id AS grupo_id,"
                . "jornada.id AS jornada_id"
        );
        //join
        $this->db->join("estudiante", "estudiante.id = matricula.estudiante_id");
        $this->db->join("grado", "grado.id = matricula.curso_grado_id");
        $this->db->join("grupo", "grupo.id = matricula.curso_grupo_id");
        $this->db->join("jornada", "jornada.id = matricula.curso_jornada_id");
        return $this->db->get("matricula")->result();
    }

    public function eliminarMatricula($estudiante_id) {
        $where = array(
            "estudiante_id" => $estudiante_id
        );
        $set = array(
            "eliminado" => "1"
        );
        $this->db->where($where);
        if ($this->db->update("matricula", $set)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function modificarMatricula($where, $set) {
        $this->db->where($where);
        if ($this->db->update("matricula", $set)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function eliminar($id) {
        $where = array(
            "id" => $id
        );
        $set = array(
            "eliminado" => "1"
        );
        $this->db->where($where);
        $this->db->update("estudiante", $set);
        return TRUE;
    }

    public function eliminarDeVerdad($estudiante_id) {
        $where = array("estudiante_id" => $estudiante_id);
        $this->db->where($where);
        if (!$this->db->delete("juicio_cargado")) {
            return FALSE;
        }
        $this->db->where($where);
        if (!$this->db->delete("nota")) {
            return FALSE;
        }
        $this->db->where($where);
        if (!$this->db->delete("matricula")) {
            return FALSE;
        }
        $this->db->where($where);
        if (!$this->db->delete("factura")) {
            return FALSE;
        }
        $this->db->where(array("id" => $estudiante_id));
        if (!$this->db->delete("estudiante")) {
            return FALSE;
        }
        return TRUE;
    }

}
