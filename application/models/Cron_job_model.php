<?php

/*
 * Este modelo va a ser el encargado de hacer cualquier cosa que sea para los cronJobs
 * no nos meteremos con los modelos ya echo, ya que estan pensados para cuado haya session, 
 * estos no seran pensados asi.
 */

class Cron_job_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //haremos una funcion en el modelo para evitar hacerlo desde otro lado
    //crearemos un link espercial para todos los cronJobs
    //en este caso para la pregunta del periodo
    public function cambioDePeriodo($key) {
        //aqui va a estar el update del periodo
        //key = "cuando_uno_quiere_ser_seguro@hace_esto3015086264".sha1
        if ($key == "e398ce65df931cd9493f703a644a88ebd8894597") {
            /* cambiar periodo
             * obtener todas las instituciones que estan en la plataforma y sus id's
             * necesito saber las fechas de cada periodo de cada institucion.
             * si no tienen estos datos es porque no esta activo o esta mal configurado
             * comparo:
             * si la fecha actual esta entre la institucion->periodo->fechaInicial y institucion->periodo->fechaFinal
             * es porque estoy en ese periodo.
             * a utilzar:
             * - periodo_model->listarTodo()
             * - lista_de_instituciones->listarTodo()
             */
            return $this->listarTodoInstitucion();
            foreach ($this->listarTodoInstitucion() as $value) {
                
            }





            /*
              $periodos = $this->periodo();
              $datetime1 = date_create($periodo['fecha_inicio']);
              $datetime2 = date_create($periodo['fecha_final']);
              $interval = date_diff($datetime1, $datetime2); //total de dias
              $actual_fecha = date_create(date("Y-m-d"));
              $inicial = date_create($periodo['fecha_inicio']);
              $transcurrido = date_diff($inicial, $actual_fecha); //Transcurrido
              //porcentaje
              $porcentaje = ($transcurrido->format('%R%a días') * 100) / $interval->format('%R%a días');

              $interval->format('%R%a días');
              $transcurrido->format('%R%a días');
              if (($interval->format('%R%a días') - $transcurrido->format('%R%a días')) <= 15 && ($interval->format('%R%a días') - $transcurrido->format('%R%a días')) >= 0) {
              //Es porque esta a en el rango de 15 dias para terminar el periodo y solo los juicios seran habilidatos con todas sus funciones
              //y las notas para calificar
              } else {
              //Significa que esta por fuera del los ultimos quince dias del periodo, lo cual solo tendra los juicios habilidatos, y solo
              //para crearlos
              }
              } else {
              //$this->configuracion_model->logInterno("Alguien Quiso cambiar el periodo, o entro sin la llave");
              return FALSE;
             * 
             */
        }
    }

    /*
     * Estos son los metodos para los cronJobs y poder hacer los cambios
     */

    public function listarTodoInstitucion($key) {
        //select * from institucion
        if ($key == "e398ce65df931cd9493f703a644a88ebd8894597") {
            return $this->db->get("institucion")->result();
        } else {
            return FALSE;
        }
    }

    public function periodos($institucion, $key) {
        if ($key == "e398ce65df931cd9493f703a644a88ebd8894597") {
            //where
            $where = array(
                "institucion_id" => $institucion,
                "eliminado" => "0"
            );
            $this->db->where($where);
            return $this->db->get("periodo")->result();
        } else {
            return FALSE;
        }
    }

    public function configuracion($institucion, $key) {
        if ($key == "e398ce65df931cd9493f703a644a88ebd8894597") {
            //where
            $where = array(
                "institucion_id" => $institucion,
                "eliminado" => "0"
            );
            $this->db->where($where);
            return $this->db->get("configuracion")->result();
        }
    }

    public function updateConfiguracion($id, $periodo_id, $key) {
        if ($key == "e398ce65df931cd9493f703a644a88ebd8894597") {
            //lo que vamos a hacer es cambiar el periodo_actual_id
            $where = array(
                "id" => $id
            );
            $this->db->where($where);
            $set = array(
                "periodo_actual" => $periodo_id
            );
            if ($this->db->update("configuracion", $set)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

}
