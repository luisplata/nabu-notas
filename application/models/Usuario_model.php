<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Usuario_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function esta($usuario) {
        //el usuario que entra es un array con los dos datos: user y pass(con sha1)
        //se adicionan clausulas del where
        $usuario['institucion.activa'] = "1";
        $usuario['sede.activa'] = "1";
        //clausula where
        $this->db->where($usuario);
        //los join
        $this->db->join("administrador", "administrador.id = usuario.administrador_id");
        $this->db->join("institucion", "institucion.id = administrador.institucion_id");
        $this->db->join("sede", "sede.id = administrador.sede_id");
        //colocamos lo que necesitamos
        $this->db->select("administrador.id AS administrador_id,"
                . "institucion.id AS institucion_id,"
                . "institucion.nombre AS institucion_nombre,"
                . "sede.id AS sede_id,"
                . "sede.direccion AS institucion_direccion,"
                . "sede.telefono AS institucion_telefono,"
                . "administrador.nombre AS usuario_nombre,"
                . "administrador.apellido AS usuario_apellido,"
                . "administrador.email AS usuario_email,"
                . "usuario.grado,"
                . "usuario.valor,"
                . "usuario.id AS usuario_id");
        //generamos el resultado
        $usuarios = $this->db->get("usuario")->result();
        if ($usuarios) {
            //hacemos el foreach para cargar los datos cargados
            foreach ($usuarios as $value) {
                $this->session->set_userdata("administrador_id", $value->administrador_id);
                $this->session->set_userdata("institucion_id", $value->institucion_id);
                $this->session->set_userdata("institucion_nombre", $value->institucion_nombre);
                $this->session->set_userdata("institucion_direccion", $value->institucion_direccion);
                $this->session->set_userdata("institucion_telefono", $value->institucion_telefono);
                $this->session->set_userdata("sede_id", $value->sede_id);
                $this->session->set_userdata("usuario_nombre", $value->usuario_nombre);
                $this->session->set_userdata("usuario_apellido", $value->usuario_apellido);
                $this->session->set_userdata("usuario_email", $value->usuario_email);
                $this->session->set_userdata("grado", $value->grado);
                $this->session->set_userdata("valor", $value->valor);
                $this->session->set_userdata("usuario_id", $value->usuario_id);
            }
            //Se cargan los datos de configuracion
            $where = array(
                "configuracion.eliminado" => "0",
                "institucion_id" => $this->session->userdata("institucion_id")
            );
            $this->db->where($where);
            //datos a seleccionar
            /**/
            $this->db->select("configuracion.escudo,"
                    . "configuracion.eliminar,"
                    . "configuracion.interes,"
                    . "configuracion.periodo_actual,"
                    . "configuracion.fecha_mensualidad,"
                    . "configuracion.factura_numero,"
                    . "configuracion.numero_notas,"
                    . "configuracion.consecutivo,"
                    . "configuracion.imprimir");
            //joins
            $this->db->join("institucion", "institucion.id = configuracion.institucion_id");
            //cargamos los datos
            $configuracion = $this->db->get("configuracion")->result();
            if ($configuracion) {
                foreach ($configuracion as $value) {
                    $this->session->set_userdata("escudo", $value->escudo);
                    $this->session->set_userdata("eliminar", $value->eliminar);
                    $this->session->set_userdata("interes", $value->interes);
                    $this->session->set_userdata("periodo_actual", $value->periodo_actual);
                    $this->session->set_userdata("fecha_mensualidad", $value->fecha_mensualidad);
                    $this->session->set_userdata("factura_numero", $value->factura_numero);
                    $this->session->set_userdata("numero_notas", $value->numero_notas);
                    $this->session->set_userdata("consecutivo", $value->consecutivo);
                    $this->session->set_userdata("imprimir", $value->imprimir);
                }
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function estaDocente($docente) {
        $docente['docente.eliminado'] = "0";
        $docente['sede.activa'] = "1";
        //sentencia where
        $this->db->where($docente);
        //selecion de datos
        /**/
        $this->db->select("docente.nombre AS nombre,"
                . "docente.apellido AS apellido,"
                . "docente.id AS id,"
                . "docente.email AS email,"
                . "sede.institucion_id AS institucion_id,"
                . "sede.id AS sede_id");
        //join
        $this->db->join("sede", "sede.id = docente.sede_id");
        $docente = $this->db->get("docente")->result();
        if ($docente) {
            foreach ($docente as $value) {
                //cargamos los datos a la session
                $this->session->set_userdata("docente_id", $value->id);
                $this->session->set_userdata("sede_id", $value->sede_id);
                $this->session->set_userdata("nombre", $value->nombre);
                $this->session->set_userdata("apellido", $value->apellido);
                $this->session->set_userdata("email", $value->email);
                $this->session->set_userdata("institucion_id", $value->institucion_id);
            }
            //cargando los datos de configuracion
            $where = array(
                "configuracion.eliminado" => "0",
                "institucion_id" => $this->session->userdata("institucion_id")
            );
            $this->db->where($where);
            //datos a seleccionar
            /**/
            $this->db->select("configuracion.escudo,"
                    . "configuracion.eliminar,"
                    . "configuracion.interes,"
                    . "configuracion.periodo_actual,"
                    . "configuracion.fecha_mensualidad,"
                    . "configuracion.factura_numero,"
                    . "configuracion.numero_notas,"
                    . "configuracion.calificacion_decimal,"
                    . "configuracion.calificacion_baja,"
                    . "configuracion.calificacion_basico,"
                    . "configuracion.calificacion_alta,"
                    . "configuracion.calificacion_superior,"
                    . "configuracion.calificacion_maxima,"
                    . "configuracion.porcentaje_bajo,"
                    . "configuracion.porcentaje_medio,"
                    . "configuracion.porcentaje_superior,"
                    . "configuracion.imprimir");
            //joins
            $this->db->join("institucion", "institucion.id = configuracion.institucion_id");
            //cargamos los datos
            $configuracion = $this->db->get("configuracion")->result();
            if ($configuracion) {
                foreach ($configuracion as $value) {
                    $this->session->set_userdata("escudo", $value->escudo);
                    $this->session->set_userdata("eliminar", $value->eliminar);
                    $this->session->set_userdata("interes", $value->interes);
                    $this->session->set_userdata("periodo_actual", $value->periodo_actual);
                    $this->session->set_userdata("fecha_mensualidad", $value->fecha_mensualidad);
                    $this->session->set_userdata("factura_numero", $value->factura_numero);
                    $this->session->set_userdata("numero_notas", $value->numero_notas);
                    $this->session->set_userdata("imprimir", $value->imprimir);
                    $this->session->set_userdata("calificacion_decimal", $value->calificacion_decimal);
                    $this->session->set_userdata("calificacion_baja", $value->calificacion_baja);
                    $this->session->set_userdata("calificacion_basico", $value->calificacion_basico);
                    $this->session->set_userdata("calificacion_alta", $value->calificacion_alta);
                    $this->session->set_userdata("calificacion_superior", $value->calificacion_superior);
                    $this->session->set_userdata("calificacion_maxima", $value->calificacion_maxima);
                    $this->session->set_userdata("porcentaje_bajo", $value->porcentaje_bajo);
                    $this->session->set_userdata("porcentaje_medio", $value->porcentaje_medio);
                    $this->session->set_userdata("porcentaje_superior", $value->porcentaje_superior);
                }
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /* desde aqui se comenzara a realizar la gestion de usuarios del sistema
     * se tiene que hacer una funcion que se ejecute una vez para pasar los datos de los docentes a la de administradores
     * y posteriormente a la de usuarios para que puedan cambiar la contraseña y demas
     * Cada funcion que se escribira tendra que modificar dos tablas
     * la de administradores
     * y la de usuarios
     */

    public function guardar($usuario) {
        if ($this->db->insert("usuario", $usuario)) {
            //significa que inserto efectivamente el administrador
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
