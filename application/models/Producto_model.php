<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Producto_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function listarTodo() {
        $where = array(
            "producto.eliminado" => "0",
            "producto.institucion_id" => $this->session->userdata("institucion_id")
        );
        $this->db->where($where);
        /* Seleccionando datos */
        $this->db->select("producto.nombre,"
                . "producto.precio,"
                . "producto.descripcion,"
                . "producto.id");
        //joins
        $this->db->join("institucion", "institucion.id = producto.institucion_id");
        //devolviendo datos
        return $this->db->get("producto")->result();
    }

    public function guardar($producto) {
        if ($this->db->insert("producto", $producto)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
