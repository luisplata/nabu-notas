<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Nota_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function buscarNotaEstudiante($where) {
        $this->db->where($where);
        $dateRange = "fecha BETWEEN '" . date("Y") . "-1-1' AND '" . date("Y") . "-12-30'";
        $this->db->where($dateRange);
        //$this->db->select("");
        //sequito de get("nota")->result()
        return $this->db->get("nota");
    }

    public function buscarNotaEstudianteAsignatura($estudiante, $asignatura, $periodo) {
        $where = array(
            "estudiante_id" => $estudiante,
            "asignatura_id" => $asignatura,
            "periodo" => $periodo
        );
        $this->db->where($where);
//        $dateRange = "tdate BETWEEN '" . date("Y") . "-1-1' AND '" . date("Y") . "-12-30'";
//        $this->db->where($dateRange);
        //$this->db->select("");
        return $this->db->get("nota")->result();
    }

    public function asignaturasCalificadas($estudiante, $periodo) {
        /* Aqui se buscara la cantidad de las asignaturas calificadas
         * la cual se sacara contando el array en el controlador
         * aqui mandara las id de las asignaturas para buscar esas notas
         * especificas y sacar el promedio y aplicarles el porcentaje si lo hay
         */
        $this->db->where(array("estudiante_id" => $estudiante, "periodo" => $periodo));
        $this->db->group_by("asignatura_id");
        //$this->db->select("");
        $asignaturas = $this->db->get("nota")->result();

        foreach ($asignaturas as $value) {
            $asignaturasCalificadas = array(
                "id" => $value
            );
        }
        return $asignaturas;
    }

    public function guardar($nota) {
        if ($this->db->insert("nota", $nota)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function actualizar($where, $set) {
        $this->db->where($where);
        return $this->db->update("nota", $set);
    }

    public function notasEstudiante($id) {
        $where = array(
            "nota.periodo" => $this->session->userdata("periodo_actual"),
            "nota.estudiante_id" => $id
        );
        $this->db->where($where);
        //select
        /**/
        //joins
        $this->db->join("estudiante", "estudiante.id = nota.estudiante_id");
        $this->db->join("docente", "docente.id = nota.docente_id");
        $this->db->join("asignatura", "asignatura.id = nota.asignatura_id");
        $this->db->join("area", "area.id = asignatura.area_id");
        //retornando
        return $this->db->get("nota")->result();
    }

    public function asignaturasDeEstudiante($id) {
        $where = array(
            "nota.periodo" => $this->session->userdata("periodo_actual"),
            "nota.estudiante_id" => $id
        );
        $this->db->where($where);
        //select
        /**/
        $this->db->select("asignatura.nombre AS asignatura_nombre,"
                . "asignatura.id AS asignatura_id");
        //joins
        $this->db->join("estudiante", "estudiante.id = nota.estudiante_id");
        $this->db->join("docente", "docente.id = nota.docente_id");
        $this->db->join("asignatura", "asignatura.id = nota.asignatura_id");
        $this->db->join("area", "area.id = asignatura.area_id");
        //retornando
        $this->db->group_by("asignatura.id");
        return $this->db->get("nota")->result();
    }

    public function numeroDePeriodos($id) {
        //esto solo devuelve un numero        
        $query = $this->db->query("select * from nota "
                . "inner join asignatura ON asignatura.id = nota.asignatura_id "
                . "inner join sede ON sede.id = asignatura.sede_id "
                . "inner join institucion ON institucion.id = sede.institucion_id "
                . "where institucion_id = " . $this->session->userdata("institucion_id") . " and estudiante_id = " . $id . " "
                . "group by nota.periodo");

        return $query->num_rows();
    }

}
