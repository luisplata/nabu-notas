<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Curso_model extends CI_Model {

    function __construct() {
        parent::__construct();
//        $this->output->enable_profiler(TRUE);
    }

    public function listarTodo() {
        //listar todos los cursos disponibles
        //where
        $where = array(
            "curso.eliminado" => "0",
            "grado.eliminado" => "0",
            "grupo.eliminado" => "0",
            "jornada.eliminado" => "0",
            "curso.sede_id" => $this->session->userdata("sede_id")
        );
        $this->db->where($where);
        //seleciono los datos
        /**/
        $this->db->select("curso.grado_id As grado_id,"
                . "curso.grupo_id AS grupo_id,"
                . "curso.jornada_id AS jornada_id,"
                . "curso.sede_id As sede_id,"
                . "curso.promocion As promocion,"
                . "grupo.nombre As grupo,"
                . "grado.nombre AS grado,"
                . "sede.nombre As sede,"
                . "grupo.codigo As grupo_codigo,"
                . "grado.codigo AS grado_codigo,"
                . "jornada.nombre AS jornada");
        //los join
        $this->db->join("grado", "grado.id = curso.grado_id");
        $this->db->join("grupo", "grupo.id = curso.grupo_id");
        $this->db->join("jornada", "jornada.id = curso.jornada_id");
        $this->db->join("sede", "sede.id = curso.sede_id");
        $this->db->join("institucion", "institucion.id = sede.institucion_id");
        //ordenando
        $this->db->order_by("grado.id,grupo.id,jornada.id");
        //retornamos
        return $this->db->get("curso")->result();
    }

    public function listarCurso($grado) {
        //listar todos los cursos disponibles
        //where
        $where = array(
            "curso.eliminado" => "0",
            "grado.eliminado" => "0",
            "grupo.eliminado" => "0",
            "jornada.eliminado" => "0",
            "grado.codigo" => $grado[0],
            "grupo.id" => $grado[1],
            "jornada.id" => $grado[2],
            "curso.sede_id" => $this->session->userdata("sede_id"),
        );
        $this->db->where($where);
        //seleciono los datos
        /**/
        $this->db->select("curso.grado_id As grado_id,"
                . "curso.grupo_id AS grupo_id,"
                . "curso.jornada_id AS jornada_id,"
                . "curso.sede_id As sede_id,"
                . "grupo.nombre As grupo,"
                . "grado.nombre AS grado,"
                . "sede.nombre As sede,"
                . "grupo.codigo As grupo_codigo,"
                . "grado.codigo AS grado_codigo,"
                . "jornada.nombre AS jornada");
        //los join
        $this->db->join("grado", "grado.id = curso.grado_id");
        $this->db->join("grupo", "grupo.id = curso.grupo_id");
        $this->db->join("jornada", "jornada.id = curso.jornada_id");
        $this->db->join("sede", "sede.id = curso.sede_id");
        $this->db->join("institucion", "institucion.id = sede.institucion_id");
        //retornamos
        return $this->db->get("curso")->result();
    }

    public function listarUnCurso($grado, $grupo, $jornada) {
        //listar todos los cursos disponibles
        //where
        $where = array(
            "curso.eliminado" => "0",
            "grado.eliminado" => "0",
            "grupo.eliminado" => "0",
            "jornada.eliminado" => "0",
            "grado.id" => $grado,
            "grupo.id" => $grupo,
            "jornada.id" => $jornada,
            "curso.sede_id" => $this->session->userdata("sede_id"),
        );
        $this->db->where($where);
        //seleciono los datos
        /**/
        $this->db->select("curso.grado_id As grado_id,"
                . "curso.grupo_id AS grupo_id,"
                . "curso.jornada_id AS jornada_id,"
                . "curso.sede_id As sede_id,"
                . "curso.promocion As promocion,"
                . "grupo.nombre As grupo,"
                . "grado.nombre AS grado,"
                . "sede.nombre As sede,"
                . "grupo.codigo As grupo_codigo,"
                . "grado.codigo AS grado_codigo,"
                . "jornada.nombre AS jornada");
        //los join
        $this->db->join("grado", "grado.id = curso.grado_id");
        $this->db->join("grupo", "grupo.id = curso.grupo_id");
        $this->db->join("jornada", "jornada.id = curso.jornada_id");
        $this->db->join("sede", "sede.id = curso.sede_id");
        $this->db->join("institucion", "institucion.id = sede.institucion_id");
        //retornamos
        return $this->db->get("curso")->result();
    }

    public function cursosPromocion($grado) {
        $where = array(
            "grado.codigo" => $grado + 1,
            "grado.eliminado" => 0
        );
        $this->db->select("grado.id,"
                . "curso.grupo_id,"
                . "grado.codigo,"
                . "grado.nombre");
        $this->db->where($where);
        $this->db->join("grado", "grado.id = curso.grado_id");
        return $this->db->get("curso")->result();
    }

    public function guardarCurso($curso) {
        //insertar curso
        if ($this->db->insert("curso", $curso)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function listarGrados() {
        //listar todos los grados
        //where
        $where = array(
            "grado.eliminado" => "0",
            "sede_id" => $this->session->userdata("sede_id")
        );
        $this->db->where($where);
        //selecionar campos
        /**/
        $this->db->select("grado.nombre AS nombre,"
                . "grado.codigo AS codigo,"
                . "grado.id AS id,");
        //join
        $this->db->join("sede", "sede.id = grado.sede_id");

        //retornar
        return $this->db->get("grado")->result();
    }

    public function listarGrado($id) {
        //listar todos los grados
        //where
        $where = array(
            "grado.eliminado" => "0",
            "grado.id" => $id,
            "sede_id" => $this->session->userdata("sede_id")
        );
        $this->db->where($where);
        //selecionar campos
        /**/
        $this->db->select("grado.nombre AS nombre,"
                . "grado.codigo AS codigo,"
                . "grado.id AS id"
                );
        //join
        $this->db->join("sede", "sede.id = grado.sede_id");

        //retornar
        return $this->db->get("grado")->result();
    }

    public function guardarGrado($grado) {
        //insertar grado nuevo
        if ($this->db->insert("grado", $grado)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function listarGrupos() {
        //listar los grupos disponibles
        //
        //where
        $where = array(
            "grupo.eliminado" => "0",
            "sede_id" => $this->session->userdata("sede_id")
        );
        $this->db->where($where);
        //selecionar campos
        /**/
        $this->db->select("grupo.nombre AS nombre,"
                . "grupo.codigo AS codigo,"
                . "grupo.id AS id");
        //join
        $this->db->join("sede", "sede.id = grupo.sede_id");
        //retornar
        return $this->db->get("grupo")->result();
    }

    public function guardarGrupo($grupo) {
        //insertar grupo nuevo
        if ($this->db->insert("grupo", $grupo)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function listarJornadas() {
        //listar los grupos disponibles
        //where
        $where = array(
            "jornada.eliminado" => "0",
            "sede_id" => $this->session->userdata("sede_id")
        );
        $this->db->where($where);
        //selecionar campos
        /**/
        $this->db->select("jornada.nombre,jornada.id, jornada.horario,jornada.sede_id");
        //join
        $this->db->join("sede", "sede.id = jornada.sede_id");
        //retornar
        return $this->db->get("jornada")->result();
    }

    public function guardarJornada($jornada) {
        //insertar grupo nuevo
        if ($this->db->insert("jornada", $jornada)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function cursoDocente() {
        //where
        $where = array(
            "carga_academica.docente_id" => $this->session->userdata("docente_id"),
            "carga_academica.sede_id" => $this->session->userdata("sede_id")
        );
        $this->db->where($where);
        //seleccion de datos
        /**/
        $this->db->select("grado.nombre AS grado,"
                . "grupo.nombre AS grupo,"
                . "jornada.nombre AS jornada,"
                . "grupo.id AS grupo_id,"
                . "grado.id AS grado_id,"
                . "jornada.id AS jornada_id,"
                . "docente.id AS docente_id,"
                . "docente.nombre AS docente_nombre");
        //groupby
        $groupBy = array(
            "carga_academica.curso_grado_id",
            "carga_academica.curso_grupo_id",
            "carga_academica.curso_jornada_id"
        );
        $this->db->group_by($groupBy);
        //join
        $this->db->join("sede", "sede.id = carga_academica.sede_id");
        $this->db->join("grado", "grado.id = carga_academica.curso_grado_id");
        $this->db->join("grupo", "grupo.id = carga_academica.curso_grupo_id");
        $this->db->join("jornada", "jornada.id = carga_academica.curso_jornada_id");
        $this->db->join("docente", "docente.id = carga_academica.docente_id");
        //retornamos
        return $this->db->get("carga_academica")->result();
    }

    public function asignaturaDocente($curso) {
        //curso array con formato 0grado-1frupo-2jornada
        //where
        $where = array(
            "carga_academica.docente_id" => $this->session->userdata("docente_id"),
            "carga_academica.sede_id" => $this->session->userdata("sede_id"),
            "carga_academica.curso_grado_id" => $curso[0],
            "carga_academica.curso_grupo_id" => $curso[1],
            "carga_academica.curso_jornada_id" => $curso[2],
        );
        $this->db->where($where);
        //seleccion de datos
        /**/
        $this->db->select("grado.nombre AS grado,"
                . "grupo.nombre AS grupo,"
                . "jornada.nombre AS jornada,"
                . "grupo.id AS grupo_id,"
                . "grado.id AS grado_id,"
                . "jornada.id AS jornada_id,"
                . "docente.id AS docente_id,"
                . "docente.nombre AS docente_nombre,"
                . "asignatura.id AS asignatura_id,"
                . "asignatura.nombre AS asignatura");
        //groupby
        $groupBy = array(
            "asignatura.id"
        );
        $this->db->group_by($groupBy);
        //join
        $this->db->join("sede", "sede.id = carga_academica.sede_id");
        $this->db->join("grado", "grado.id = carga_academica.curso_grado_id");
        $this->db->join("grupo", "grupo.id = carga_academica.curso_grupo_id");
        $this->db->join("jornada", "jornada.id = carga_academica.curso_jornada_id");
        $this->db->join("docente", "docente.id = carga_academica.docente_id");
        $this->db->join("asignatura", "asignatura.id = carga_academica.asignatura_id");
        //retornamos
        return $this->db->get("carga_academica")->result();
    }

    public function actualizarCurso($where, $set) {
        $this->db->where($where);
        if ($this->db->update("curso", $set)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	
	
	//codigo de ospino
	
	
    public function actualizarGrupo($where, $datos){
        
          $this->db->where($where);
        if ($this->db->update("grupo", $datos)) {
            echo "se redirecciono";
            redirect("cursos/grupos");
        } else {
            echo "error al actualizar el grupo";
        }
        
    }
	
	    public function actualizarJornada($where,$datos){
             $this->db->where($where);
        if ($this->db->update("jornada", $datos)) {
            echo "Todo Va Bien";
            redirect("cursos/jornadas");
        } else {
            echo "error al actualizar la jornada";
        }
    }
	
	
    public function listarGrupo() {
       
        //creamos el where
        $where = array(
            "grupo.eliminado" => "0",
           
            "sede_id" => $this->session->userdata("sede_id")
        );
        //añado el where a la consulta
        $this->db->where($where);
        //creo y añado el select
        $this->db->select("grupo.nombre AS nombre,"
                . "grupo.id AS id,"
                . "grupo.codigo AS codigo"
        );
        //Retorno de la consulta
        return $this->db->get("grupo")->result();
    }
	    public function actualizarGrado($where,$datos){
             $this->db->where($where);
        if ($this->db->update("grado", $datos)) {
            redirect("cursos/grados");
        } else {
            echo "mal";
        }
    }
	
	    //funcion que elimina un grado, eliminado =1
    public function eliminarGrado($where, $set) {

        $this->db->where($where);
        if ($this->db->update("grado", $set)) {
            redirect("cursos/grados");
        } else {
            echo "se ha producido un error... verificar en curso_modelo linea 238";
        }
    }
	
	   public function eliminarGrupo($where, $set) {

        $this->db->where($where);
        if ($this->db->update("grupo", $set)) {
            echo "se elimino el grupo";
            redirect("cursos/grupos");
        } else {
            echo "se ha producido un error..";
        }
    }
	
	public function eliminarJornada($where, $set) {

        $this->db->where($where);
        if ($this->db->update("jornada", $set)) {
            echo "se elimino el grupo";
            redirect("cursos/jornadas");
        } else {
            echo "se ha producido un error..";
        }
    }
	
	
	
	  public function editarJornada($id) {
        //listar los grupos disponibles
        //where
        $where = array(
            "jornada.id"=>$id,
            "jornada.eliminado" => "0"
     
        );
        $this->db->where($where);
        //selecionar campos
        /**/
        $this->db->select("jornada.nombre,jornada.id, jornada.horario,jornada.sede_id");
        //join
        $this->db->join("sede", "sede.id = jornada.sede_id");
        //retornar
        return $this->db->get("jornada")->result();
    }

}
