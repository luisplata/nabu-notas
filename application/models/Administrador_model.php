<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Administrador_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function listarTodo() {
        //selecionando a todos los administradores
        $sql = "select 
                u.id,
                a.nombre,
                a.apellido,
                u.eliminado,
                u.activo,
                a.documento,
                a.email,
                a.telefono
            from usuario u 
            
            inner join administrador a ON a.id = u.administrador_id
            
            where 
                a.institucion_id = ?
                and a.sede_id = ?
                and u.eliminado = ?";

        $filtro = array(
            $this->session->userdata("institucion_id"),
            $this->session->userdata("sede_id"),
            0
        );
        return $this->db->query($sql, $filtro)->result();
    }

    public function guardar($administrador) {
        if ($this->db->insert("administrador", $administrador)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function Eliminar($id) {
        $sql = "update usuario set eliminado = ? where id = ?";
        $filtro = array(
            1,
            $id
        );
        if ($this->db->query($sql, $filtro)) {
            //elimino con exito?
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function GetUser($id) {
        //buscamos al usuario con la id y devolvemos todos los datos
        $sql = "select 
                a.id,
                a.nombre,
                a.apellido,
                a.documento,
                a.email,
                a.telefono
            from usuario u
            inner join administrador a ON a.id = u.administrador_id
            where u.id = ?";
        $filtro = array(
            $id
        );
        return $this->db->query($sql, $filtro)->result();
    }

    public function Modificar($usuario, $id) {
        $this->db->set($usuario);
        $this->db->where("id", $id);
        return $this->db->update("administrador");
    }

}
