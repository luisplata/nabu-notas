<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Sede_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function ListarTodo() {
        //where
        $where = array(
            "institucion.activa" => "1",
            "institucion.eliminado" => "0",
            //"sede.activa" => "1",
            "sede.eliminado" => "0",
            "sede.institucion_id" => $this->session->userdata("institucion_id")
        );
        $this->db->where($where);
        //hacemos join
        $this->db->join("institucion", "institucion.id = sede.institucion_id");
        //seleccionamos los datos a devolver
        $this->db->select("sede.id AS sede_id,"
                . "sede.nombre AS sede_nombre,"
                . "sede.email AS sede_email,"
                . "sede.direccion As sede_direccion,"
                . "sede.telefono As sede_telefono,"
                . "sede.activa AS sede_activa,"
                . "institucion.id AS institucion_id,"
                . "institucion.nombre AS institucion_nombre");
        //retornamos datos
        return $this->db->get("sede")->result();
    }

    public function Guardar($sede) {
        if ($this->db->insert("sede", $sede)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function Activar($estado, $id) {
        $this->db->set(array(
            "activa" => $estado
        ));
        $this->db->where("id", $id);
        return $this->db->update("sede");
    }

    public function Eliminar($id) {
        $this->db->set(array(
            "eliminado" => 1
        ));
        $this->db->where("id", $id);
        return $this->db->update("sede");
    }

}
