<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Categoria_egreso_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function listarTodo() {
        //where
        $where = array(
            "institucion_id" => $this->session->userdata("institucion_id"),
            "eliminado" => 0
        );
        $this->db->where($where);
        //select
        /**/
        //joins
        /**/
        //retornando
        return $this->db->get("categoria_egreso")->result();
    }

    public function listarUno($id) {
        //where
        $where = array(
            "institucion_id" => $this->session->userdata("institucion_id"),
            "eliminado" => 0,
            "id" => $id
        );
        $this->db->where($where);
        //select
        /**/
        //joins
        /**/
        //retornando
        return $this->db->get("categoria_egreso")->result();
    }

    public function guardar($categoriaEgreso) {
        if ($this->db->insert("categoria_egreso", $categoriaEgreso)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function eliminar($id) {
        $where = array(
            "id" => $id
        );
        $this->db->where($where);
        $set = array("eliminado" => 1);
        if ($this->db->update("categoriaEgreso", $set)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function modificar($categoriaEgreso) {
        $where = array(
            "id" => $categoriaEgreso['id']
        );
        $this->db->where($where);
        if ($this->db->update("categoriaEgreso", $categoriaEgreso)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
