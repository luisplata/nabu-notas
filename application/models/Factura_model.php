<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Factura_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function listarTodo() {
        $where = array(
            "factura.institucion_id" => $this->session->userdata("institucion_id"),
            "factura.sede_id" => $this->session->userdata("sede_id")
        );
        $this->db->where($where);
        /* seleccionados */
        $this->db->select("factura.id,"
                . "factura.fecha,"
                . "factura.eliminado,"
                . "factura.interes,"
                . "producto.id AS producto_id,"
                . "producto.precio AS producto_precio,"
                . "producto.eliminado AS producto_eliminado,"
                . "producto.nombre AS producto_nombre,"
                . "institucion.id AS institucion_id,"
                . "institucion.nombre AS institucion_nombre");
        //joins
        $this->db->join("institucion", "institucion.id = factura.institucion_id");
        $this->db->join("sede", "sede.id = factura.sede_id");
        $this->db->join("producto", "producto.id = factura.producto_id");
        //retornando datos
        return $this->db->get("factura")->result();
    }

    public function listarTodoNumero() {
        $where = array(
            "factura.institucion_id" => $this->session->userdata("institucion_id"),
            "factura.sede_id" => $this->session->userdata("sede_id")
        );
        $this->db->where($where);
        /* seleccionados */
        $this->db->select("factura.numero,"
                . "factura.fecha,"
                . "factura.id");
        //joins
        $this->db->join("institucion", "institucion.id = factura.institucion_id");
        $this->db->join("sede", "sede.id = factura.sede_id");
        $this->db->join("producto", "producto.id = factura.producto_id");
        //agrupando
        $this->db->group_by("factura.numero");
        //ordenando los datos
        $this->db->order_by("factura.numero", "desc");
        //retornando datos
        return $this->db->get("factura")->result();
    }

    public function ultimoNumero() {
        //sacando la ultima factura de la base de datos
        $where = array(
            "institucion_id" => $this->session->userdata("institucion_id")
        );
        $this->db->where($where);
        $confi = $this->db->get("configuracion")->result();
        foreach ($confi as $value) {
            return $ultimoNumero = $value->factura_numero;
        }
    }

    public function guardar($facturas, $estudiante_id, $banco = 0) {
        foreach ($facturas as $productoIndividual) {
            //se crea la consulta para que 
            $insert = array(
                "producto_id" => $productoIndividual['id'],
                "usuario_id" => $this->session->userdata("usuario_id"),
                "fecha" => date("Y-m-d"),
                "numero" => $this->ultimoNumero() + 1,
                "institucion_id" => $this->session->userdata("institucion_id"),
                "sede_id" => $this->session->userdata("sede_id"),
                "interes" => $this->session->userdata("interes"),
                "estudiante_id" => $estudiante_id,
                "banco" => $banco
            );
            if ($this->db->insert("factura", $insert)) {
                return TRUE;
            } else {
                //noguarda los registros
                //retornamos false?
                return FALSE;
            }
        }
        //actualizando el dato de configuracion
        $data = array(
            'factura_numero' => $this->ultimoNumero() + 1
        );
        $this->session->set_userdata("factura_numero", $this->ultimoNumero());
        $this->db->where('institucion_id', $this->session->userdata("institucion_id"));
        $this->db->update('configuracion', $data);
        return TRUE;
    }

    public function listarTodoEgresos() {
        //where
        $where = array(
            "egreso.eliminado" => 0,
            "egreso.institucion_id" => $this->session->userdata("institucion_id")
        );
        $this->db->where($where);
        $this->db->join("usuario", "usuario.id = egreso.usuario_id");
        $this->db->join("administrador", "administrador.id = usuario.administrador_id");
        $this->db->select("egreso.descripcion,"
                . "egreso.cantidad,"
                . "egreso.id,"
                . "egreso.fecha,"
                . "usuario.id AS usuario_id,"
                . "administrador.id AS administrador_id,"
                . "administrador.nombre AS administrador_nombre,"
                . "administrador.apellido AS administrador_apellido");
        return $this->db->get("egreso")->result();
    }

    public function nuevoEgreso($egreso) {
        if ($this->db->insert("egreso", $egreso)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
