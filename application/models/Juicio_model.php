<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Juicio_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function listarTodoDocente() {
        //where
        $where = array(
            "juicio.docente_id" => $this->session->userdata("docente_id"),
            "juicio.eliminado" => "0"
        );
        $this->db->where($where);
        //select
        /**/
        $this->db->select("juicio.docente_id AS docente_id,"
                . "juicio.descripcion,"
                . "juicio.id,"
                . "asignatura.nombre AS asignatura_nombre,"
                . "asignatura.id AS asignatura_id");
        //join
        $this->db->join("asignatura", "asignatura.id = juicio.asignatura_id");
        return $this->db->get("juicio")->result();
    }

    public function listarPorEstudiante($id) {
        $where = array(
            "juicio_cargado.estudiante_id" => $id,
            "juicio_cargado.eliminado" => "0",
            "juicio_cargado.periodo" => $this->session->userdata("periodo_actual")
        );
        $this->db->where($where);
        //select
        /**/
        $this->db->select("juicio.descripcion,juicio_cargado.nota,"
                . "juicio.asignatura_id");
        //join
        $this->db->join("estudiante", "estudiante.id = juicio_cargado.estudiante_id");
        $this->db->join("juicio", "juicio.id = juicio_cargado.juicio_id");
        return $this->db->get("juicio_cargado")->result();
    }

    public function guardar($juicio) {
        if ($this->db->insert("juicio", $juicio)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function guardarCargarJuicio($juicio_cargado) {
        if ($this->db->insert("juicio_cargado", $juicio_cargado)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function eliminar($id) {
        $sed = array(
            "eliminado" => "1"
        );
        $where = array("id" => $id);
        $this->db->where($where);
        if ($this->db->update("juicio", $sed)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function listarUnJuicio($id) {
        $where = array(
            "id" => $id
        );
        $this->db->where($where);
        return $this->db->get("juicio")->result();
    }

    public function modificar($juicio) {
        $where = array("id" => $juicio['id']);
        $this->db->where($where);
        if ($this->db->update("juicio", $juicio)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function califarJuicio($juicio, $estudiante, $nota) {
        $where = array(
            "juicio_id" => $juicio,
            "estudiante_id" => $estudiante
        );
        $this->db->where($where);
        $set = array(
            "nota" => $nota
        );
        if ($this->db->update("juicio_cargado", $set)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function calificarJuicio($array) {
        //juicio_id,estudiante_id,periodo,nota
        $where = array(
            "juicio_id" => $array['juicio_id'],
            "estudiante_id" => $array['estudiante_id'],
            "periodo" => $array['periodo']
        );
        $this->db->where($where);
        $set = array(
            "nota" => $array['nota']
        );
        if ($this->db->update("juicio_cargado", $set)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function verJuiciosInstitucion($docente_id) {
        /*
         * Aqui se consultara los juicios.
         * juicios de los docentes que pertenescan a la sede de la institucion en session
         */
        //where
        $where = array(
            "juicio.eliminado" => "0",
            "docente.eliminado" => "0",
            "sede.institucion_id" => $this->session->userdata("institucion_id"),
            "juicio.docente_id" => $docente_id
        );
        $this->db->where($where);
        $this->db->select("juicio.descripcion,asignatura.nombre,docente.apellido");
        $this->db->join("docente", "docente.id = juicio.docente_id");
        $this->db->join("asignatura", "asignatura.id = juicio.asignatura_id");
        $this->db->join("sede", "sede.id = docente.sede_id");
        return $this->db->get("juicio")->result();
    }

}
