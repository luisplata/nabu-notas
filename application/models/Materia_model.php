<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Materia_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function listarTodoAreas() {
        //where de la sentencia
        $where = array(
            "area.eliminado" => "0",
            "area.sede_id" => $this->session->userdata("sede_id")
        );
        $this->db->where($where);
        //los datos seleccionados
        /**/
        $this->db->select("area.nombre AS nombre,"
                . "area.codigo AS codigo,"
                . "area.id AS id");
        //los join
        $this->db->join("sede", "sede.id = area.sede_id");
        //lo retornado
        return $this->db->get("area")->result();
    }

    public function guardarArea($area) {
        if ($this->db->insert("area", $area)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function listarTodoAsignaturas() {
        //where de la sentencia
        $where = array(
            "asignatura.eliminado" => "0",
            "asignatura.sede_id" => $this->session->userdata("sede_id")
        );
        $this->db->where($where);
        //los datos seleccionados
        /**/
        $this->db->select("asignatura.id AS id,"
                . "asignatura.nombre AS nombre,"
                . "asignatura.codigo AS codigo,"
                . "area.nombre AS area_nombre");
        //los join
        $this->db->join("sede", "sede.id = asignatura.sede_id");
        $this->db->join("area", "area.id = asignatura.area_id");
        //lo retornado
        return $this->db->get("asignatura")->result();
    }

    public function guardarAsignatura($asignatura) {
        if ($this->db->insert("asignatura", $asignatura)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function listarTodoCargaAcademica() {
        //where de la sentencia
        $where = array(
            "carga_academica.eliminado" => "0",
            "carga_academica.sede_id" => $this->session->userdata("sede_id")
        );
        $this->db->where($where);
        //los datos seleccionados
        /**/
        $this->db->select("asignatura.nombre AS asignatura_nombre,"
                . "docente.nombre AS docente_nombre,"
                . "docente.apellido AS docente_apellido,"
                . "grado.nombre As grado,"
                . "grupo.nombre AS grupo,"
                . "jornada.nombre AS jornada,"
                . "sede.nombre As sede_nombre,"
                . "asignatura.id AS asignatura_id,"
                . "docente.id AS docente_id,"
                . "grado.id AS grado_id,"
                . "grupo.id AS grupo_id,"
                . "jornada.id AS jornada_id");
        //los join
        $this->db->join("sede", "sede.id = carga_academica.sede_id");
        $this->db->join("docente", "docente.id = carga_academica.docente_id");
        $this->db->join("asignatura", "asignatura.id = carga_academica.asignatura_id");
        $this->db->join("grado", "grado.id = carga_academica.curso_grado_id");
        $this->db->join("grupo", "grupo.id = carga_academica.curso_grupo_id");
        $this->db->join("jornada", "jornada.id = carga_academica.curso_jornada_id");
        //
        $this->db->order_by("docente.id, asignatura_id, grado.id, grupo.id, jornada.id");
        //lo retornado
        return $this->db->get("carga_academica")->result();
    }

    public function guardarCargaAcademica($cargaAcademica) {
        if ($this->db->insert("carga_academica", $cargaAcademica)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	
	// codigo de ospino
	
	   // elimina el area (eliminado = 0 A eliminado = 1)
    public function eliminarArea($where, $set){
        $this->db->where($where);
        if($this->db->update("area",$set)){
            redirect("materias/areas");
        }else{
            echo "todo va mal con el area";
            
        }
    }
	
    public function listarArea($id) {
        //listar los grupos disponibles
        //
        //where
        $where = array(
            "area.eliminado" => "0",
            "area.id"=>$id,
            "sede_id" => $this->session->userdata("sede_id")
        );
        $this->db->where($where);
        //selecionar campos
        /**/
        $this->db->select("area.nombre AS nombre,"
                . "area.codigo AS codigo,"
                . "area.id AS id");
    
        //retornar
        return $this->db->get("area")->result();
    }

	    public function actualizarArea($where, $datos) {

        $this->db->where($where);
        if ($this->db->update("area", $datos)) {
            redirect("materias/areas");
        } else {
            echo "mal";
        }
    }

}
